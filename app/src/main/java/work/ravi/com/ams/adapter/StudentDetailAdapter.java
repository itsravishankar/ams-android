package work.ravi.com.ams.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;

import work.ravi.com.ams.R;
import work.ravi.com.ams.ui.activity.ViewAttendanceActivity;
import work.ravi.com.ams.databinding.ViewStudentItemBinding;
import work.ravi.com.ams.pojo.StudentDetail;
import work.ravi.com.ams.util.AppUtil;


public class StudentDetailAdapter extends RecyclerView.Adapter<StudentDetailAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<StudentDetail> studentDetailArrayList;
    private int lastPosition = -1;
    private String tableName;
    private String department;
    private String year;
    private String section;

    public StudentDetailAdapter(Context mContext, ArrayList<StudentDetail> studentDetailArrayList, String department, String year, String section) {
        this.mContext = mContext;
        this.studentDetailArrayList = studentDetailArrayList;
        this.department = department;
        this.year = year;
        this.section = section;
        this.tableName = department + "_" + year;
        AppUtil.initiaUnivarsalImageLoader(mContext);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewStudentItemBinding viewStudentItemBinding = DataBindingUtil.inflate(layoutInflater, R.layout.view_student_item, parent, false);

        return new MyViewHolder(viewStudentItemBinding);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final StudentDetail studentDetail = studentDetailArrayList.get(position);

        holder.studentCount.setText(Integer.toString(position + 1));
        holder.studentName.setText(studentDetail.getStudentName());
        holder.studentEmail.setText(studentDetail.getStudentEmail());
        holder.studentPhone.setText(studentDetail.getStudentMob());
        holder.studentFatherName.setText(studentDetail.getStudentFatherName());
        holder.studentFatherPhone.setText(studentDetail.getStudentFatherMob());
        holder.studentRoll.setText(studentDetail.getStudentRollno());
        holder.studentGroup.setText(studentDetail.getStudentGroup());
        holder.container.startShimmerAnimation();
        AppUtil.setUnivarsalImageLoader(holder.studentImgUrl, studentDetail.getStudentImgUrl(), 100);

        holder.viewAttendance.setOnClickListener(v -> {
            Intent attendancGraphIntent = new Intent(mContext, ViewAttendanceActivity.class);
            attendancGraphIntent.putExtra("g_department", department);
            attendancGraphIntent.putExtra("g_year", year);
            attendancGraphIntent.putExtra("g_section", section);
            attendancGraphIntent.putExtra("g_rollno", studentDetail.getStudentRollno());
            attendancGraphIntent.putExtra("g_student_name", studentDetail.getStudentName());
            mContext.startActivity(attendancGraphIntent);
        });

        setAnimation(holder.itemView, position);

    }

    @Override
    public int getItemCount() {
        return studentDetailArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView studentName, studentEmail, studentPhone, studentFatherName, studentFatherPhone, studentRoll, studentGroup, studentCount, viewAttendance;
        private ImageView studentImgUrl;
        private ShimmerFrameLayout container;

        public MyViewHolder(ViewStudentItemBinding viewStudentItemBinding) {
            super(viewStudentItemBinding.getRoot());

            studentName = viewStudentItemBinding.viewStudentName;
            studentEmail = viewStudentItemBinding.viewStudentEmail;
            studentPhone = viewStudentItemBinding.viewStudentMob;
            studentFatherName = viewStudentItemBinding.viewStudentFatherName;
            studentFatherPhone = viewStudentItemBinding.viewStudentFatherMob;
            studentRoll = viewStudentItemBinding.viewStudentRollNo;
            studentGroup = viewStudentItemBinding.viewStudentGroup;
            studentCount = viewStudentItemBinding.viewStudentCount;
            studentImgUrl = viewStudentItemBinding.studentImg;
            viewAttendance = viewStudentItemBinding.viewAttendance;
            container = viewStudentItemBinding.shimmerViewContainer;

        }
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.zoom_in_anim);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}