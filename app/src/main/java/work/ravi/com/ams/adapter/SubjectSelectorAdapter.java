package work.ravi.com.ams.adapter;

import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import work.ravi.com.ams.R;
import work.ravi.com.ams.ui.activity.MarkAttendanceActivity;
import work.ravi.com.ams.databinding.ItemSubjectSelectorBinding;
import work.ravi.com.ams.pojo.SubjectBySubjectRight;
import work.ravi.com.ams.util.ActivityEnum;
import work.ravi.com.ams.util.AppUtil;
import work.ravi.com.ams.util.JSONExtractor;
import work.ravi.com.ams.util.NetworkUtility;

import static android.content.ContentValues.TAG;

/**
 * Created by Ravi on 06-03-2018.
 */

public class SubjectSelectorAdapter
        extends RecyclerView.Adapter<SubjectSelectorAdapter.SubjectSelectorViewHolder>
        implements LoaderManager.LoaderCallbacks<String> {

    private int LOADER_ID = 0;
    private static String TABLE_NAME_REC = "table_name_rec";
    private static String STUDENT_SECTION = "student_section";
    private static String TABLE_NAME = "table_name";
    private static String SUBJECT_NAME = "subject_name";
    private static String SUBJECT_RIGHT = "subject_rights";
    private ArrayList<SubjectBySubjectRight> subjectBySubjectRights;
    private TextView mTextViewSubjectId, mTextViewSubjectName;
    private CardView mCardViewSubject;
    private String operation, tableName, section, subjectRight;
    private MultipartBody.Builder postData;
    private Context context;
    private LoaderManager loaderManager;
    private SubjectBySubjectRight subjectBySubjectRight;
    private boolean isAllStudentDetailAPIHit = false;
    private String baseEncodedData;


    public SubjectSelectorAdapter(Context context, ArrayList<SubjectBySubjectRight> subjectBySubjectRights, LoaderManager loaderManager, String tableName, String section, String subjectRight) {
        this.context = context;
        this.subjectBySubjectRights = subjectBySubjectRights;
        this.loaderManager = loaderManager;
        this.tableName = tableName;
        this.subjectRight = subjectRight;
        this.section = section;
    }

    @Override
    public SubjectSelectorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        ItemSubjectSelectorBinding itemSubjectSelectorBinding = DataBindingUtil
                .inflate(layoutInflater, R.layout.item_subject_selector, parent, false);
        postData = new MultipartBody.Builder();
        return new SubjectSelectorViewHolder(itemSubjectSelectorBinding);
    }

    @Override
    public void onBindViewHolder(SubjectSelectorViewHolder holder, int position) {
        mTextViewSubjectId.setText(subjectBySubjectRights.get(position).getSubjectId());
        mTextViewSubjectName.setText(subjectBySubjectRights.get(position).getSubjectName());
    }

    @Override
    public int getItemCount() {
        return subjectBySubjectRights.size();
    }

    private void initAllStudentListLoader() {
        isAllStudentDetailAPIHit = true;
        AppUtil.showProgressDialog(context);
        postData
                .addFormDataPart(TABLE_NAME, tableName + "_student_record_" + section);
        Log.d(TAG, "initAllStudentListLoader: " + tableName + "_student_record_" + section);
        operation = NetworkUtility.STUDENT_ALL_DETAIL;
        loaderManager.restartLoader(LOADER_ID, null, this).forceLoad();
    }

    private void initStudentListLoader(SubjectBySubjectRight subjectBySubjectRight) {
        isAllStudentDetailAPIHit = false;
        AppUtil.showProgressDialog(context);
        postData
                .addFormDataPart(TABLE_NAME_REC, tableName + "_" + subjectBySubjectRight.getSubjectShortcut() + "_rec_" + section)
                .addFormDataPart(STUDENT_SECTION, section)
                .addFormDataPart(TABLE_NAME, tableName)
                .addFormDataPart(SUBJECT_NAME, subjectBySubjectRight.getSubjectName())
                .addFormDataPart(SUBJECT_RIGHT, subjectRight);
        operation = NetworkUtility.STUDENT_DETAIL;
        loaderManager.restartLoader(LOADER_ID, null, this).forceLoad();
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        return new StudentListAsyncLoader(context, operation, postData);
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        finishStudentListLoader(data);
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {
    }

    private void finishStudentListLoader(String data) {
        AppUtil.hideProgressDialog();
        byte[] decode = Base64.decode(data.getBytes(), Base64.DEFAULT);
        String jsonString = new String(decode);
        if (data.isEmpty()) {
            AppUtil.showCustomDialog(context, "Something Went Wrong", "Failed to connect to server.", true, R.drawable.ic_error_outline);
        } else {

            String[] response = JSONExtractor.getStatusMessage(jsonString);
            String status = response[0];
            String message = response[1];
            if (status.equals("0")) {
                AppUtil.showCustomDialog(context, "Failed", message, true, R.drawable.ic_error_outline);
            } else {
                if (isAllStudentDetailAPIHit) {
                    Log.d(TAG, "onLoadFinished: JsonResponse AllStudentList " + jsonString);
                    JSONExtractor.extractStudentAllDetailResponse(jsonString);
                    if (!(subjectBySubjectRights.size() == 0)) {
                        Log.d(TAG, "finishAllStudentListLoader: ");
                        //HIts API to Load the Enrolled Students list
                        if (AppUtil.isInternetIsAvailable(context)) {
                            initStudentListLoader(subjectBySubjectRight);
                        } else {
                            AppUtil.showNoInternetDialog(context);
                        }
                    } else {
                        AppUtil.showCustomDialog(context, "Something Went Wrong", "server.under.maintenanceException", true, R.drawable.ic_error_outline);
                    }
                } else {
                    Log.d(TAG, "onLoadFinished: JsonResponse SubjectStudentList " + jsonString);
                    baseEncodedData = jsonString;
                    onStartNextActivity(ActivityEnum.MARK_ATTENDANCE);
                }
            }
        }
    }

    private void onStartNextActivity(ActivityEnum activity) {
        if (ActivityEnum.MARK_ATTENDANCE == activity) {
            Intent intentarkAttendace = new Intent(context, MarkAttendanceActivity.class);
            intentarkAttendace.putExtra("intentExtra", baseEncodedData);
            intentarkAttendace.putExtra("section", section);
            intentarkAttendace.putExtra("table_name", tableName);
            intentarkAttendace.putExtra("subject_shortcut", subjectBySubjectRight.getSubjectShortcut());
            intentarkAttendace.putExtra("subject_name", subjectBySubjectRight.getSubjectName());
            context.startActivity(intentarkAttendace);
        }
    }

    static class StudentListAsyncLoader extends AsyncTaskLoader<String> {
        String operation;
        MultipartBody.Builder postData;

        public StudentListAsyncLoader(Context context, String operation,
                                      MultipartBody.Builder postData) {
            super(context);
            this.operation = operation;
            this.postData = postData;
        }

        @Override
        public String loadInBackground() {
            String response = NetworkUtility.getServerResponse(getContext(), operation, postData);
            return response;
        }
    }

    public class SubjectSelectorViewHolder extends RecyclerView.ViewHolder {

        public SubjectSelectorViewHolder(ItemSubjectSelectorBinding itemSubjectSelectorBinding) {
            super(itemSubjectSelectorBinding.getRoot());
            mTextViewSubjectId = itemSubjectSelectorBinding.textViewSubjectId;
            mTextViewSubjectName = itemSubjectSelectorBinding.textViewSubjectName;
            mCardViewSubject = itemSubjectSelectorBinding.cardViewSubject;

            mCardViewSubject.setOnClickListener(v -> {
                Log.d(TAG, "onClick: ");
                subjectBySubjectRight = subjectBySubjectRights.get(getAdapterPosition());
                if (AppUtil.isInternetIsAvailable(context)) {
                    initAllStudentListLoader();
                } else {
                    AppUtil.showNoInternetDialog(context);
                }
            });
        }
    }
}
