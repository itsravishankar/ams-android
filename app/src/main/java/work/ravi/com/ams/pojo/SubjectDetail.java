package work.ravi.com.ams.pojo;

/**
 * Created by acer on 05-03-2018.
 */

public class SubjectDetail {
     private String subjectId;
     private String subjectName;
     private String subjectSrct;
     private String facultyName;
     private String subjectRights;
     private String subjectType;

    public SubjectDetail(String subjectId, String subjectName, String subjectSrct, String facultyName, String subjectRights, String subjectType) {
        this.subjectId = subjectId;
        this.subjectName = subjectName;
        this.subjectSrct = subjectSrct;
        this.facultyName = facultyName;
        this.subjectRights = subjectRights;
        this.subjectType = subjectType;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectSrct() {
        return subjectSrct;
    }

    public void setSubjectSrct(String subjectSrct) {
        this.subjectSrct = subjectSrct;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public String getSubjectRights() {
        return subjectRights;
    }

    public void setSubjectRights(String subjectRights) {
        this.subjectRights = subjectRights;
    }

    public String getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(String subjectType) {
        this.subjectType = subjectType;
    }
}
