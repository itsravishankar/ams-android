package work.ravi.com.ams.pojo;

/**
 * Created by acer on 03-03-2018.
 */

public class HodLoginDetail {
    private String hodDepartment;
    private String hodName;
    private String hodEmail;
    private String DepartmentKey;
    private String publicKey;

    public HodLoginDetail(String hodDepartment, String hodName, String hodEmail, String departmentKey, String publicKey) {
        this.hodDepartment = hodDepartment;
        this.hodName = hodName;
        this.hodEmail = hodEmail;
        DepartmentKey = departmentKey;
        this.publicKey = publicKey;
    }

    public String getHodDepartment() {
        return hodDepartment;
    }

    public void setHodDepartment(String hodDepartment) {
        this.hodDepartment = hodDepartment;
    }

    public String getHodName() {
        return hodName;
    }

    public void setHodName(String hodName) {
        this.hodName = hodName;
    }

    public String getHodEmail() {
        return hodEmail;
    }

    public void setHodEmail(String hodEmail) {
        this.hodEmail = hodEmail;
    }

    public String getDepartmentKey() {
        return DepartmentKey;
    }

    public void setDepartmentKey(String departmentKey) {
        DepartmentKey = departmentKey;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }
}
