package work.ravi.com.ams.pojo;

import java.util.HashMap;

/**
 * Created by Ravi on 06-03-2018.
 */

/**
 * This POJO store all the student record in hashmap
 */
public class AllStudentList {
    private static final AllStudentList ourInstance = new AllStudentList();
    private  HashMap<String, StudentDetail> studentList;

    public static AllStudentList getInstance() {
        return ourInstance;
    }

    private AllStudentList() {
    }
    public void setStudentList(HashMap<String, StudentDetail> studentList){
        this.studentList = studentList;
    }
    public HashMap<String, StudentDetail> getStudentList() {
        return studentList;
    }
}
