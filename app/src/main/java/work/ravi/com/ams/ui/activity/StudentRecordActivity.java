package work.ravi.com.ams.ui.activity;

import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;

import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import work.ravi.com.ams.R;
import work.ravi.com.ams.adapter.StudentDetailAdapter;
import work.ravi.com.ams.databinding.ActivityStudentRecordBinding;
import work.ravi.com.ams.ui.fragment.selector.DepartmentSelectorFragment;
import work.ravi.com.ams.pojo.StudentDetail;
import work.ravi.com.ams.util.AppUtil;
import work.ravi.com.ams.util.JSONExtractor;
import work.ravi.com.ams.util.NetworkUtility;

import static android.content.ContentValues.TAG;

/**
 * Created by acer on 04-03-2018.
 */

public class StudentRecordActivity extends AppCompatActivity implements View.OnClickListener, LoaderManager.LoaderCallbacks<String> {
    private ActivityStudentRecordBinding activityStudentRecordBinding;
    private ArrayList<StudentDetail> studentDetailArrayList;
    private MultipartBody.Builder postData;

    private StudentDetail studentDetail;
    private StudentDetailAdapter studentDetailAdapter;

    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView studentRecyclerView;

    private String operation;
    private int LOADER_ID = 0;
    private String tableName;
    private String department;
    private String year;
    private String section;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityStudentRecordBinding = DataBindingUtil.setContentView(this, R.layout.activity_student_record);

        getBundleExtras();

        studentDetailArrayList = new ArrayList<>();
        studentDetailAdapter = new StudentDetailAdapter(this, studentDetailArrayList,department,year,section);
        studentRecyclerView = activityStudentRecordBinding.studentRecyclerViewLayout.recyclerViewStudent;

        mLayoutManager = new GridLayoutManager(this, 1);
        studentRecyclerView.setLayoutManager(mLayoutManager);
        studentRecyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        studentRecyclerView.setItemAnimator(new DefaultItemAnimator());
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("finish_activity"));
        postData = new MultipartBody.Builder();

        if (AppUtil.isInternetIsAvailable(this)) {
            initStudentRecordLoader();
        } else {
            AppUtil.showNoInternetDialog(this);
        }
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        private GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /*converter dp to px*/
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private void getBundleExtras() {
        department = getIntent().getExtras().getString(DepartmentSelectorFragment.DEPARTMENT);
        year = getIntent().getExtras().getString(DepartmentSelectorFragment.YEAR);
        section = getIntent().getExtras().getString(DepartmentSelectorFragment.SECTION);
        tableName = department + "_" + year + "_student_record_" + section;
        Log.d(TAG, "getBundleExtras: " + tableName);
    }

    private void initStudentRecordLoader() {
        AppUtil.showProgressDialog(this);

        /*get the student detail*/

        operation = NetworkUtility.STUDENT_ALL_DETAIL;
        postData
                .addFormDataPart("table_name", tableName);
        getLoaderManager().restartLoader(LOADER_ID, null, this).forceLoad();

    }

    private void finishStudentRecordData(String data) {
        AppUtil.hideProgressDialog();
        if (data.isEmpty()) {
            AppUtil.showCustomDialog(this, "Something Went Wrong", "Failed to connect to server.", true, R.drawable.ic_error_outline);
        } else {
            byte[] decode = Base64.decode(data.getBytes(), Base64.DEFAULT);
            String jsonString = new String(decode);


            String[] response = JSONExtractor.getStatusMessage(jsonString);
            String status = response[0];
            String message = response[1];
            if (status.equals("0")) {
                AppUtil.showCustomDialog(this, "Failed", message, true, R.drawable.ic_error_outline);
            } else {
                studentDetailArrayList = JSONExtractor.extractStudentAllDetailResponse(jsonString);
                studentDetailAdapter = new StudentDetailAdapter(this, studentDetailArrayList,department,year,section);
                studentRecyclerView.setAdapter(studentDetailAdapter);
            }

        }
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        return new StudentRecordAsyncLoader(this, postData, operation);
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {

        finishStudentRecordData(data);
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {

    }

    static class StudentRecordAsyncLoader extends AsyncTaskLoader<String> {
        String operation;
        MultipartBody.Builder postData;
        private String TAG = "StudentRecordAsyncLoader";

        public StudentRecordAsyncLoader(Context context, MultipartBody.Builder postData, String operation) {
            super(context);
            this.operation = operation;
            this.postData = postData;
        }

        @Override
        public String loadInBackground() {
            String response = NetworkUtility.getServerResponse(getContext(), operation, postData);
            Log.d(TAG, "loadInBackground: UserLogin Response" + response);
            return response;
        }
    }
}
