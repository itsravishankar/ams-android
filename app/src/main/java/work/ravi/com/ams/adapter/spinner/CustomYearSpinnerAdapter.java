package work.ravi.com.ams.adapter.spinner;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import work.ravi.com.ams.R;
import work.ravi.com.ams.pojo.DepartmentDetail;

/**
 * Created by Ravi on 04-03-2018.
 */

public class CustomYearSpinnerAdapter extends ArrayAdapter<String> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final List<String> yearList;
    private final int mResourceLayout;

    public CustomYearSpinnerAdapter(@NonNull Context context, @LayoutRes int resource,
                                          @NonNull List objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResourceLayout = resource;
        yearList = objects;

    }
    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResourceLayout, parent, false);

        TextView textViewYear = view.findViewById(R.id.text_view_year);
        ImageView yearImage = view.findViewById(R.id.image_view_year_spinner);

        textViewYear.setText(yearList.get(position));

        switch (position){

            case 0:
                yearImage.setImageResource(R.drawable.ic_one_year);
                break;
            case 1:
                yearImage.setImageResource(R.drawable.ic_second_year);
                break;
            case 2:
                yearImage.setImageResource(R.drawable.ic_third_year);
                break;
            case 3:
                yearImage.setImageResource(R.drawable.ic_fourth_year);
                break;
        }

        return view;
    }
}



/*
public class CustomYearSpinnerAdapter extends ArrayAdapter<String> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private ArrayList<String> yearList = new ArrayList<>();
    private final int mResource;
    List<DepartmentDetail> detailList;

    public CustomYearSpinnerAdapter (@NonNull Context context, @LayoutRes int resource,
                                          @NonNull List objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        detailList = objects;
        yearList.add("1st Year");
        yearList.add("2nd Year");
        yearList.add("3rd Year");
        yearList.add("4th Year");

    }
    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);

        TextView textViewYear = view.findViewById(R.id.text_view_year);
        ImageView yearImage = view.findViewById(R.id.image_view_year_spinner);

        textViewYear.setText(yearList.get(position));

        switch (position){

            case 0:
                yearImage.setImageResource(R.drawable.ic_one_year);
                break;
            case 1:
                yearImage.setImageResource(R.drawable.ic_second_year);
                break;
            case 2:
                yearImage.setImageResource(R.drawable.ic_third_year);
                break;
            case 3:
                yearImage.setImageResource(R.drawable.ic_fourth_year);
                break;
        }

        return view;
    }
}
*/
