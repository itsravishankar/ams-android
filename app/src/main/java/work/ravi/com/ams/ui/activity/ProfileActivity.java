package work.ravi.com.ams.ui.activity;

import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;

import work.ravi.com.ams.R;
import work.ravi.com.ams.databinding.FacultyUserProfileBinding;
import work.ravi.com.ams.databinding.UserProfileBinding;
import work.ravi.com.ams.ui.activity.prelogin.LoginActivity;

/**
 * Created by acer on 04-03-2018.
 */

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {
    private UserProfileBinding userProfileBinding;

    LinearLayout linearLayoutFaculty, linearLayoutHod;

    TextView profileFacultyName, profileFacultyEmail, profileFacultyDepartment, profileFacultyPassword, profileFacultyMob, profileFacultySpecification;

    TextView studentRollno, studentMob, studentGroup, studentFatherName, studentFatherMob;

    TextView hodName, hodEmail, hodDepartment, departmentKey, publicKey;

    private String sStringIntentExtra;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userProfileBinding = DataBindingUtil.setContentView(this, R.layout.user_profile);
        bindView();
        sStringIntentExtra = getIntent().getStringExtra(LoginActivity.USER_TYPE);

        if (sStringIntentExtra.equals("0")) {
            setHodProfileData();
        } else if (sStringIntentExtra.equals("1")) {
            setFacultyProfileData();
        } else if (sStringIntentExtra.equals("2")) {
            Toast.makeText(this, "unknown.profile.Exception", Toast.LENGTH_LONG).show();//TODO change here

        } else {
            Toast.makeText(this, "unknown.profile.Exception", Toast.LENGTH_LONG).show();
        }

    }

    private void bindView() {

        profileFacultyName = userProfileBinding.facultyUserProfile.facultyUserName;
        profileFacultyEmail = userProfileBinding.facultyUserProfile.facultyUserEmail;
        profileFacultyDepartment = userProfileBinding.facultyUserProfile.facultyUserDepartment;
        profileFacultyPassword = userProfileBinding.facultyUserProfile.facultyUserPassword;
        profileFacultyMob = userProfileBinding.facultyUserProfile.facultyUserMobile;
        profileFacultySpecification = userProfileBinding.facultyUserProfile.facultyUserSpecification;

        hodName = userProfileBinding.hodUserProfile.hodUserName;
        hodEmail = userProfileBinding.hodUserProfile.hodUserEmail;
        hodDepartment = userProfileBinding.hodUserProfile.hodUserDepartment;
        departmentKey = userProfileBinding.hodUserProfile.hodUserDepKey;
        publicKey = userProfileBinding.hodUserProfile.hodPublicKey;

        linearLayoutFaculty = userProfileBinding.facultyLl;
        linearLayoutHod = userProfileBinding.hodLl;
    }

    private void setFacultyProfileData() {


        linearLayoutHod.setVisibility(View.GONE);
        linearLayoutFaculty.setVisibility(View.VISIBLE);
        profileFacultyName.setText(getValueByPrefrenceForFaculty(getString(R.string.faculty_name)));
        profileFacultyEmail.setText(getValueByPrefrenceForFaculty(getString(R.string.faculty_email)));
        profileFacultyDepartment.setText(getValueByPrefrenceForFaculty(getString(R.string.faculty_dep)));
        profileFacultyPassword.setText(getValueByPrefrenceForFaculty(getString(R.string.faculty_pass)));
        profileFacultyMob.setText(getValueByPrefrenceForFaculty(getString(R.string.faculty_mob)));
        profileFacultySpecification.setText(getValueByPrefrenceForFaculty(getString(R.string.faculty_spe)));

    }

    private void setHodProfileData() {
        linearLayoutFaculty.setVisibility(View.GONE);
        linearLayoutHod.setVisibility(View.VISIBLE);
        hodName.setText(getValueByPrefrenceForHod(getString(R.string.hod_name)));
        hodEmail.setText(getValueByPrefrenceForHod(getString(R.string.hod_email)));
        hodDepartment.setText(getValueByPrefrenceForHod(getString(R.string.hod_department)));
        departmentKey.setText(getValueByPrefrenceForHod(getString(R.string.dep_key)));
        publicKey.setText(getValueByPrefrenceForHod(getString(R.string.public_key)));
    }

    /*get value by sharedpre..*/
    public String getValueByPrefrenceForFaculty(String key) {
        String value = null;
        SharedPreferences userDetails = this.getSharedPreferences(getString(R.string.facutly_login_sp), MODE_PRIVATE);
        value = userDetails.getString(key, "Dummy Value").toString();
        return value;
    }

    public String getValueByPrefrenceForHod(String key) {
        String value = null;
        SharedPreferences userDetails = this.getSharedPreferences(getString(R.string.hod_login_sp), MODE_PRIVATE);
        value = userDetails.getString(key, "Dummy Value").toString();
        return value;
    }

    public String getValueByPrefrenceForStudent(String key) {
        String value = null;
        SharedPreferences userDetails = this.getSharedPreferences(getString(R.string.student_login_sp), MODE_PRIVATE);
        value = userDetails.getString(key, "Dummy Value").toString();
        return value;
    }

    @Override
    public void onClick(View v) {

    }
}
