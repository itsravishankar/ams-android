package work.ravi.com.ams.sharedprefrence;

import android.content.Context;
import android.content.SharedPreferences;

import work.ravi.com.ams.ui.fragment.selector.DepartmentSelectorFragment;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by acer on 05-03-2018.
 */

public class LoginSession {
    private static final LoginSession ourInstance = new LoginSession();
    private static final String LOGIN_SP_NAME = "LOGIN_SP_NAME";
    private static final String SET_LOGIN = "SET_LOGIN";
    private static final String SET_USER = "SET_USER";
    private static final String SET_CURRENT_DATE = "SET_CURRENT_DATE";
    private static final String SET_CURRENT_TIME = "SET_CURRENT_TIME";
    private static final String SET_NEW_USER = "SET_NEW_USER";
    private static final String SET_FACULTY_KEY = "SET_FACULTY_KEY";
    private static LoginSession loginSession = new LoginSession();
    private static SharedPreferences sp;
    private static SharedPreferences.Editor editor;

    public static LoginSession getInstance(Context context) {
        sp = context.getSharedPreferences(LOGIN_SP_NAME, MODE_PRIVATE);
        editor = sp.edit();
        return loginSession;
    }

    public boolean isLoggedIn() {
        return sp.getBoolean(SET_LOGIN, false);
    }

    public void setLogIn(boolean isLogin) {
        editor.putBoolean(SET_LOGIN, isLogin);
        editor.commit();
    }
    public boolean isAppRunningFirstTime() {
        return sp.getBoolean(SET_NEW_USER, true);
    }

    public void setAppRunningFirstTime(boolean b) {
        editor.putBoolean(SET_NEW_USER, b);
        editor.commit();
    }
    public int getUserType() {
        return sp.getInt(SET_USER, -1);
    }

    public void setUserType(int userType) {
        editor.putInt(SET_USER, userType);
        editor.commit();
    }
    public String getTime() {
        return sp.getString(SET_CURRENT_TIME, "");
    }

    public void setTime(String time) {
        editor.putString(SET_CURRENT_TIME, time);
        editor.commit();
    }
    public String getFacultyKey() {
        return sp.getString(SET_FACULTY_KEY, "");
    }

    public void setFacultyKey(String key) {
        editor.putString(SET_FACULTY_KEY, key);
        editor.commit();
    }
    public String getDepartment() {
        return sp.getString(DepartmentSelectorFragment.DEPARTMENT, "");
    }

    public void setDepartment(String department) {
        editor.putString(DepartmentSelectorFragment.DEPARTMENT, department);
        editor.commit();
    }
    public String getYear() {
        return sp.getString(DepartmentSelectorFragment.YEAR, "");
    }

    public void setYear(String year) {
        editor.putString(DepartmentSelectorFragment.YEAR, year);
        editor.commit();
    }
    public String getSection() {
        return sp.getString(DepartmentSelectorFragment.SECTION, "");
    }

    public void setDSection(String department) {
        editor.putString(DepartmentSelectorFragment.SECTION, department);
        editor.commit();
    }
    public String getCurrentDate() {
        return sp.getString(SET_CURRENT_DATE, "");
    }

    public void setCurrentDate(String currentDate) {
        editor.putString(SET_CURRENT_DATE, currentDate);
        editor.commit();
    }

    private LoginSession() {
    }
}
