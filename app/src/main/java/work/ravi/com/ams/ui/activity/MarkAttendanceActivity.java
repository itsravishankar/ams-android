package work.ravi.com.ams.ui.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

import work.ravi.com.ams.R;
import work.ravi.com.ams.adapter.MarkAttendanceStudentListAdapter;
import work.ravi.com.ams.databinding.ActivityMarkAttendanceBinding;
import work.ravi.com.ams.pojo.EnrolledStudentList;
import work.ravi.com.ams.pojo.MarkedAttendanceHolder;
import work.ravi.com.ams.sharedprefrence.LoginSession;
import work.ravi.com.ams.sharedprefrence.MarkedAttendanceRecords;
import work.ravi.com.ams.util.JSONExtractor;

/**
 * Created by Ravi on 06-03-2018.
 */

public class MarkAttendanceActivity extends AppCompatActivity  {

    private static final String TAG = "MarkAttendanceActivity";
    private ArrayList<EnrolledStudentList> studentDetails = new ArrayList<>();
    private RecyclerView mRecyclerViewStudentList;
    private FloatingActionButton floatingActionButton;
    ArrayList<String> hashMapKeyName = new ArrayList<>();
    HashMap<String, MarkedAttendanceHolder> markedAttendanceHolderHashMap = new HashMap<>();
    private Toolbar toolbar, searchToolbar;
    private Menu search_menu;
    private MenuItem item_search;
    private String jsonString, section, tableName, subjectShortcut, subjectName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMarkAttendanceBinding activityMarkAttendanceBinding = DataBindingUtil
                .setContentView(this, R.layout.activity_mark_attendance);
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("finish_activity"));
        toolbar = activityMarkAttendanceBinding.toolbarMarkAttendance.toolbar;
        searchToolbar = activityMarkAttendanceBinding.toolbarSearchBarMarkAttendance.searchtoolbar;
        setSupportActionBar(toolbar);
        setSearchToolbar();
        floatingActionButton = activityMarkAttendanceBinding.floatingActionButtonSaveAttendance;
        getIntentExtras();
        Log.d(TAG, "onCreate: jsonString :" + jsonString);
        studentDetails = JSONExtractor.extractEnrolledStudentsDetailResponse(jsonString);
        mRecyclerViewStudentList = activityMarkAttendanceBinding.recyclerViewAttendanceList;



        initStudentList(studentDetails);
       floatingActionButton.setOnClickListener(view -> {
           String record = MarkAttendanceStudentListAdapter.getMarkedAttendanceList();
           String date = LoginSession.getInstance(this).getCurrentDate();
           String time = LoginSession.getInstance(this).getTime();
           MarkedAttendanceHolder markedAttendanceHolder =
                   new MarkedAttendanceHolder(tableName+"_"+subjectShortcut,
                           section, date, time, record, subjectName);
           MarkedAttendanceRecords markedAttendanceRecords = MarkedAttendanceRecords.getInstance(this);
           markedAttendanceHolderHashMap = markedAttendanceRecords.getAttendanceRecord();
           markedAttendanceHolderHashMap.put(date + time, markedAttendanceHolder);
           hashMapKeyName = markedAttendanceRecords.getHashMapKeyName();
           hashMapKeyName.add(date + time);
           markedAttendanceRecords.setAttendanceRecord(markedAttendanceHolderHashMap);
           markedAttendanceRecords.setHashMapKeyName(hashMapKeyName);
           Toast.makeText(this, "Attendance Saved on Local DataBase", Toast.LENGTH_SHORT).show();
           finish();
       });
    }

    private void getIntentExtras() {
        Bundle bundle = getIntent().getExtras();

        jsonString = bundle.getString("intentExtra");
        section = bundle.getString("section");
        tableName = bundle.getString("table_name");
        subjectShortcut = bundle.getString("subject_shortcut");
        subjectName = bundle.getString("subject_name");
    }


    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_search:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    circleReveal(searchToolbar,1,true,true);
                else
                    searchToolbar.setVisibility(View.VISIBLE);

                item_search.expandActionView();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void setSearchToolbar() {

        if (searchToolbar != null) {
            searchToolbar.inflateMenu(R.menu.menu_search);
            search_menu= searchToolbar.getMenu();

            searchToolbar.setNavigationOnClickListener(v -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    circleReveal(searchToolbar,1,true,false);
                else
                    searchToolbar.setVisibility(View.GONE);
            });

            item_search = search_menu.findItem(R.id.action_filter_search);

            MenuItemCompat.setOnActionExpandListener(item_search, new MenuItemCompat.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionCollapse(MenuItem item) {
                    // Do something when collapsed
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        circleReveal(searchToolbar,1,true,false);
                    }
                    else
                        searchToolbar.setVisibility(View.GONE);
                    return true;
                }

                @Override
                public boolean onMenuItemActionExpand(MenuItem item) {
                    // Do something when expanded
                    return true;
                }
            });

            initSearchView();


        } else
            Log.d("toolbar", "setSearchToolbar: NULL");
    }

    public void initSearchView() {
        final SearchView searchView =
                (SearchView) search_menu.findItem(R.id.action_filter_search).getActionView();

        // Enable/Disable Submit button in the keyboard

        searchView.setSubmitButtonEnabled(false);

        // Change search close button image

        ImageView closeButton = (ImageView) searchView.findViewById(R.id.search_close_btn);
        closeButton.setImageResource(R.drawable.ic_close);


        // set hint and the text colors

        EditText txtSearch = ((EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text));
        txtSearch.setHint("Search..");
        txtSearch.setHintTextColor(Color.DKGRAY);
        txtSearch.setTextColor(getResources().getColor(R.color.colorPrimary));


        // set the cursor

        AutoCompleteTextView searchTextView = (AutoCompleteTextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, R.drawable.search_cursor); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
            e.printStackTrace();
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                callSearch(query);
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                callSearch(newText);
                return true;
            }

            public void callSearch(String query) {
                //Do searching

                ArrayList<EnrolledStudentList> list = studentDetails;
                ArrayList<EnrolledStudentList> filteredRollNos = new ArrayList<>();

                for (int i = 0; i < list.size(); i++){

                    EnrolledStudentList enrolledStudent = list.get(i);
                    final String rollSearch = enrolledStudent.getRollNo().toString().trim();
                    if (rollSearch.contains(query)){
                        filteredRollNos.add(list.get(i));
                    }
                }
                // Code to populate recycler view with mathced items
                MarkAttendanceStudentListAdapter markAttendanceStudentListAdapter = new MarkAttendanceStudentListAdapter(MarkAttendanceActivity.this, filteredRollNos);
                mRecyclerViewStudentList.setAdapter(markAttendanceStudentListAdapter);
                Log.i("query", "" + query);

            }

        });

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void circleReveal(View view, int posFromRight, boolean containsOverflow, final boolean isShow)
    {
        final View myView = view;

        int width=myView.getWidth();

        if(posFromRight>0)
            width-=(posFromRight*getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material))-(getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material)/ 2);
        if(containsOverflow)
            width-=getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_overflow_material);

        int cx=width;
        int cy=myView.getHeight()/2;

        Animator anim;
        if(isShow)
            anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0,(float)width);
        else
            anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, (float)width, 0);

        anim.setDuration((long)220);

        // make the view invisible when the animation is done
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if(!isShow)
                {
                    super.onAnimationEnd(animation);
                    myView.setVisibility(View.INVISIBLE);
                }
            }
        });

        // make the view visible and start the animation
        if(isShow)
            myView.setVisibility(View.VISIBLE);

        // start the animation
        anim.start();

    }




    private void initStudentList(ArrayList<EnrolledStudentList> studentDetails) {
        RecyclerView.LayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
        MarkAttendanceStudentListAdapter markAttendanceStudentListAdapter = new MarkAttendanceStudentListAdapter(this, studentDetails);
        mRecyclerViewStudentList.setLayoutManager(mLinearLayoutManager);
        mRecyclerViewStudentList.setItemAnimator(new DefaultItemAnimator());
        mRecyclerViewStudentList.setAdapter(markAttendanceStudentListAdapter);
    }


}
