
package work.ravi.com.ams.ui.fragment.selector;


import android.app.ActivityOptions;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import work.ravi.com.ams.R;
import work.ravi.com.ams.adapter.spinner.CustomDepartmentSpinnerAdapter;
import work.ravi.com.ams.adapter.spinner.CustomSectionSpinnerAdapter;
import work.ravi.com.ams.adapter.spinner.CustomYearSpinnerAdapter;
import work.ravi.com.ams.databinding.FragmentDepartmentSelectionBinding;
import work.ravi.com.ams.pojo.DepartmentDetail;
import work.ravi.com.ams.ui.activity.GenerateReportActivity;
import work.ravi.com.ams.ui.activity.NotifyStudentActivity;
import work.ravi.com.ams.ui.activity.StudentRecordActivity;
import work.ravi.com.ams.ui.activity.prelogin.LoginActivity;
import work.ravi.com.ams.ui.activity.prelogin.PreLoginActivity;
import work.ravi.com.ams.ui.activity.prelogin.SplashActivity;
import work.ravi.com.ams.util.ActivityEnum;
import work.ravi.com.ams.util.AppUtil;
import work.ravi.com.ams.util.JSONExtractor;
import work.ravi.com.ams.util.NetworkUtility;

import static android.content.ContentValues.TAG;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Ravi on 04-03-2018.
 */

public class DepartmentSelectorFragment extends Fragment implements LoaderManager.LoaderCallbacks<String> {

    public static String DEPARTMENT = "DEPARTMENT";
    public static String YEAR = "YEAR";
    public static String SECTION = "SECTION";
    private static String sStringDepartmentIntentExtra, sStringYearIntentExtra, sStringSectionIntentExtra;
    private FragmentDepartmentSelectionBinding fragmentDepartmentSelectionBinding;
    private MultipartBody.Builder postData;
    private ArrayList<DepartmentDetail> departmentDetailArrayList;
    private String operation;
    private int LOADER_ID = 0;
    private Spinner mSpinnerDepartment, mSpinnerYear, mSpinnerSection;
    private DepartmentDetail departmentDetail;
    private Button mButtonNext;
    private String activityIndicator = "";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        fragmentDepartmentSelectionBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_department_selection, container, false);
        mButtonNext = fragmentDepartmentSelectionBinding.buttonDepartmentSelector;

        activityIndicator = getArguments().getString("activity_indicator");
        if (AppUtil.isInternetIsAvailable(getActivity())) {
            initDepartmentLoader();
        } else {
            AppUtil.showNoInternetDialog(getActivity());
        }
        departmentDetailArrayList = new ArrayList<>();
        onNextButtonClick();
        return fragmentDepartmentSelectionBinding.getRoot();
    }

    private void onNextButtonClick() {
        mButtonNext.setOnClickListener(v -> {
            Log.d(TAG, "onClick: " + activityIndicator);
            if (activityIndicator.equals("student_record")) {
                if (sStringDepartmentIntentExtra
                        .equals(getValueByPrefrenceForFaculty(getResources()
                                .getString(R.string.faculty_dep))))
                    openStudentRecordActivity();
                else
                    AppUtil.showCustomDialog(getActivity(), "Error", "You are not from the selected department !", true, R.drawable.ic_error_outline);
            } else if (activityIndicator.equals("mark_attendance")) {
                openSubjectSelectorFragment();
            } else if (activityIndicator.equals("generate_report")) {
                generateReportActivity();
            } else if (activityIndicator.equals("notify_student")) {
                openNotifyStudentActivity();
            } else if (activityIndicator.equals("student_login")) {
                openLoginScreen();
            } else
                Toast.makeText(getActivity(), "activity.IndicatorException", Toast.LENGTH_SHORT).show();
        });
    }

    private void openLoginScreen() {
        Intent intentLogin = new Intent(getActivity(), LoginActivity.class);
        ActivityOptions activityOptions = null;
        intentLogin.putExtra("USER_TYPE", PreLoginActivity.STUDENT);
        intentLogin.putExtra(DEPARTMENT, sStringDepartmentIntentExtra);
        intentLogin.putExtra(YEAR, sStringYearIntentExtra);
        intentLogin.putExtra(SECTION, sStringSectionIntentExtra);
        startActivity(intentLogin);
    }

    /*get value by sharedpre..*/
    public String getValueByPrefrenceForFaculty(String key) {
        String value = null;
        SharedPreferences userDetails = getActivity().getSharedPreferences(getString(R.string.facutly_login_sp), MODE_PRIVATE);
        value = userDetails.getString(key, "Dummy Value").toString();
        return value;
    }

    //notify_student
    private void openNotifyStudentActivity() {
        Intent intent = new Intent(getActivity(), NotifyStudentActivity.class);
        Bundle args = new Bundle();
        args.putString(DEPARTMENT, sStringDepartmentIntentExtra);
        args.putString(YEAR, sStringYearIntentExtra);
        args.putString(SECTION, sStringSectionIntentExtra);
        intent.putExtras(args);
        startActivity(intent);
    }

    private void openStudentRecordActivity() {
        Intent intent = new Intent(getActivity(), StudentRecordActivity.class);
        Bundle args = new Bundle();
        args.putString(DEPARTMENT, sStringDepartmentIntentExtra);
        args.putString(YEAR, sStringYearIntentExtra);
        args.putString(SECTION, sStringSectionIntentExtra);
        intent.putExtras(args);
        startActivity(intent);
    }

    private void generateReportActivity() {
        Intent intent = new Intent(getActivity(), GenerateReportActivity.class);
        Bundle args = new Bundle();
        args.putString(DEPARTMENT, sStringDepartmentIntentExtra);
        args.putString(YEAR, sStringYearIntentExtra);
        args.putString(SECTION, sStringSectionIntentExtra);
        intent.putExtras(args);
        startActivity(intent);
    }

    private void openSubjectSelectorFragment() {
        SubjectSelectorFragment subjectSelectorFragment = new SubjectSelectorFragment();
        Bundle args = new Bundle();
        args.putString(DEPARTMENT, sStringDepartmentIntentExtra);
        args.putString(YEAR, sStringYearIntentExtra);
        args.putString(SECTION, sStringSectionIntentExtra);
        subjectSelectorFragment.setArguments(args);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.rl_fragment_container, subjectSelectorFragment);
        fragmentTransaction.addToBackStack("frag");
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();

        /*FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        //SubjectSelectorFragment subjectSelectorFragment = new SubjectSelectorFragment();
        Bundle args = new Bundle();
        args.putString(DEPARTMENT, sStringDepartmentIntentExtra);
        args.putString(YEAR, sStringYearIntentExtra);
        args.putString(SECTION, sStringSectionIntentExtra);
        subjectSelectorFragment.setArguments(args);
        fragmentTransaction.replace(R.id.rl_fragment_container, subjectSelectorFragment);
        fragmentTransaction.addToBackStack("prefrence");
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();*/
    }

    private void initDepartmentSpinner() {
        mSpinnerDepartment = fragmentDepartmentSelectionBinding.spinnerDepartment;
        CustomDepartmentSpinnerAdapter customDepartmentSpinnerAdapter = new CustomDepartmentSpinnerAdapter(getActivity(), R.layout.spinner_department_layout, departmentDetailArrayList);
        mSpinnerDepartment.setAdapter(customDepartmentSpinnerAdapter);
        mSpinnerDepartment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                initYearSpinner();
                sStringDepartmentIntentExtra = ((TextView) view.findViewById(R.id.text_view_department_shortcut)).getText().toString();
                Log.d(TAG, "onItemSelected: " + sStringDepartmentIntentExtra);
                mSpinnerDepartment.setSelection(position, true);
                departmentDetail = departmentDetailArrayList.get(position);
                AppUtil.hideProgressDialog();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mButtonNext.setClickable(false);
                mButtonNext.setBackgroundColor(getResources().getColor(R.color.colorGrey));
            }
        });
    }

    private void initSectionSpinner(int noOfSection, int year) {
        ArrayList<String> sectionList = new ArrayList<>();
        if (noOfSection == 0) {
            mButtonNext.setClickable(false);
            mButtonNext.setBackgroundColor(getResources().getColor(R.color.colorGrey));
        }
        for (int i = 0; i < noOfSection; i++) {
            sectionList.add(Character.toString((char) (65 + i)));
        }
        mSpinnerSection = fragmentDepartmentSelectionBinding.spinnerSection;
        CustomSectionSpinnerAdapter customSectionSpinnerAdapter = new CustomSectionSpinnerAdapter(getActivity(), R.layout.spinner_section_layout, sectionList);
        mSpinnerSection.setAdapter(customSectionSpinnerAdapter);
        mSpinnerSection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //String item = ((TextView) view.findViewById(R.id.text_view_section)).getText().toString();
                sStringSectionIntentExtra = Character.toString((char) (97 + position));
                Log.d(TAG, "onItemSelected: " + sStringSectionIntentExtra);
                mSpinnerSection.setSelection(position, true);
                mButtonNext.setClickable(true);
                mButtonNext.setBackgroundColor(getResources().getColor(R.color.cyan));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mButtonNext.setClickable(false);
                mButtonNext.setBackgroundColor(getResources().getColor(R.color.colorGrey));
            }
        });
    }

    private void initYearSpinner() {

        ArrayList<String> yearList = new ArrayList<>();
        yearList.add("1st Year");
        yearList.add("2nd Year");
        yearList.add("3rd Year");
        yearList.add("4th Year");

        mSpinnerYear = fragmentDepartmentSelectionBinding.spinnerYear;
        CustomYearSpinnerAdapter customYearSpinnerAdapter = new CustomYearSpinnerAdapter(getActivity(), R.layout.spinner_year_layout, yearList);
        mSpinnerYear.setAdapter(customYearSpinnerAdapter);
        mSpinnerYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSpinnerYear.setSelection(position, true);
                String year = null;

                switch (position) {

                    case 0:
                        initSectionSpinner(Integer.parseInt(departmentDetail.getFirstYear()), 1);
                        year = "first_year";
                        break;
                    case 1:
                        initSectionSpinner(Integer.parseInt(departmentDetail.getSecondYear()), 2);
                        year = "second_year";
                        break;
                    case 2:
                        initSectionSpinner(Integer.parseInt(departmentDetail.getThirdYear()), 3);
                        year = "third_year";
                        break;
                    case 3:
                        initSectionSpinner(Integer.parseInt(departmentDetail.getFourthYear()), 4);
                        year = "fourth_year";
                        break;
                }
                sStringYearIntentExtra = year;
                Log.d(TAG, "onItemSelected: " + sStringYearIntentExtra);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mButtonNext.setClickable(false);
                mButtonNext.setBackgroundColor(getResources().getColor(R.color.colorGrey));
            }
        });
    }

    private void initDepartmentLoader() {
        AppUtil.showProgressDialog(getActivity());
        operation = NetworkUtility.DEPARTMENT_DETAIL;
        postData = new MultipartBody.Builder();
        getLoaderManager().restartLoader(LOADER_ID, null, this).forceLoad();
    }

    private void finishDepartmentLoader(String data) {

        if (data.isEmpty()) {
            AppUtil.showCustomDialog(getActivity(), "Something Went Wrong", "Failed to connect to server.", true, R.drawable.ic_error_outline);
        } else {
            byte[] decode = Base64.decode(data.getBytes(), Base64.DEFAULT);
            String jsonString = new String(decode);
            Log.d(TAG, "onLoadFinished: JsonResponse " + jsonString);
            departmentDetailArrayList = JSONExtractor.extractDepartmentDetailResponse(jsonString);
            Log.d(TAG, "finishDepartmentLoader: departmentArraySize: " + departmentDetailArrayList.size());
            if (!(departmentDetailArrayList.size() == 0)) {
                Log.d(TAG, "finishLoginLoader: startFragment DepartmentSelectorFragment");
                initDepartmentSpinner();

            } else {
                AppUtil.showCustomDialog(getActivity(), "Something Went Wrong", "server.under.maintenanceException", true, R.drawable.ic_error_outline);
            }
        }
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        return new DepartmentAsyncLoader(getActivity(), postData, operation);
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        AppUtil.hideProgressDialog();
        finishDepartmentLoader(data);
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {

    }


    static class DepartmentAsyncLoader extends AsyncTaskLoader<String> {
        private static final String TAG = "DepartmentAsyncLoader";
        MultipartBody.Builder postData;
        String operation;

        public DepartmentAsyncLoader(Context context, MultipartBody.Builder postData, String operation) {
            super(context);
            this.postData = postData;
            this.operation = operation;
        }

        @Override
        public String loadInBackground() {
            String response = NetworkUtility.getServerResponse(getContext(), operation, postData);
            Log.d(TAG, "loadInBackground:" + response);
            return response;
        }
    }
}
