package work.ravi.com.ams.ui.fragment.selector;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import work.ravi.com.ams.R;
import work.ravi.com.ams.adapter.SubjectSelectorAdapter;
import work.ravi.com.ams.databinding.FragmentSubjectSelectionBinding;
import work.ravi.com.ams.databinding.SubjectRightsDialogBinding;
import work.ravi.com.ams.pojo.SubjectBySubjectRight;
import work.ravi.com.ams.sharedprefrence.LoginSession;
import work.ravi.com.ams.util.AppUtil;
import work.ravi.com.ams.util.JSONExtractor;
import work.ravi.com.ams.util.NetworkUtility;

import static android.content.ContentValues.TAG;

/**
 * Created by Ravi on 04-03-2018.
 */

public class SubjectSelectorFragment extends Fragment implements View.OnClickListener, LoaderManager.LoaderCallbacks<String> {

    private static String SUBJECT_KEY = "subject_right";
    private static String TABLE_NAME = "table_name";
    private FragmentSubjectSelectionBinding fragmentSubjectSelectionBinding;
    private SubjectRightsDialogBinding subjectRightsDialogBinding;
    private EditText editTextSubjectKey;
    private AlertDialog aBuilder;
    private String operation, tableName;
    private MultipartBody.Builder postData;
    private int LOADER_ID = 0;
    private String mStringSubjectKey, section;
    private ArrayList<SubjectBySubjectRight> subjectBySubjectRights = new ArrayList<>();
    private RecyclerView mRecyclerViewSubjects;
    private String facultyKey;
    private boolean isFacultyEnteringKey = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        fragmentSubjectSelectionBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_subject_selection, container, false);
        mRecyclerViewSubjects = fragmentSubjectSelectionBinding.recyclerViewSubjectsRights;
        facultyKey = LoginSession.getInstance(getActivity()).getFacultyKey();
        mStringSubjectKey = facultyKey;
        postData = new MultipartBody.Builder();
        getBundleExtras();
        if (facultyKey.equals("")) {
            isFacultyEnteringKey = true;
            addSubjectRightDialog(getActivity());
        } else {
            initSubjectSelectorLoader(facultyKey);
        }
        return fragmentSubjectSelectionBinding.getRoot();
    }


    private void getBundleExtras() {
        String department = getArguments().getString(DepartmentSelectorFragment.DEPARTMENT);
        String year = getArguments().getString(DepartmentSelectorFragment.YEAR);
        section = getArguments().getString(DepartmentSelectorFragment.SECTION);
        tableName = department + "_" + year;
        Log.d(TAG, "getBundleExtras: " + department + year + section);
    }

    public void addSubjectRightDialog(Context context) {
        subjectRightsDialogBinding = DataBindingUtil
                .inflate(LayoutInflater.from(context), R.layout.subject_rights_dialog, (ViewGroup) fragmentSubjectSelectionBinding.getRoot(), false);
        View subCatView = subjectRightsDialogBinding.getRoot();
        Button buttonSubmit = subjectRightsDialogBinding.btnSubmit;
        Button buttonClose = subjectRightsDialogBinding.btnCancle;
        editTextSubjectKey = subjectRightsDialogBinding.editTextSubjectRights;
        aBuilder = new AlertDialog.Builder(context).create();
        aBuilder.setView(subCatView);
        aBuilder.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        aBuilder.show();
        aBuilder.setCancelable(false);
        buttonClose.setOnClickListener(this);
        buttonSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_cancle:
                aBuilder.dismiss();
                getFragmentManager().popBackStack();
                //Toast.makeText(getActivity(), "Please enter your subject key", Toast.LENGTH_LONG).show();
                break;
            case R.id.btn_submit:
                mStringSubjectKey = editTextSubjectKey.getText().toString().trim();
                if (TextUtils.isEmpty(mStringSubjectKey)) {
                    Toast.makeText(getActivity(), "Enter your subject key", Toast.LENGTH_LONG).show();
                } else {
                    if (AppUtil.isInternetIsAvailable(getActivity())) {
                        LoginSession.getInstance(getActivity()).setFacultyKey(mStringSubjectKey);
                        initSubjectSelectorLoader(mStringSubjectKey);
                    } else {
                        AppUtil.showNoInternetDialog(getActivity());
                    }
                }
                break;
        }
    }

    public void initSubjectSelectorLoader(String subjectKey) {
        AppUtil.showProgressDialog(getActivity());
        postData
                .addFormDataPart(SUBJECT_KEY, subjectKey)
                .addFormDataPart(TABLE_NAME, tableName);
        operation = NetworkUtility.SUBJECT_RIGHTS;
        getLoaderManager().restartLoader(LOADER_ID, null, this).forceLoad();
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        return new SubjectSelectorAsyncLoader(getActivity(), operation, postData);
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        Log.d(TAG, "onLoadFinished: " + data);
        finishSubjectLoader(data);
    }

    private void finishSubjectLoader(String data) {
        AppUtil.hideProgressDialog();
        byte[] decode = Base64.decode(data.getBytes(), Base64.DEFAULT);
        String jsonString = new String(decode);
        if (data.isEmpty()) {
            AppUtil.showCustomDialog(getActivity(), "Something Went Wrong", "Failed to connect to server.", true, R.drawable.ic_error_outline);
        } else {

            String[] response = JSONExtractor.getStatusMessage(jsonString);
            String status = response[0];
            String message = response[1];
            if (status.equals("0")) {
                AppUtil.showCustomDialog(getActivity(), "Failed", message, true, R.drawable.ic_error_outline);
            } else {
                Log.d(TAG, "onLoadFinished: JsonResponse " + jsonString);
                subjectBySubjectRights = JSONExtractor.extractSubjectDetailBySubjectRightResponse(jsonString);
                Log.d(TAG, "finishSubjectLoader: departmentArraySize: " + subjectBySubjectRights.size());
                if (!(subjectBySubjectRights.size() == 0)) {
                    Log.d(TAG, "finishLoginLoader: initalze recycler view");
                    if (isFacultyEnteringKey)
                        aBuilder.dismiss();
                    initSubjectList(subjectBySubjectRights);
                } else {
                    AppUtil.showCustomDialog(getActivity(), "Something Went Wrong", "server.under.maintenanceException", true, R.drawable.ic_error_outline);
                }
            }
        }
    }

    private void initSubjectList(ArrayList<SubjectBySubjectRight> subjectBySubjectRights) {
        RecyclerView.LayoutManager mLinearLayoutManager = new LinearLayoutManager(getActivity()
                .getParent());
        SubjectSelectorAdapter subjectSelectorAdapter = new SubjectSelectorAdapter(getActivity(), subjectBySubjectRights, getLoaderManager(), tableName, section, mStringSubjectKey);
        mRecyclerViewSubjects.setLayoutManager(mLinearLayoutManager);
        mRecyclerViewSubjects.setItemAnimator(new DefaultItemAnimator());
        mRecyclerViewSubjects.setAdapter(subjectSelectorAdapter);
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {

    }

    static class SubjectSelectorAsyncLoader extends AsyncTaskLoader<String> {
        String operation;
        MultipartBody.Builder postData;

        public SubjectSelectorAsyncLoader(Context context, String operation,
                                          MultipartBody.Builder postData) {
            super(context);
            this.operation = operation;
            this.postData = postData;
        }

        @Override
        public String loadInBackground() {
            String response = NetworkUtility.getServerResponse(getContext(), operation, postData);
            return response;
        }
    }
}
