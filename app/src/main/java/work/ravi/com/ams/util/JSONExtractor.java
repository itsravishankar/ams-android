package work.ravi.com.ams.util;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import work.ravi.com.ams.pojo.AllStudentList;
import work.ravi.com.ams.pojo.DepartmentDetail;
import work.ravi.com.ams.pojo.EnrolledStudentList;
import work.ravi.com.ams.pojo.FacultyLoginDetail;
import work.ravi.com.ams.pojo.HodLoginDetail;
import work.ravi.com.ams.pojo.HolidayDetail;
import work.ravi.com.ams.pojo.StudentDetail;
import work.ravi.com.ams.pojo.SubjectBySubjectRight;
import work.ravi.com.ams.pojo.SubjectDetail;
import work.ravi.com.ams.pojo.TotalAttendanceDetail;


/**
 * This class will help to extract different json received from server.
 * Created by Akash Kumar on 23-Dec-17.
 */


public final class JSONExtractor {
    private static final String TAG = "JSONExtractor";

    /**
     * This class is for utility, there is no need to make object of this class.
     */
    private JSONExtractor() {
    }


    public static String[] getStatusMessage(String serverResponse) {
        String status = "0", message = "Malformed Data Received. Please try again later.";
        try {
            JSONObject jsonObject = new JSONObject(serverResponse);
            status = String.valueOf(jsonObject.getInt("status"));
            message = jsonObject.getString("msg");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return new String[]{status, message};
    }

    public static boolean isJSON(String response) {
        return response.startsWith("{") && response.endsWith("}");
    }

    public static FacultyLoginDetail extractFacultyLoginResponse(String jsonString) {
        FacultyLoginDetail facultyLoginDetail = null;
        try {
            JSONObject rootJO = new JSONObject(jsonString);
            JSONObject dataJO = rootJO.getJSONObject("data");

            String facultyDep = dataJO.getString("faculty_department");
            String facultyName = dataJO.getString("faculty_name");
            String facultyPassword = dataJO.getString("faculty_password");
            String facultyEmail = dataJO.getString("faculty_email");
            String facultyMob = dataJO.getString("faculty_mob_no");
            String facultySpec = dataJO.getString("faculty_specialization");
            facultyLoginDetail = new FacultyLoginDetail(facultyDep, facultyName, facultyPassword, facultyEmail, facultyMob, facultySpec);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return facultyLoginDetail;
    }

    public static HodLoginDetail extractHodLoginResponce(String jsonString) {
        HodLoginDetail hodLoginDetail = null;
        try {
            JSONObject rootJO = new JSONObject(jsonString);
            JSONObject dataJO = rootJO.getJSONObject("data");

            String hodDepartment = dataJO.getString("department");
            String hodName = dataJO.getString("hod_name");
            String hodEmail = dataJO.getString("email");
            String departmentKey = dataJO.getString("dep_key");
            String publicKey = dataJO.getString("public_key");
            hodLoginDetail = new HodLoginDetail(hodDepartment, hodName, hodEmail, departmentKey, publicKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return hodLoginDetail;
    }

    public static StudentDetail extractStudentResponse(String jsonString) {


        StudentDetail studentDetail = null;
        try {
            JSONObject rootJO = new JSONObject(jsonString);
            JSONObject dataJO = rootJO.getJSONObject("data");

            String studentRollno = dataJO.getString("student_roll_no");
            String studentName = dataJO.getString("student_name");
            String studentMob = dataJO.getString("student_mob_no");
            String studentGroup = dataJO.getString("student_group");
            String studentFatherName = dataJO.getString("student_fname");
            String studentEmail = dataJO.getString("student_email");
            String studentFatherMob = dataJO.getString("student_fmob_no");
            String studentImgUrl = dataJO.getString("student_pic_url");
            studentDetail = new StudentDetail(studentRollno, studentName, studentMob, studentGroup, studentFatherName, studentEmail, studentFatherMob, studentImgUrl);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return studentDetail;
    }

    public static ArrayList<DepartmentDetail> extractDepartmentDetailResponse(String jsonString) {

        ArrayList<DepartmentDetail> departmentDetailArrayList = new ArrayList<>();

        try {
            JSONObject rootJO = new JSONObject(jsonString);
            JSONArray dataJA = rootJO.getJSONArray("data");

            for (int i = 0; i < dataJA.length(); i++) {
                JSONObject jsonObject = dataJA.getJSONObject(i);
                String departmentId = jsonObject.getString("department_id");
                String departmentName = jsonObject.getString("department_name");
                String departmentSrct = jsonObject.getString("department_srct");
                String departmentKey = jsonObject.getString("department_key");
                String firstYear = jsonObject.getString("first_year");
                String secondYear = jsonObject.getString("second_year");
                String thirdYear = jsonObject.getString("third_year");
                String fourthYear = jsonObject.getString("fourth_year");
                DepartmentDetail departmentDetail = new DepartmentDetail(departmentId, departmentName, departmentSrct, departmentKey, firstYear, secondYear, thirdYear, fourthYear);
                departmentDetailArrayList.add(departmentDetail);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return departmentDetailArrayList;
    }

    /*json extractor for subject*/
    public static ArrayList<SubjectBySubjectRight> extractSubjectDetailBySubjectRightResponse(String jsonString) {

        ArrayList<SubjectBySubjectRight> subjectBySubjectRights = new ArrayList<>();

        try {
            JSONObject rootJO = new JSONObject(jsonString);
            JSONArray dataJA = rootJO.getJSONArray("data");

            for (int i = 0; i < dataJA.length(); i++) {
                JSONObject jsonObject = dataJA.getJSONObject(i);
                String subjectName = jsonObject.getString("subject_name");
                String subjectId = jsonObject.getString("subject_id");
                String subjectShortcut = jsonObject.getString("subject_srct");
                SubjectBySubjectRight subjectBySubjectRight = new SubjectBySubjectRight(subjectName, subjectId, subjectShortcut);
                subjectBySubjectRights.add(subjectBySubjectRight);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return subjectBySubjectRights;
    }

    public static ArrayList<SubjectDetail> extractSubjectDetailResponse(String jsonString) {
        ArrayList<SubjectDetail> subjectDetailArrayList = new ArrayList<>();
        try {
            JSONObject rootJO = new JSONObject(jsonString);
            JSONArray dataJA = rootJO.getJSONArray("data");

            for (int i = 0; i < dataJA.length(); i++) {
                JSONObject jsonObject = dataJA.getJSONObject(i);

                String subjectId = jsonObject.getString("subject_id");
                String subjectName = jsonObject.getString("subject_name");
                String subjectShortcut = jsonObject.getString("subject_srct");
                String subjectFaculty = jsonObject.getString("faculty_name");
                String subjectRights = jsonObject.getString("subject_rights");
                String subjectType = jsonObject.getString("subject_type");
                SubjectDetail subjectDetail = new SubjectDetail(subjectId, subjectName, subjectShortcut, subjectFaculty, subjectRights, subjectType);
                subjectDetailArrayList.add(subjectDetail);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return subjectDetailArrayList;
    }

    public static ArrayList<StudentDetail> extractStudentAllDetailResponse(String jsonString) {

        ArrayList<StudentDetail> studentDetailArrayList = new ArrayList<>();
        HashMap<String, StudentDetail> studentDetailHashMap = new HashMap<>();

        try {
            JSONObject rootJO = new JSONObject(jsonString);
            JSONArray dataJA = rootJO.getJSONArray("data");

            for (int i = 0; i < dataJA.length(); i++) {
                JSONObject dataJO = dataJA.getJSONObject(i);
                String studentRollno = dataJO.getString("student_roll_no");
                String studentName = dataJO.getString("student_name");
                String studentMob = dataJO.getString("student_mob_no");
                String studentGroup = dataJO.getString("student_group");
                String studentFatherName = dataJO.getString("student_fname");
                String studentEmail = dataJO.getString("student_email");
                String studentFatherMob = dataJO.getString("student_fmob_no");
                String studentImgUrl = dataJO.getString("student_pic_url");
                StudentDetail studentDetail = new StudentDetail(studentRollno, studentName, studentMob, studentGroup, studentFatherName, studentEmail, studentFatherMob, studentImgUrl);
                studentDetailArrayList.add(studentDetail);
                studentDetailHashMap.put(studentRollno, studentDetail);
            }
            AllStudentList.getInstance().setStudentList(studentDetailHashMap);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return studentDetailArrayList;
    }

    public static ArrayList<EnrolledStudentList> extractEnrolledStudentsDetailResponse(String jsonString) {

        ArrayList<EnrolledStudentList> enrolledStudentLists = new ArrayList<>();

        try {
            JSONObject rootJO = new JSONObject(jsonString);
            JSONArray dataJA = rootJO.getJSONArray("data");

            for (int i = 0; i < dataJA.length(); i++) {
                JSONObject dataJO = dataJA.getJSONObject(i);
                String rollNo = dataJO.getString("roll_no");
                String id = dataJO.getString("id");
                EnrolledStudentList enrolledStudentList = new EnrolledStudentList(id, rollNo);
                enrolledStudentLists.add(enrolledStudentList);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return enrolledStudentLists;
    }

    public static ArrayList<HolidayDetail> extractHolidayResponse(String jsonString) {
        ArrayList<HolidayDetail> holidayDetailArrayList = new ArrayList<>();
        try {
            JSONObject rootJO = new JSONObject(jsonString);
            JSONArray dataJA = rootJO.getJSONArray("data");

            for (int i = 0; i < dataJA.length(); i++) {
                JSONObject jsonObject = dataJA.getJSONObject(i);
                String holidayDate = jsonObject.getString("date");
                String holidayName = jsonObject.getString("holiday_name");
                String description = jsonObject.getString("description");

                HolidayDetail holidayDetail = new HolidayDetail(holidayDate, holidayName, description);
                holidayDetailArrayList.add(holidayDetail);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return holidayDetailArrayList;
    }

    public static String extractCurrentDateResponse(String jsonString) {
        String currentDate = null;
        try {
            JSONObject rootJO = new JSONObject(jsonString);
            currentDate = rootJO.getString("msg");
            Log.d(TAG, "extractCurrentDateResponse: " + currentDate);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return currentDate;
    }

    public static ArrayList<TotalAttendanceDetail> extractTotalAttendanceResponse(String jsonString) {
        ArrayList<TotalAttendanceDetail> totalAttendanceDetailArrayList = new ArrayList<>();
        try {
            JSONObject rootJO = new JSONObject(jsonString);
            JSONArray dataJA = rootJO.getJSONArray("data");

            for (int i = 0; i < dataJA.length(); i++) {
                JSONObject jsonObject = dataJA.getJSONObject(i);
                int present = jsonObject.getInt("present");
                int absent = jsonObject.getInt("absent");
                int total = jsonObject.getInt("total");

                Log.d(TAG, "extractTotalAttendanceResponse: " + present + " " + absent);
                TotalAttendanceDetail totalAttendanceDetail = new TotalAttendanceDetail(absent, present, total);
                totalAttendanceDetailArrayList.add(totalAttendanceDetail);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return totalAttendanceDetailArrayList;
    }

    public static ArrayList<String> extractMappedStudentResponse(String jsonString) {
        ArrayList<String> rollNo = new ArrayList<>();
        try {
            JSONObject rootJO = new JSONObject(jsonString);
            JSONArray dataJA = rootJO.getJSONArray("msg");
            for (int i = 0; i < dataJA.length(); i++) {
                JSONObject jsonObject = dataJA.getJSONObject(i);
                String roll_no = jsonObject.getString("roll_no");
                rollNo.add(roll_no);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return rollNo;
    }

    public static ArrayList<TotalAttendanceDetail> extractGenerateReportResponse(String jsonString, String rollno) {
        ArrayList<TotalAttendanceDetail> totalAttendanceDetailArrayList = new ArrayList<>();
        try {
            JSONObject rootJO = new JSONObject(jsonString);
            JSONObject dataJO = rootJO.getJSONObject("data");

            JSONArray rollNo = dataJO.getJSONArray(rollno);

            for (int i = 0; i < rollNo.length(); i++) {
                JSONObject jsonObject = rollNo.getJSONObject(i);
                int present = jsonObject.getInt("present");
                int absent = jsonObject.getInt("absent");
                int total = jsonObject.getInt("total");

                Log.d(TAG, "extractTotalAttendanceResponse: " + present + " " + absent);
                TotalAttendanceDetail totalAttendanceDetail = new TotalAttendanceDetail(absent, present, total);
                totalAttendanceDetailArrayList.add(totalAttendanceDetail);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return totalAttendanceDetailArrayList;
    }
}
