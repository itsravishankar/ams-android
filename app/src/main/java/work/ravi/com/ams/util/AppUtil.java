package work.ravi.com.ams.util;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import work.ravi.com.ams.R;


public class AppUtil {
    private static final String TAG = "AppUtil";

    private AppUtil() { //Does not need to make instance of this class
    }

    /**
     * Check Device is connected to the Internet or not.
     *
     * @return <b>true</b> if device is Connected to Internet, <br>
     * <b>false</b> if device is NOT Connected to Internet
     */

    public static boolean isInternetIsAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;

        if (connectivityManager != null)
            networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public static void hideKeypad(Context context, View currentFocus) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        boolean hideSuccessfully = false;

        if (inputMethodManager == null) {
            Log.d(TAG, "hideKeypad: inputmethod manager is null");
        } else {
            Log.d(TAG, "hideKeypad: inputmethod manager is NOT null");
        }

        if (currentFocus == null) {
            Log.d(TAG, "hideKeypad: current focus is null");
        } else {
            Log.d(TAG, "hideKeypad: current focus^ is NOT null");
        }

        if (currentFocus != null && currentFocus instanceof EditText && inputMethodManager != null) {
            hideSuccessfully = inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
        }
        Log.d(TAG, "hideKeypad: keypad hided successfully? " + hideSuccessfully);
    }

    /**
     * This method will show home button(back button) on the action bar.<br>
     * Optionally set the icon if specified.
     *
     * @param context       Context of the activity.
     * @param setCustomIcon True if you want to set custom home icon.
     * @param drawableID    Drawable icon ID. eg. R.drawable.arrow_down.
     */

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public static void showHomeButton(Context context, boolean setCustomIcon, int drawableID) {
        ActionBar actionBar = ((Activity) context).getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);

            if (setCustomIcon)
                actionBar.setHomeAsUpIndicator(drawableID);
        }
    }

    /**
     * This method will set the title if action bar is NOT null.
     *
     * @param context Context of the activity.
     */

    public static void setTitle(Context context, String title) {
        ActionBar actionBar = ((Activity) context).getActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    public static String sharingUrl(Context context) {
        return "https://play.google.com/store/apps/details?id=" + context.getPackageName();
    }


    public static void showNoInternetDialog(final Context context) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
        builder.setTitle("NO INTERNET!");
        builder.setIcon(R.drawable.ic_internet);
        builder.setNegativeButton("Setting", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                ((AppCompatActivity) context).finish();
            }
        });
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setMessage("Take a Jio sim or Hack your neighbour's wifi. :)");
        builder.show();
    }

    public static void showCustomDialog(Context context, String title, String message, boolean showIcon, int iconID) {
        Log.d(TAG, "showCustomDialog: is called");
        AlertDialog.Builder aBuilder = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message);

        aBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent("finish_activity"));
                dialogInterface.cancel();
            }
        });

        if (showIcon)
            aBuilder.setIcon(ContextCompat.getDrawable(context, iconID));

        aBuilder.show();

    }


    /**
     * Used to hide the action bar
     *
     * @param context Activity Context
     */
    public static void hideActionBar(Context context) {
        try {
            ((Activity) context).getActionBar().hide();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Used to show the action bar
     *
     * @param context Activity Context
     */
    public static void showActionBar(Context context) {
        try {
            ((Activity) context).getActionBar().show();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    public static boolean isValidEmail(Context context, String email) {
        if (email.contains("@") && email.contains(".") && !email.contains("$")) {
            return true;
        }
        return false;
    }

    public static boolean isValidCompanyPhone(Context context, String mobile) {
        String companyPhoneRegex = context.getString(R.string.mobile_regex);

        Pattern mPattern = Pattern.compile(companyPhoneRegex);
        Matcher mMatcher = mPattern.matcher(mobile);

        if (mMatcher.matches()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isValidMobile(Context context, String mobile) {
        String mobileRegex = context.getString(R.string.mobile_regex);

        Pattern mPattern = Pattern.compile(mobileRegex);
        Matcher mMatcher = mPattern.matcher(mobile);

        if (mMatcher.matches()) {
            return true;
        } else {
            return false;
        }
    }

    public static String getSpValue(Context context, String spNameKey) {
        String[] spArr = spNameKey.split("/");

        SharedPreferences sp = context.getSharedPreferences(spArr[0], Context.MODE_PRIVATE);
        if (sp.getAll().isEmpty()) {
            return "";
        }

        return sp.getString(spArr[1], "");
    }

    public static String extractBase64(String data) {
        Log.d(TAG, "extractBase64: ");
        String decodedString = "";
        try {
            byte[] decode = Base64.decode(data, Base64.DEFAULT);

            decodedString = new String(decode);

        } catch (IllegalArgumentException e) {
            Log.e(TAG, "extractBase64: Unable to Extract Base64 ", e);
        }
        return decodedString;
    }

    public static boolean isJSON(String response) {
        return response.startsWith("{") && response.endsWith("}");
    }

    public static void initiaUnivarsalImageLoader(Context context) {
        // UNIVERSAL IMAGE LOADER SETUP
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisc(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                context)
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();

        ImageLoader.getInstance().init(config);
        // END - UNIVERSAL IMAGE LOADER SETUP
    }

    public static void setUnivarsalImageLoader(ImageView imageView, String url, int cornerRadiusPixels) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .displayer(new RoundedBitmapDisplayer(cornerRadiusPixels))
                .cacheInMemory(true).cacheOnDisc(true)
                .showImageForEmptyUri(R.drawable.cross_mark)
                .showImageOnFail(R.drawable.question_mark)
                .showImageOnLoading(android.R.color.transparent)
                .bitmapConfig(Bitmap.Config.RGB_565).build();

        imageLoader.displayImage(url, imageView, options);
    }

    private static AlertDialog progressDialog = null;

    public static void showProgressDialog(Context context) {
        if (progressDialog == null)
            progressDialog = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.ProgressDialogTheme)).create();

        progressDialog.setView(LayoutInflater.from(context).inflate(R.layout.progress_dialog_layout, null));
        progressDialog.show();
    }

    public static void showProgressDialog(Context context, String text) {
        if (progressDialog == null)
            progressDialog = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.ProgressDialogTheme)).create();

        View dialogLayout = LayoutInflater.from(context).inflate(R.layout.progress_dialog_layout, null);
        ((TextView) dialogLayout.findViewById(R.id.progress_dialog_text_view)).setText(text);
        progressDialog.setView(dialogLayout);
        progressDialog.show();
    }

    public static void showProgressDialog(Context context, String title, boolean isCancelable) {
        if (progressDialog == null)
            progressDialog = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.ProgressDialogTheme)).create();

        View dialogLayout = LayoutInflater.from(context).inflate(R.layout.progress_dialog_layout, null);
        ((TextView) dialogLayout.findViewById(R.id.progress_dialog_text_view)).setText(title);
        progressDialog.setCancelable(isCancelable);
        progressDialog.setView(dialogLayout);
        progressDialog.show();
    }

    public static void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

}
