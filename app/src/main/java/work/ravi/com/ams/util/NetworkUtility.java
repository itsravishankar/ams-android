package work.ravi.com.ams.util;

import android.content.Context;
import android.util.Log;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import work.ravi.com.ams.R;

/**
 * Created by Akash Kumar on 05-Dec-17.
 */

public final class NetworkUtility {
    private static final String TAG = "NetworkUtility";
    public static final String LOGIN = "LOGIN";
    public static final String STUDENT_DETAIL = "STUDENT_DETAIL";
    public static final String STUDENT_ALL_DETAIL = "STUDENT_ALL_DETAIL";
    public static final String DEPARTMENT_DETAIL = "DEPARTMENT_DETAIL";
    public static final String SUBJECT_RIGHTS = "SUBJECT_RIGHTS";
    public static final String ALL_SUBJECT = "ALL_SUBJECT";
    public static final String HOLIDAY_DETAIL = "HOLIDAY_DETAIL";
    public static final String MARK_ATTENDANCE = "MARK_ATTENDANCE";
    public static final String TOTAL_ATTENDANCE = "TOTAL_ATTENDANCE";
    public static final String GENERATE_REPORT = "GENERATE_REPORT";

    // This is utility class, no need to make a constructor of this class
    private NetworkUtility() {
    }

    /**
     * This method will return the <b>Response</b> from the Servcr if nothing goes wrong.<br>
     * Supported operations: LOGIN, SIGNUP, FETCH_ANNOUNCEMENT, FORGET_PASSWORD
     *
     * @param operation type of operation to be performed.
     * @return Return the response getting from the server, or empty string if anything goes wrong.
     */

    public static String getServerResponse(Context context, String operation, MultipartBody.Builder mBuilder) {

        String url = getApiUrl(context);

        mBuilder = putApiDetail(context, operation, mBuilder);

        OkHttpClient okHttpClient = new OkHttpClient();

        RequestBody requestBody = mBuilder.setType(MediaType.parse("multipart/form-data")).build();

        Request request = new Request.Builder()
                .method("POST", requestBody)
                .url(url)
                .build();

        Response response = null;
        String serverResponse = "";

        try {
            response = okHttpClient.newCall(request).execute();
            serverResponse = response.body().string();
            Log.d(TAG, "getServerResponse: " + serverResponse);
        } catch (IOException e) {
            Log.e(TAG, "NetworkUtility getServerResponse: Failed to execute request", e);
            e.printStackTrace();
        } finally {
            if (response != null)
                response.close();
        }

        return serverResponse;
    }


    /**
     * This method will return the <b>URL</b> for the specified operation.<br>
     * Supported operations: LOGIN, SIGNUP, FETCH_ANNOUNCEMENT, FORGET_PASSWORD
     *
     * @return Full Url to the server if operation is supported else an empty string.
     */

    public static String getApiUrl(Context context) {
        return context.getString(R.string.api_url);
    }


    public static MultipartBody.Builder putApiDetail(Context context, String operation, MultipartBody.Builder postBuilder) {

        switch (operation.trim()) {
            case LOGIN: {
                postBuilder.addFormDataPart("sname", context.getString(R.string.login_sname));
                postBuilder.addFormDataPart("fname", context.getString(R.string.login_fname));
                break;
            }
            case STUDENT_DETAIL: {
                postBuilder.addFormDataPart("sname", context.getString(R.string.student_detail_sname));
                postBuilder.addFormDataPart("fname", context.getString(R.string.student_detail_fname));
                break;
            }
            case STUDENT_ALL_DETAIL: {
                postBuilder.addFormDataPart("sname", context.getString(R.string.student_detail_sname));
                postBuilder.addFormDataPart("fname", context.getString(R.string.student_all_detail_fname));
                break;
            }
            case DEPARTMENT_DETAIL: {
                postBuilder.addFormDataPart("sname", context.getString(R.string.department_sname));
                postBuilder.addFormDataPart("fname", context.getString(R.string.department_fname));
                break;
            }
            case HOLIDAY_DETAIL: {
                postBuilder.addFormDataPart("sname", context.getString(R.string.holiday_sname));
                postBuilder.addFormDataPart("fname", context.getString(R.string.holiday_fname));
                break;
            }
            case SUBJECT_RIGHTS: {
                postBuilder.addFormDataPart("sname", context.getString(R.string.subject_rights_sname));
                postBuilder.addFormDataPart("fname", context.getString(R.string.subject_rights_fname));
                break;
            }
            case ALL_SUBJECT: {
                postBuilder.addFormDataPart("sname", context.getString(R.string.subject_rights_sname));
                postBuilder.addFormDataPart("fname", context.getString(R.string.all_subject_fname));
                break;
            }
            case MARK_ATTENDANCE: {
                postBuilder.addFormDataPart("sname", context.getString(R.string.mark_attendance_sname));
                postBuilder.addFormDataPart("fname", context.getString(R.string.mark_attendance_fname));
                break;
            }
            case TOTAL_ATTENDANCE: {
                postBuilder.addFormDataPart("sname", context.getString(R.string.total_attendance_sname));
                postBuilder.addFormDataPart("fname", context.getString(R.string.total_attendance_fname));
                break;
            }
            case GENERATE_REPORT: {
                postBuilder.addFormDataPart("sname", context.getString(R.string.generate_report_sname));
                postBuilder.addFormDataPart("fname", context.getString(R.string.generate_report_fname));
                break;
            }
        }

        return postBuilder;
    }
}
