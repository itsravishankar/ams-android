package work.ravi.com.ams.pojo;

/**
 * Created by acer on 06-03-2018.
 */

public class HolidayDetail {
    private String holidayDate;
    private String holidayName;
    private String description;

    public HolidayDetail(String holidayDate, String holidayName, String description) {
        this.holidayDate = holidayDate;
        this.holidayName = holidayName;
        this.description = description;
    }

    public String getHolidayDate() {
        return holidayDate;
    }

    public void setHolidayDate(String holidayDate) {
        this.holidayDate = holidayDate;
    }

    public String getHolidayName() {
        return holidayName;
    }

    public void setHolidayName(String holidayName) {
        this.holidayName = holidayName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
