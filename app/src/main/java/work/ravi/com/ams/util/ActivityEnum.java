package work.ravi.com.ams.util;

/**
 * Created by Ravi on 06-03-2018.
 */

public enum  ActivityEnum {
    MARK_ATTENDANCE,
    UPLOAD_ATTENDANCE,
    NOTIFY_STUDENT,
    STUDENT_RECORD
}
