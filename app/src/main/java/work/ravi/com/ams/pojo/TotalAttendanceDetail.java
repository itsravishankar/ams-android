package work.ravi.com.ams.pojo;

/**
 * Created by acer on 14-03-2018.
 */

public class TotalAttendanceDetail {
    private int absent;
    private int present;
    private int total;

    public TotalAttendanceDetail(int absent, int present, int total) {
        this.absent = absent;
        this.present = present;
        this.total = total;
    }

    public int getAbsent() {
        return absent;
    }

    public void setAbsent(int absent) {
        this.absent = absent;
    }

    public int getPresent() {
        return present;
    }

    public void setPresent(int present) {
        this.present = present;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
