package work.ravi.com.ams.ui.activity;

import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Loader;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import work.ravi.com.ams.R;
import work.ravi.com.ams.adapter.NotifyStudentAdapter;
import work.ravi.com.ams.adapter.ReportCardAdapter;
import work.ravi.com.ams.databinding.ReportCardBinding;
import work.ravi.com.ams.pojo.SubjectDetail;
import work.ravi.com.ams.pojo.TotalAttendanceDetail;
import work.ravi.com.ams.util.AppUtil;
import work.ravi.com.ams.util.JSONExtractor;
import work.ravi.com.ams.util.NetworkUtility;

import static android.content.ContentValues.TAG;

public class ReportCardActivity extends AppCompatActivity implements View.OnClickListener,LoaderManager.LoaderCallbacks<String>{


    private ReportCardBinding reportCardBinding;
    private String name;
    private String rollno;
    private String branch;
    private String year;
    private String section;
    private TextView nameTv,rollnoTv,branchTv,yearTv,sectionTv;
    private boolean isFirtLoader = false;
    private String operation;
    private MultipartBody.Builder postData;
    private int LOADER_ID = 0;
    private ArrayList<TotalAttendanceDetail> totalAttendanceDetail;
    private int totalSubjectSize;
    private float[] attendanceData;
    private float[] totalAttendanceData;
    private float[] absentData;
    private String[] attendanceDataLable;
    private String tableName;

    private ReportCardAdapter reportCardAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView reportCardRecyclerView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        reportCardBinding = DataBindingUtil.setContentView(this, R.layout.report_card);
        postData = new MultipartBody.Builder();
        totalAttendanceDetail = new ArrayList<>();
        getBundleExtras();
        bindView();
        printView();
        initGetSubjectDetail();
    }
    @Override
    public void onClick(View v) {

    }

    private void getBundleExtras() {
        branch = getIntent().getExtras().getString("repo_branch");
        year = getIntent().getExtras().getString("repo_year");
        section = getIntent().getExtras().getString("repo_section");
        rollno = getIntent().getExtras().getString("repo_rollno");
        name = getIntent().getExtras().getString("repo_name");
        tableName = branch + "_" + year;
    }

    private void bindView(){
        nameTv = reportCardBinding.reportCardStudentName;
        rollnoTv = reportCardBinding.reportCardStudentRoll;
        branchTv = reportCardBinding.reportCardBranch;
        yearTv = reportCardBinding.reportCardYear;
        sectionTv = reportCardBinding.reportCardSection;
        reportCardRecyclerView = reportCardBinding.reportCardRecyclerView;
    }

    private void printView(){
        nameTv.setText(name);
        rollnoTv.setText(rollno);
        branchTv.setText(branch.toUpperCase());
        yearTv.setText(year.toUpperCase());
        sectionTv.setText(section.toUpperCase());

        reportCardAdapter = new ReportCardAdapter(this, totalAttendanceDetail,attendanceDataLable);
        mLayoutManager = new GridLayoutManager(this, 1);
        reportCardRecyclerView.setLayoutManager(mLayoutManager);
        reportCardRecyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        reportCardRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }


    private void initGetSubjectDetail() {
        AppUtil.showProgressDialog(this);
        isFirtLoader = true;
        operation = NetworkUtility.ALL_SUBJECT;
        postData
                .addFormDataPart("table_name", tableName);
        getLoaderManager().restartLoader(LOADER_ID, null, this).forceLoad();
    }

    private void initDummyChartActivity() {

        AppUtil.showProgressDialog(this);
        operation = NetworkUtility.TOTAL_ATTENDANCE;
        postData
                .addFormDataPart("table_name", tableName)
                .addFormDataPart("roll_no", rollno)
                .addFormDataPart("section", section);
        getLoaderManager().restartLoader(LOADER_ID, null, this).forceLoad();

    }

    private void finishDummyChartActivity(String data) {
        AppUtil.hideProgressDialog();
        if (data.isEmpty()) {
            AppUtil.showCustomDialog(this, "Something Went Wrong", "Failed to connect to server.", true, R.drawable.ic_error_outline);
        } else {
            byte[] decode = Base64.decode(data.getBytes(), Base64.DEFAULT);
            String jsonString = new String(decode);

            String[] response = JSONExtractor.getStatusMessage(jsonString);
            String status = response[0];
            String message = response[1];
            if (status.equals("0")) {
                AppUtil.showCustomDialog(this, "Failed", message, true, R.drawable.ic_error_outline);
            } else {
                totalAttendanceDetail = JSONExtractor.extractTotalAttendanceResponse(jsonString);
                totalSubjectSize = totalAttendanceDetail.size();

                attendanceData = new float[totalSubjectSize];
                totalAttendanceData = new float[totalSubjectSize];
                absentData = new float[totalSubjectSize];

                for (int i = 0; i < totalSubjectSize; i++) {
                    attendanceData[i] = totalAttendanceDetail.get(i).getPresent();
                    totalAttendanceData[i] = totalAttendanceDetail.get(i).getTotal();
                    absentData[i] = totalAttendanceDetail.get(i).getAbsent();
                }
                reportCardAdapter = new ReportCardAdapter(this, totalAttendanceDetail,attendanceDataLable);
                reportCardRecyclerView.setAdapter(reportCardAdapter);
            }
        }
    }

    private void finishGetSubjectDetail(String data) {
        AppUtil.hideProgressDialog();
        if (data.isEmpty()) {
            AppUtil.showCustomDialog(this, "Something Went Wrong", "Failed to connect to server.", true, R.drawable.ic_error_outline);
        } else {
            byte[] decode = Base64.decode(data.getBytes(), Base64.DEFAULT);
            String jsonString = new String(decode);

            String[] response = JSONExtractor.getStatusMessage(jsonString);
            String status = response[0];
            String message = response[1];
            if (status.equals("0")) {
                AppUtil.showCustomDialog(this, "Failed", message, true, R.drawable.ic_error_outline);
            } else {
                ArrayList<SubjectDetail> subjectDetailArrayList = JSONExtractor.extractSubjectDetailResponse(jsonString);
                attendanceDataLable = new String[subjectDetailArrayList.size()];
                for (int i = 0; i < subjectDetailArrayList.size(); i++) {
                    attendanceDataLable[i] = subjectDetailArrayList.get(i).getSubjectSrct().toUpperCase();
                }
                initDummyChartActivity();
            }
        }
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        return new ReportCardAsyncLoader(this,postData,operation);
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        if (isFirtLoader) {
            isFirtLoader = false;
            finishGetSubjectDetail(data);
        } else {
            finishDummyChartActivity(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {

    }


    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        private GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /*converter dp to px*/
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }


    static class ReportCardAsyncLoader extends AsyncTaskLoader<String> {
        MultipartBody.Builder postData;
        String operation;
        public ReportCardAsyncLoader(Context context,MultipartBody.Builder postData,String operation) {
            super(context);
            this.postData = postData;
            this.operation = operation;
        }

        @Override
        public String loadInBackground() {
            String response = NetworkUtility.getServerResponse(getContext(),operation,postData);
            return response;
        }
    }
}
