package work.ravi.com.ams.ui.activity;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;

import work.ravi.com.ams.R;
import work.ravi.com.ams.databinding.ActivitySelectorBinding;
import work.ravi.com.ams.ui.fragment.selector.DepartmentSelectorFragment;

/**
 * Created by Ravi on 04-03-2018.
 */

public class SelectorActivity extends AppCompatActivity {

    private ActivitySelectorBinding activitySelectorBinding;
    private String checkActivityTrans;
    private BroadcastReceiver act2InitReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySelectorBinding = DataBindingUtil.setContentView(this, R.layout.activity_selector);
        checkActivityTrans = getIntent().getExtras().getString("activity_indicator");
        onDepartmentSelector();
        act2InitReceiver= new BroadcastReceiver()
        {

            @Override
            public void onReceive(Context context, Intent intent)
            {
                finish();
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(act2InitReceiver, new IntentFilter("finish_activity"));



    }

    private void onDepartmentSelector() {
        Bundle bundle = new Bundle();
        bundle.putString("activity_indicator", checkActivityTrans);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        DepartmentSelectorFragment departmentSelectorFragment = new DepartmentSelectorFragment();
        fragmentTransaction.replace(R.id.rl_fragment_container, departmentSelectorFragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        departmentSelectorFragment.setArguments(bundle);
        fragmentTransaction.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(act2InitReceiver);
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
        }
       /* else if(getFragmentManager().getBackStackEntryCount() == 1) {
            moveTaskToBack(false);
        }*/
        else {
            getFragmentManager().popBackStack();
        }
    }
}
