package work.ravi.com.ams.sharedprefrence;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import work.ravi.com.ams.pojo.MarkedAttendanceHolder;

/**
 * Created by Ravi on 3/16/2018.
 */

public class MarkedAttendanceRecords {
    private static final String ATTENDANCE_RECORD = "attendance_record";
    private static final MarkedAttendanceRecords ourInstance = new MarkedAttendanceRecords();
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor sEditor;

    public static MarkedAttendanceRecords getInstance(Context context) {

        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(ATTENDANCE_RECORD, Context.MODE_PRIVATE);
            sEditor = sharedPreferences.edit();
        }

        return ourInstance;
    }

    private MarkedAttendanceRecords() {
    }

    public void setAttendanceRecord(HashMap<String, MarkedAttendanceHolder> attendanceHashMap) {
        Gson gson = new Gson();
        String json = gson.toJson(attendanceHashMap);
        sEditor.putString("attendanceHashMap", json);
        sEditor.apply();
    }
    public HashMap<String, MarkedAttendanceHolder> getAttendanceRecord() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString("attendanceHashMap", null);
        Type type = new TypeToken<HashMap<String, MarkedAttendanceHolder>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public void setHashMapKeyName(ArrayList<String> hashMapKeyName) {
        Gson gson = new Gson();
        String json = gson.toJson(hashMapKeyName);
        sEditor.putString("hashMapKeyName", json);
        sEditor.apply();
    }
    public ArrayList<String> getHashMapKeyName() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString("hashMapKeyName", null);
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();
        return gson.fromJson(json, type);
    }
}
