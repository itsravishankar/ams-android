package work.ravi.com.ams.ui.activity.prelogin;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import work.ravi.com.ams.R;
import work.ravi.com.ams.databinding.ActivitySplashBinding;
import work.ravi.com.ams.sharedprefrence.LoginSession;
import work.ravi.com.ams.ui.activity.MainActivity;
import work.ravi.com.ams.ui.activity.StudentActivity;

/**
 * Created by Ravi on 29-12-2017.
 */

public class SplashActivity extends AppCompatActivity {

    ImageView mImageView;
    private ActivitySplashBinding activitySplashBinding;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySplashBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        mImageView = activitySplashBinding.imageSplash;
        startAnimation();
        startNextActivity();
    }

    private void startNextActivity() {

        int SPLASH_TIME = 1500;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(LoginSession.getInstance(SplashActivity.this).isLoggedIn()){
                    switch (LoginSession.getInstance(SplashActivity.this).getUserType()){

                        case 0:
                            Intent intentLaunchMainActivity= new Intent(
                                    SplashActivity.this, MainActivity.class);
                            intentLaunchMainActivity
                                    .putExtra(LoginActivity.USER_TYPE,
                                            LoginSession.getInstance(SplashActivity.this)
                                                    .getUserType()+"");
                            startActivity(intentLaunchMainActivity);
                            finish();
                            break;
                        case 1:
                            Intent intentLaunchMain= new Intent(
                                    SplashActivity.this, MainActivity.class);
                            intentLaunchMain
                                    .putExtra(LoginActivity.USER_TYPE,
                                            LoginSession.getInstance(SplashActivity.this)
                                                    .getUserType()+"");
                            startActivity(intentLaunchMain);
                            finish();
                            break;
                        case 2:
                            Intent intentLaunchStudentActivity= new Intent(
                                    SplashActivity.this, StudentActivity.class);
                            startActivity(intentLaunchStudentActivity);
                            finish();
                            break;
                    }

                }
                    else {
                    Intent intentLaunchLoginActivity = new Intent(
                            SplashActivity.this, PreLoginActivity.class);
                    startActivity(intentLaunchLoginActivity);
                    finish();
                }

            }
        }, SPLASH_TIME);
    }

    private void startAnimation() {
       Animation an = AnimationUtils.loadAnimation(this, R.anim.beat);
        an.reset();
        mImageView.clearAnimation();
        mImageView.startAnimation(an);
    }
}
