package work.ravi.com.ams.pojo;

/**
 * Created by acer on 02-03-2018.
 */

public class FacultyLoginDetail {
    private String facultyDep;
    private String facultyName;
    private String facultyPassword;
    private String facultyEmail;
    private String facultyMob;
    private String facultySpec;

    public FacultyLoginDetail(String facultyDep, String facultyName, String facultyPassword, String facultyEmail, String facultyMob, String facultySpec) {
        this.facultyDep = facultyDep;
        this.facultyName = facultyName;
        this.facultyPassword = facultyPassword;
        this.facultyEmail = facultyEmail;
        this.facultyMob = facultyMob;
        this.facultySpec = facultySpec;
    }

    public String getFacultyDep() {
        return facultyDep;
    }

    public void setFacultyDep(String facultyDep) {
        this.facultyDep = facultyDep;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public String getFacultyPassword() {
        return facultyPassword;
    }

    public void setFacultyPassword(String facultyPassword) {
        this.facultyPassword = facultyPassword;
    }

    public String getFacultyEmail() {
        return facultyEmail;
    }

    public void setFacultyEmail(String facultyEmail) {
        this.facultyEmail = facultyEmail;
    }

    public String getFacultyMob() {
        return facultyMob;
    }

    public void setFacultyMob(String facultyMob) {
        this.facultyMob = facultyMob;
    }

    public String getFacultySpec() {
        return facultySpec;
    }

    public void setFacultySpec(String facultySpec) {
        this.facultySpec = facultySpec;
    }
}
