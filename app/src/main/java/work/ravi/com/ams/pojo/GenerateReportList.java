package work.ravi.com.ams.pojo;

import java.util.ArrayList;

/**
 * Created by acer on 17-03-2018.
 */

public class GenerateReportList {
    private ArrayList<TotalAttendanceDetail> totalAttendanceDetail;
    private String rollNo;

    public GenerateReportList(ArrayList<TotalAttendanceDetail> totalAttendanceDetail, String rollNo) {
        this.totalAttendanceDetail = totalAttendanceDetail;
        this.rollNo = rollNo;
    }

    public ArrayList<TotalAttendanceDetail> getTotalAttendanceDetail() {
        return totalAttendanceDetail;
    }

    public void setTotalAttendanceDetail(ArrayList<TotalAttendanceDetail> totalAttendanceDetail) {
        this.totalAttendanceDetail = totalAttendanceDetail;
    }

    public String getRollNo() {
        return rollNo;
    }

    public void setRollNo(String rollNo) {
        this.rollNo = rollNo;
    }
}
