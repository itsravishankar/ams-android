package work.ravi.com.ams.ui.activity;

import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;
import okhttp3.MultipartBody;
import work.ravi.com.ams.R;
import work.ravi.com.ams.databinding.ActivityMainBinding;
import work.ravi.com.ams.pojo.HolidayDetail;
import work.ravi.com.ams.pojo.MarkedAttendanceHolder;
import work.ravi.com.ams.sharedprefrence.LoginSession;
import work.ravi.com.ams.sharedprefrence.MarkedAttendanceRecords;
import work.ravi.com.ams.ui.activity.prelogin.LoginActivity;
import work.ravi.com.ams.ui.activity.prelogin.PreLoginActivity;
import work.ravi.com.ams.util.AppUtil;
import work.ravi.com.ams.util.JSONExtractor;
import work.ravi.com.ams.util.NetworkUtility;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, LoaderManager.LoaderCallbacks<String>, WaveSwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "MainActivity";
    private Toolbar toolbar;
    private LinearLayout markAttendance, studentRecord, generateReport, uploadAttendance,profileView, notifyStudent;
    private WaveSwipeRefreshLayout waveSwipeRefreshLayout;
    private ActivityMainBinding activityMainBinding;
    private String sStringIntentExtra;
    private String operation;
    private MultipartBody.Builder postData;
    private int LOADER_ID = 0;
    private String currentDateDayTime = "";
    private ArrayList<HolidayDetail> holidayDetailArrayList = new ArrayList<>();
    private boolean isSunday = false;
    private boolean isHoliday = false;
    private boolean isNoNineToSixPM = false;
    private int holidayIndex;
    private boolean isRefreshing = false, isAppRuningFirstTime;
    final Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        if (LoginSession.getInstance(this).isAppRunningFirstTime()){
            ArrayList<String> hashMapKeyName = new ArrayList<>();
            HashMap<String, MarkedAttendanceHolder> markedAttendanceHolderHashMap = new HashMap<>();
            MarkedAttendanceRecords.getInstance(this).setAttendanceRecord(markedAttendanceHolderHashMap);
            MarkedAttendanceRecords.getInstance(this).setHashMapKeyName(hashMapKeyName);
            LoginSession.getInstance(this).setAppRunningFirstTime(false);
        }
        initCollapsingToolbar();
        FloatingActionButton fab = activityMainBinding.appBarMain.fab;

        sStringIntentExtra = getIntent().getStringExtra(LoginActivity.USER_TYPE);
        bindView(activityMainBinding);

        fab.setOnClickListener(view -> Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show());

        DrawerLayout drawer = activityMainBinding.drawerLayout;
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = activityMainBinding.navView;
        navigationView.setItemBackgroundResource(R.color.colorWhite);
        navigationView.setNavigationItemSelectedListener(this);

        postData = new MultipartBody.Builder();

        if (AppUtil.isInternetIsAvailable(this)) {
            initMainActivityLoader();
        } else {
            AppUtil.showNoInternetDialog(this);
        }
//2018-03-06 (Tue) 03:52:53
        markAttendance.setOnClickListener(this);
        studentRecord.setOnClickListener(this);
        uploadAttendance.setOnClickListener(this);
        waveSwipeRefreshLayout.setOnRefreshListener(this);
        generateReport.setOnClickListener(this);
        profileView.setOnClickListener(this);
        uploadAttendance.setOnClickListener(this);
        notifyStudent.setOnClickListener(this);

    }


    public void bindView(ActivityMainBinding activityMainBinding) {
        waveSwipeRefreshLayout = activityMainBinding.appBarMain.contentMain.mainSwipe;
        markAttendance = activityMainBinding.appBarMain.contentMain.markAttendanceLl;
        studentRecord = activityMainBinding.appBarMain.contentMain.studentRecordLl;
        generateReport = activityMainBinding.appBarMain.contentMain.generateReportLl;

        profileView = activityMainBinding.appBarMain.contentMain.profileViewLl;

        uploadAttendance = activityMainBinding.appBarMain.contentMain.llUploadAttendance;
        notifyStudent = activityMainBinding.appBarMain.contentMain.notifyStudentLl;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mark_attendance_ll: {
                if (false) {//TODO isSunday
                    AppUtil.showCustomDialog(this, "Sorry!", "There is no class in Sunday", true, R.drawable.ic_sentiment);
                } else {
                    if (false) {//TODO isNoNineToSixPM
                        AppUtil.showCustomDialog(this, "Sorry!", "Lecture time should be 9 to 6 pm", true, R.drawable.ic_sentiment);
                    } else {
                        if (isHoliday) {
                            AppUtil.showCustomDialog(this, holidayDetailArrayList.get(holidayIndex).getHolidayName(), holidayDetailArrayList.get(holidayIndex).getDescription(), true, R.drawable.ic_sentiment);
                        } else {
                            Intent intentSelector1 = new Intent(MainActivity.this, SelectorActivity.class);
                            intentSelector1.putExtra("activity_indicator", "mark_attendance");
                            startActivity(intentSelector1);
                        }
                    }
                }
                break;
            }
            case R.id.student_record_ll: {

                if (false) {//TODO isSunday
                    AppUtil.showCustomDialog(this, "Sorry!", "There is no class in Sunday", true, R.drawable.ic_sentiment);
                } else {
                    if (false) {//TODO isNoNineToSixPM
                        AppUtil.showCustomDialog(this, "Sorry!", "Lecture time should be 9 to 6 pm", true, R.drawable.ic_sentiment);
                    } else {
                        if (isHoliday) {
                            AppUtil.showCustomDialog(this, holidayDetailArrayList.get(holidayIndex).getHolidayName(), holidayDetailArrayList.get(holidayIndex).getDescription(), true, R.drawable.ic_sentiment);
                        } else {
                            Intent intentSelector2 = new Intent(MainActivity.this, SelectorActivity.class);
                            intentSelector2.putExtra("activity_indicator", "student_record");
                            startActivity(intentSelector2);
                            overridePendingTransition(R.anim.intent_enter_anim, R.anim.intent_exit_anim);
                        }
                    }
                }
                break;
            }
            case R.id.generate_report_ll: {

                if (false) {//TODO isSunday
                    AppUtil.showCustomDialog(this, "Sorry!", "There is no class in Sunday", true, R.drawable.ic_sentiment);
                } else {
                    if (false) {//TODO isNoNineToSixPM
                        AppUtil.showCustomDialog(this, "Sorry!", "Lecture time should be 9 to 6 pm", true, R.drawable.ic_sentiment);
                    } else {
                        if (isHoliday) {
                            AppUtil.showCustomDialog(this, holidayDetailArrayList.get(holidayIndex).getHolidayName(), holidayDetailArrayList.get(holidayIndex).getDescription(), true, R.drawable.ic_sentiment);

                        } else {//
                            Intent intentDummyChart = new Intent(MainActivity.this, SelectorActivity.class);
                            intentDummyChart.putExtra("activity_indicator","generate_report");
                            startActivity(intentDummyChart);
                            overridePendingTransition(R.anim.intent_enter_anim, R.anim.intent_exit_anim);
                        }
                    }
                }
                break;
            }

            case R.id.profile_view_ll: {

                if (false) {//TODO isSunday
                    AppUtil.showCustomDialog(this, "Sorry!", "There is no class in Sunday", true, R.drawable.ic_sentiment);
                } else {
                    if (false) {//TODO isNoNineToSixPM
                        AppUtil.showCustomDialog(this, "Sorry!", "Lecture time should be 9 to 6 pm", true, R.drawable.ic_sentiment);
                    } else {
                        if (isHoliday) {
                            AppUtil.showCustomDialog(this, holidayDetailArrayList.get(holidayIndex).getHolidayName(), holidayDetailArrayList.get(holidayIndex).getDescription(), true, R.drawable.ic_sentiment);
                        } else {

                            Intent intentProfile = new Intent(MainActivity.this, ProfileActivity.class);
                            intentProfile.putExtra(LoginActivity.USER_TYPE, sStringIntentExtra);
                            startActivity(intentProfile);
                            overridePendingTransition(R.anim.intent_enter_anim, R.anim.intent_exit_anim);

                        }
                    }
                }
                break;
            }

            case R.id.ll_upload_attendance: {

                if (false) {//TODO isSunday
                    AppUtil.showCustomDialog(this, "Sorry!", "There is no class in Sunday", true, R.drawable.ic_sentiment);
                } else {
                    if (false) {//TODO isNoNineToSixPM
                        AppUtil.showCustomDialog(this, "Sorry!", "Lecture time should be 9 to 6 pm", true, R.drawable.ic_sentiment);
                    } else {
                        if (isHoliday) {
                            AppUtil.showCustomDialog(this, holidayDetailArrayList.get(holidayIndex).getHolidayName(), holidayDetailArrayList.get(holidayIndex).getDescription(), true, R.drawable.ic_sentiment);
                        } else {

                            Intent intentSelector2 = new Intent(MainActivity.this, UploadAttendanceActivity.class);
                            startActivity(intentSelector2);
                            overridePendingTransition(R.anim.intent_enter_anim, R.anim.intent_exit_anim);

                        }
                    }
                }
                break;
            }
            case R.id.notify_student_ll: {

                if (false) {//TODO isSunday
                    AppUtil.showCustomDialog(this, "Sorry!", "There is no class in Sunday", true, R.drawable.ic_sentiment);
                } else {
                    if (false) {//TODO isNoNineToSixPM
                        AppUtil.showCustomDialog(this, "Sorry!", "Lecture time should be 9 to 6 pm", true, R.drawable.ic_sentiment);
                    } else {
                        if (isHoliday) {
                            AppUtil.showCustomDialog(this, holidayDetailArrayList.get(holidayIndex).getHolidayName(), holidayDetailArrayList.get(holidayIndex).getDescription(), true, R.drawable.ic_sentiment);

                        } else {//
                            Intent intentDummyChart = new Intent(MainActivity.this, SelectorActivity.class);
                            intentDummyChart.putExtra("activity_indicator","notify_student");
                            startActivity(intentDummyChart);
                            overridePendingTransition(R.anim.intent_enter_anim, R.anim.intent_exit_anim);
                        }
                    }
                }
                break;
            }
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = activityMainBinding.drawerLayout;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action1_2) {
            Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
            intent.putExtra(LoginActivity.USER_TYPE, sStringIntentExtra);
            startActivity(intent);
            overridePendingTransition(R.anim.intent_enter_anim, R.anim.intent_exit_anim);
            return true;
        }
        if (id == R.id.action1_3) {
            clearSharedPreference(sStringIntentExtra);
            Intent intent = new Intent(MainActivity.this, PreLoginActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        if (id == R.id.action1_1) {
            LoginSession.getInstance(this).setFacultyKey("");
            AppUtil.showCustomDialog(this, "Success","Faculty key successfully reset..",true, R.drawable.ic_thumb_up);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_mark_attendance) {
            // Handle the camera action
            if (false) {//TODO isSunday
                AppUtil.showCustomDialog(this, "Sorry!", "There is no class in Sunday", true, R.drawable.ic_sentiment);
            } else {
                if (false) {//TODO isNoNineToSixPM
                    AppUtil.showCustomDialog(this, "Sorry!", "Lecture time should be 9 to 6 pm", true, R.drawable.ic_sentiment);
                } else {
                    if (isHoliday) {
                        AppUtil.showCustomDialog(this, holidayDetailArrayList.get(holidayIndex).getHolidayName(), holidayDetailArrayList.get(holidayIndex).getDescription(), true, R.drawable.ic_sentiment);
                    } else {
                        Intent intentSelector1 = new Intent(MainActivity.this, SelectorActivity.class);
                        intentSelector1.putExtra("activity_indicator", "mark_attendance");
                        startActivity(intentSelector1);
                    }
                }
            }

        } else if (id == R.id.nav_student_record) {
            if (false) {//TODO isSunday
                AppUtil.showCustomDialog(this, "Sorry!", "There is no class in Sunday", true, R.drawable.ic_sentiment);
            } else {
                if (false) {//TODO isNoNineToSixPM
                    AppUtil.showCustomDialog(this, "Sorry!", "Lecture time should be 9 to 6 pm", true, R.drawable.ic_sentiment);
                } else {
                    if (isHoliday) {
                        AppUtil.showCustomDialog(this, holidayDetailArrayList.get(holidayIndex).getHolidayName(), holidayDetailArrayList.get(holidayIndex).getDescription(), true, R.drawable.ic_sentiment);
                    } else {
                        Intent intentSelector2 = new Intent(MainActivity.this, SelectorActivity.class);
                        intentSelector2.putExtra("activity_indicator", "student_record");
                        startActivity(intentSelector2);
                        overridePendingTransition(R.anim.intent_enter_anim, R.anim.intent_exit_anim);
                    }
                }
            }

        } else if (id == R.id.nav_upload_attendance) {

            if (false) {//TODO isSunday
                AppUtil.showCustomDialog(this, "Sorry!", "There is no class in Sunday", true, R.drawable.ic_sentiment);
            } else {
                if (false) {//TODO isNoNineToSixPM
                    AppUtil.showCustomDialog(this, "Sorry!", "Lecture time should be 9 to 6 pm", true, R.drawable.ic_sentiment);
                } else {
                    if (isHoliday) {
                        AppUtil.showCustomDialog(this, holidayDetailArrayList.get(holidayIndex).getHolidayName(), holidayDetailArrayList.get(holidayIndex).getDescription(), true, R.drawable.ic_sentiment);
                    } else {

                        Intent intentSelector2 = new Intent(MainActivity.this, UploadAttendanceActivity.class);
                        startActivity(intentSelector2);
                        overridePendingTransition(R.anim.intent_enter_anim, R.anim.intent_exit_anim);

                    }
                }
            }
        } else if (id == R.id.nav_notify_attendance) {

            if (false) {//TODO isSunday
                AppUtil.showCustomDialog(this, "Sorry!", "There is no class in Sunday", true, R.drawable.ic_sentiment);
            } else {
                if (false) {//TODO isNoNineToSixPM
                    AppUtil.showCustomDialog(this, "Sorry!", "Lecture time should be 9 to 6 pm", true, R.drawable.ic_sentiment);
                } else {
                    if (isHoliday) {
                        AppUtil.showCustomDialog(this, holidayDetailArrayList.get(holidayIndex).getHolidayName(), holidayDetailArrayList.get(holidayIndex).getDescription(), true, R.drawable.ic_sentiment);

                    } else {//
                        Intent intentDummyChart = new Intent(MainActivity.this, SelectorActivity.class);
                        intentDummyChart.putExtra("activity_indicator","notify_student");
                        startActivity(intentDummyChart);
                        overridePendingTransition(R.anim.intent_enter_anim, R.anim.intent_exit_anim);
                    }
                }
            }
        }else if (id == R.id.nav_generate_report){
            if (false) {//TODO isSunday
                AppUtil.showCustomDialog(this, "Sorry!", "There is no class in Sunday", true, R.drawable.ic_sentiment);
            } else {
                if (false) {//TODO isNoNineToSixPM
                    AppUtil.showCustomDialog(this, "Sorry!", "Lecture time should be 9 to 6 pm", true, R.drawable.ic_sentiment);
                } else {
                    if (isHoliday) {
                        AppUtil.showCustomDialog(this, holidayDetailArrayList.get(holidayIndex).getHolidayName(), holidayDetailArrayList.get(holidayIndex).getDescription(), true, R.drawable.ic_sentiment);

                    } else {//
                        Intent intentDummyChart = new Intent(MainActivity.this, SelectorActivity.class);
                        intentDummyChart.putExtra("activity_indicator","generate_report");
                        startActivity(intentDummyChart);
                        overridePendingTransition(R.anim.intent_enter_anim, R.anim.intent_exit_anim);
                    }
                }
            }
        }
        else if (id == R.id.nav_view_profile) {

            Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
            intent.putExtra(LoginActivity.USER_TYPE, sStringIntentExtra);
            startActivity(intent);
            overridePendingTransition(R.anim.intent_enter_anim, R.anim.intent_exit_anim);
        }else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_bug_report) {

        }

        DrawerLayout drawer = activityMainBinding.drawerLayout;
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /*clear sharedPreference*/
    private void clearSharedPreference(String userType) {
        LoginSession.getInstance(this).setLogIn(false);
        SharedPreferences sp = getSharedPreferences(userType, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        if (userType.equals("1")) {
            Log.d(TAG, "clearSharedPreference: 1");
            editor.remove(getString(R.string.faculty_dep));
            editor.remove(getString(R.string.faculty_name));
            editor.remove(getString(R.string.faculty_pass));
            editor.remove(getString(R.string.faculty_email));
            editor.remove(getString(R.string.faculty_mob));
            editor.remove(getString(R.string.faculty_spe));

        } else if (userType.equals("0")) {
            Log.d(TAG, "clearSharedPreference: 0");
            editor.remove(getString(R.string.hod_department));
            editor.remove(getString(R.string.hod_name));
            editor.remove(getString(R.string.hod_email));
            editor.remove(getString(R.string.dep_key));
            editor.remove(getString(R.string.public_key));

        } else if (userType.equals("2")) {
            Log.d(TAG, "clearSharedPreference: 2");
            editor.remove(getString(R.string.student_name));
            editor.remove(getString(R.string.student_roll_no));
            editor.remove(getString(R.string.student_mob));
            editor.remove(getString(R.string.student_email));
            editor.remove(getString(R.string.student_father_name));
            editor.remove(getString(R.string.student_father_mob));
            editor.remove(getString(R.string.student_group));
            editor.remove(getString(R.string.student_img_url));
        }
        editor.apply();
    }

    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar = activityMainBinding.appBarMain.collapseToolbar;
        collapsingToolbar.setTitle(getString(R.string.app_name));
        AppBarLayout appBarLayout = activityMainBinding.appBarMain.appbarMain;
        //appBarLayout.setExpanded(true);
        toolbar = activityMainBinding.appBarMain.toolbar;
        setSupportActionBar(toolbar);
        ActionBar mActionBarSupport = getSupportActionBar();
        mActionBarSupport.setHomeButtonEnabled(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(getString(R.string.app_name));
                    collapsingToolbar.setContentScrimColor(0x00000000);

                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(getString(R.string.app_name));
                    isShow = false;
                }
            }
        });
    }

    private void initMainActivityLoader() {
        AppUtil.showProgressDialog(this);
        operation = NetworkUtility.HOLIDAY_DETAIL;
        getLoaderManager().restartLoader(LOADER_ID, null, this).forceLoad();
    }

    private void refreshMainActivityLoader() {
        isRefreshing = true;
        isNoNineToSixPM = false;
        isHoliday = false;
        isSunday = false;
        holidayIndex = 0;
        operation = NetworkUtility.HOLIDAY_DETAIL;
        getLoaderManager().restartLoader(LOADER_ID, null, this).forceLoad();
    }

    private void finishMainActivityData(String data) {
        if (isRefreshing) {
            waveSwipeRefreshLayout.setRefreshing(false);
            isRefreshing = false;
        } else {
            AppUtil.hideProgressDialog();
        }


        if (data.isEmpty()) {
            AppUtil.showCustomDialog(this, "Something Went Wrong", "Failed to connect to server.", true, R.drawable.ic_error_outline);
        } else {
            byte[] decode = Base64.decode(data.getBytes(), Base64.DEFAULT);
            String jsonString = new String(decode);


            String[] response = JSONExtractor.getStatusMessage(jsonString);
            String status = response[0];
            String message = response[1];
            if (status.equals("0")) {
                AppUtil.showCustomDialog(this, "Failed", message, true, R.drawable.ic_error_outline);
            } else {
                currentDateDayTime = JSONExtractor.extractCurrentDateResponse(jsonString);
                LoginSession.getInstance(this).setCurrentDate(currentDateDayTime.substring(0,10));
                LoginSession.getInstance(this).setTime(currentDateDayTime.substring(16,22));
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        incrementTimeEveryMinute();
                    }
                }, 60000);
                Log.d(TAG, "finishMainActivityData: current date" + currentDateDayTime);
                holidayDetailArrayList = JSONExtractor.extractHolidayResponse(jsonString);
                if (currentDateDayTime.isEmpty()) {
                    initMainActivityLoader();
                } else if (currentDateDayTime.substring(12, 15).equals("Sun")) {
                    isSunday = true;
                } else if ((Integer.parseInt(currentDateDayTime.substring(17, 19)) <= 9) || (Integer.parseInt(currentDateDayTime.substring(17, 19)) >= 18)) {
                    isNoNineToSixPM = true;
                    Log.d(TAG, "finishMainActivityData: isNineToSixPM1 " + (Integer.parseInt(currentDateDayTime.substring(17, 19))));
                } else {
                    /*Toast.makeText(this, "size of holidayDetail"+holidayDetailArrayList.size(), Toast.LENGTH_LONG).show();*/
                    isSunday = false;
                    isNoNineToSixPM = false;
                    Log.d(TAG, "finishMainActivityData: isNineToSixPM2 " + (Integer.parseInt(currentDateDayTime.substring(17, 19))));
                     /*check for holiday*/
                    for (int holidayDate = 0; holidayDate < holidayDetailArrayList.size(); holidayDate++) {
                        if (holidayDetailArrayList.get(holidayDate).getHolidayDate().equals(currentDateDayTime.substring(0, 10))) {
                            isHoliday = true;
                            holidayIndex = holidayDate;
                        }
                    }
                }
            }

        }
    }

    private void incrementTimeEveryMinute() {
        final Runnable r = new Runnable() {
            public void run() {
                handler.postDelayed(this, 60000);
                LoginSession l = LoginSession.getInstance(MainActivity.this);
                String time = l.getTime();
                SimpleDateFormat df = new SimpleDateFormat("HH:mm");
                Date d = null;
                try {
                    d = df.parse(time);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar cal = Calendar.getInstance();
                cal.setTime(d);
                cal.add(Calendar.MINUTE, 1);
                String newTime = df.format(cal.getTime());
                l.setTime(newTime);
            }
        };
        r.run();
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        return new MainActivityAsyncLoader(this, postData, operation);
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {

        finishMainActivityData(data);
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {

    }

    @Override
    public void onRefresh() {
        if (AppUtil.isInternetIsAvailable(this)) {
            refreshMainActivityLoader();
        } else {
            AppUtil.showNoInternetDialog(this);
            waveSwipeRefreshLayout.setRefreshing(false);
        }
    }

    static class MainActivityAsyncLoader extends AsyncTaskLoader<String> {
        String operation;
        MultipartBody.Builder postData;

        public MainActivityAsyncLoader(Context context, MultipartBody.Builder postData, String operation) {
            super(context);
            this.postData = postData;
            this.operation = operation;
        }

        @Override
        public String loadInBackground() {
            String response = NetworkUtility.getServerResponse(getContext(), operation, postData);
            Log.d(TAG, "loadInBackground: response" + response);
            return response;
        }
    }


}
