package work.ravi.com.ams.adapter.spinner;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import work.ravi.com.ams.R;
import work.ravi.com.ams.pojo.DepartmentDetail;

/**
 * Created by Ravi on 04-03-2018.
 */

public class CustomDepartmentSpinnerAdapter extends ArrayAdapter<String> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final List<DepartmentDetail> departmentList;
    private final int mResourceLayout;

    public CustomDepartmentSpinnerAdapter(@NonNull Context context, @LayoutRes int resource,
                                          @NonNull List objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResourceLayout = resource;
        departmentList = objects;
    }
    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResourceLayout, parent, false);

       /* TextView offTypeTv = (TextView) view.findViewById(R.id.offer_type_txt);
        TextView numOffersTv = (TextView) view.findViewById(R.id.num_offers_txt);
        TextView maxDiscTV = (TextView) view.findViewById(R.id.max_discount_txt);

        Offer offerData = items.get(position);

        offTypeTv.setText(offerData.getOfferType());
        numOffersTv.setText(offerData.getNumberOfCoupons());
        maxDiscTV.setText(offerData.getMaxDicount());*/
        TextView textViewDepartment = view.findViewById(R.id.text_view_department_name);
        TextView textViewDepartmentShortcut = view.findViewById(R.id.text_view_department_shortcut);
        ImageView departmentImage = view.findViewById(R.id.image_view_department_spinner);

        DepartmentDetail departmentDetail = departmentList.get(position);
        textViewDepartment.setText(departmentDetail.getDepartmentName());
        textViewDepartmentShortcut.setText(departmentDetail.getDepartmentSrct());

        switch (Integer.parseInt(departmentDetail.getDepartmentId())){

            case 10:
                departmentImage.setImageResource(R.drawable.ic_computer_science_department);
                break;
            case 40:
                departmentImage.setImageResource(R.drawable.ic_mechanical_department);
                break;
            case 00:
                departmentImage.setImageResource(R.drawable.ic_civil_department);
                break;
            case 13:
                departmentImage.setImageResource(R.drawable.ic_information_technology_department);
                break;
            case 31:
                departmentImage.setImageResource(R.drawable.ic_electronics_department);
                break;
            default:
                departmentImage.setImageResource(R.drawable.ic_default_department);
                break;
        }

        return view;
    }
}
