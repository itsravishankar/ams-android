package work.ravi.com.ams.pojo;

/**
 * Created by Ravi on 3/16/2018.
 */

public class MarkedAttendanceHolder {
    private String tableName;
    private String section;
    private String date;
    private String time;
    private String attendanceValues;
    private String subjectName;

    public MarkedAttendanceHolder() {
    }

    public MarkedAttendanceHolder(String tableName, String section, String date, String time, String attendanceValues, String subjectName) {
        this.tableName = tableName;
        this.section = section;
        this.date = date;
        this.time = time;
        this.attendanceValues = attendanceValues;
        this.subjectName = subjectName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAttendanceValues() {
        return attendanceValues;
    }

    public void setAttendanceValues(String attendanceValues) {
        this.attendanceValues = attendanceValues;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
