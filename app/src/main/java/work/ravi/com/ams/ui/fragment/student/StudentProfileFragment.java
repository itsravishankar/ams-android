package work.ravi.com.ams.ui.fragment.student;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import work.ravi.com.ams.R;
import work.ravi.com.ams.databinding.FragmentStudentProfileBinding;
import work.ravi.com.ams.util.AppUtil;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Ravi on 3/17/2018.
 */

public class StudentProfileFragment extends Fragment {

    private TextView mTextViewUserName, mTextViewUserFatherName, mTextViewUserPhoneNo, mTextViewUserEmail
            ,mTextViewUserFatherMobileNo, mTextViewUserCourse;
    private ImageView mImageViewProfile;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        FragmentStudentProfileBinding fragmentStudentProfileBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_student_profile, container, false);
        mTextViewUserName = fragmentStudentProfileBinding.studentProfileCard.textViewStudentName;
        mTextViewUserCourse = fragmentStudentProfileBinding.studentProfileCard.textViewUserCourse;
        mTextViewUserEmail = fragmentStudentProfileBinding.studentProfileCard.textViewUserEmail;
        mTextViewUserFatherMobileNo = fragmentStudentProfileBinding.studentProfileCard.textViewFatherPhoneNo;
        mTextViewUserFatherName = fragmentStudentProfileBinding.studentProfileCard.textViewFatherName;
        mTextViewUserPhoneNo = fragmentStudentProfileBinding.studentProfileCard.textViewUserPhoneNo;
        mImageViewProfile = fragmentStudentProfileBinding.studentProfileCard.imageViewIdProfilePic;
        AppUtil.initiaUnivarsalImageLoader(getActivity());
        setStudentProfileData();
        return fragmentStudentProfileBinding.getRoot();
    }

    public String getValueByPrefrenceForStudent(String key) {
        String value = null;
        SharedPreferences userDetails = getActivity().getSharedPreferences(getString(R.string.student_login_sp), MODE_PRIVATE);
        value = userDetails.getString(key, "Dummy Value").toString();
        return value;
    }

    private void setStudentProfileData() {
        mTextViewUserName.setText(getValueByPrefrenceForStudent(getString(R.string.student_name)));
        mTextViewUserEmail.setText(getValueByPrefrenceForStudent(getString(R.string.student_email)));
        mTextViewUserCourse.setText(getValueByPrefrenceForStudent(getString(R.string.student_roll_no)));
        mTextViewUserPhoneNo.setText(getValueByPrefrenceForStudent(getString(R.string.student_mob)));
        /*studentGroup.setText(getValueByPrefrenceForStudent(getString(R.string.student_group)));*/
        mTextViewUserFatherName.setText(getValueByPrefrenceForStudent(getString(R.string.student_father_name)));
        mTextViewUserFatherMobileNo.setText(getValueByPrefrenceForStudent(getString(R.string.student_father_mob)));
        AppUtil.setUnivarsalImageLoader(mImageViewProfile, getValueByPrefrenceForStudent(getString(R.string.student_img_url)), 0);
    }


}
