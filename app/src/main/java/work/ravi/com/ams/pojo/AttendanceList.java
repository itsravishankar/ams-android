package work.ravi.com.ams.pojo;

/**
 * Created by Ravi on 3/10/2018.
 */

public class AttendanceList {
    private String rollNumber;
    private String attendanceStatus;

    public AttendanceList(String rollNumber, String attendanceStatus) {
        this.rollNumber = rollNumber;
        this.attendanceStatus = attendanceStatus;
    }

    public String getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getAttendanceStatus() {
        return attendanceStatus;
    }

    public void setAttendanceStatus(String attendanceStatus) {
        this.attendanceStatus = attendanceStatus;
    }
}
