package work.ravi.com.ams.adapter;

import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import work.ravi.com.ams.R;
import work.ravi.com.ams.databinding.GenerateReportItemBinding;
import work.ravi.com.ams.databinding.ReportCardBinding;
import work.ravi.com.ams.pojo.GenerateReportList;
import work.ravi.com.ams.pojo.StudentDetail;
import work.ravi.com.ams.ui.activity.ReportCardActivity;
import work.ravi.com.ams.ui.activity.ViewAttendanceActivity;
import work.ravi.com.ams.util.AppUtil;
import work.ravi.com.ams.util.JSONExtractor;
import work.ravi.com.ams.util.NetworkUtility;

import static android.content.ContentValues.TAG;


public class GenerateReportAdapter extends RecyclerView.Adapter<GenerateReportAdapter.MyViewHolder>  {

    private Context mContext;
    private ArrayList<GenerateReportList> generateReportLists;
    private int lastPosition = -1;
    private String department;
    private String section;
    private String year;
    private ArrayList<StudentDetail> studentDetailArrayList;


    public GenerateReportAdapter(Context mContext, ArrayList<GenerateReportList> generateReportLists,ArrayList<StudentDetail> studentDetailArrayList , String department, String year, String section) {
        this.mContext = mContext;
        this.generateReportLists = generateReportLists;
        this.studentDetailArrayList = studentDetailArrayList;
        this.department = department;
        this.year = year;
        this.section = section;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        GenerateReportItemBinding generateReportItemBinding = DataBindingUtil.inflate(layoutInflater, R.layout.generate_report_item, parent, false);

        return new MyViewHolder(generateReportItemBinding);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final GenerateReportList generateReportList = generateReportLists.get(position);
        int p = 0, a = 0, t = 0;
        for (int i = 0; i < generateReportList.getTotalAttendanceDetail().size(); i++) {
            p = p + generateReportList.getTotalAttendanceDetail().get(i).getPresent();//TODO use p,a,t for total present, absent , total compilation
            a = a + generateReportList.getTotalAttendanceDetail().get(i).getAbsent();
            t = t + generateReportList.getTotalAttendanceDetail().get(i).getTotal();
        }


        holder.studentRoll.setText(generateReportList.getRollNo());
        holder.studentName.setText(studentDetailArrayList.get(position).getStudentName());
        holder.generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ReportCardActivity.class);
                intent.putExtra("repo_name", studentDetailArrayList.get(position).getStudentName());
                intent.putExtra("repo_rollno", studentDetailArrayList.get(position).getStudentRollno());
                intent.putExtra("repo_branch", department);
                intent.putExtra("repo_year", year);
                intent.putExtra("repo_section", section);
                mContext.startActivity(intent);
            }
        });
        //holder.studentRoll.setText("P :"+String.valueOf(p)+" A :"+String.valueOf(a)+" T :"+String.valueOf(t));//Todo use for print
        Log.d(TAG, "onBindViewHolder: Gl->GetTA :" + generateReportList.getTotalAttendanceDetail().size());

        setAnimation(holder.itemView, position);

    }

    @Override
    public int getItemCount() {

        return generateReportLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView studentRoll, studentName;
        private ImageView generate;


        public MyViewHolder(GenerateReportItemBinding generateReportItemBinding) {
            super(generateReportItemBinding.getRoot());

            studentName = generateReportItemBinding.generateStudentName;
            studentRoll = generateReportItemBinding.generateStudentRollno;
            generate = generateReportItemBinding.generateIm;
        }
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.zoom_in_anim);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}