package work.ravi.com.ams.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import work.ravi.com.ams.R;
import work.ravi.com.ams.databinding.ItemMarkAttendanceStudentListBinding;
import work.ravi.com.ams.pojo.AllStudentList;
import work.ravi.com.ams.pojo.AttendanceList;
import work.ravi.com.ams.pojo.EnrolledStudentList;
import work.ravi.com.ams.pojo.StudentDetail;
import work.ravi.com.ams.util.AppUtil;

import static android.content.ContentValues.TAG;

/**
 * Created by Ravi on 06-03-2018.
 */

public class MarkAttendanceStudentListAdapter extends RecyclerView.Adapter<MarkAttendanceStudentListAdapter.MarkAttendanceStudentListViewHolder>{

    private Context context;
    private static ArrayList<EnrolledStudentList> studentDetails = new ArrayList<>();
    private static HashMap<String, StudentDetail> studentDetailHashMap = new HashMap<>();
    private ArrayList<String> attendanceMarkList = new ArrayList<>();


    public MarkAttendanceStudentListAdapter(Context context, ArrayList<EnrolledStudentList> studentDetails){
        this.context = context;
        this.studentDetails = studentDetails;
        studentDetailHashMap = AllStudentList.getInstance().getStudentList();
        for (int i = 0; i<studentDetails.size(); i++) {
            attendanceMarkList.add(studentDetails.get(i).getRollNo());
            Log.d(TAG, "MarkAttendanceStudentListAdapter: " + studentDetails.get(i).getRollNo());
        }
        Collections.sort(attendanceMarkList);
    }

    @Override
    public MarkAttendanceStudentListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemMarkAttendanceStudentListBinding itemMarkAttendanceStudentListBinding = DataBindingUtil
                .inflate(LayoutInflater.from(context), R.layout.item_mark_attendance_student_list, parent, false);

        AppUtil.initiaUnivarsalImageLoader(context);
        return new MarkAttendanceStudentListViewHolder(itemMarkAttendanceStudentListBinding);
    }

    @Override
    public void onBindViewHolder(final MarkAttendanceStudentListViewHolder holder, int position) {
        String rollNo = attendanceMarkList.get(position);
        StudentDetail studentDetail = studentDetailHashMap.get(rollNo);
        holder.mTextViewName.setText(studentDetail.getStudentName());
        holder.mTextViewRollNo.setText(studentDetail.getStudentRollno());
        holder.mTextViewGroup.setText(studentDetail.getStudentGroup());
        AppUtil.setUnivarsalImageLoader(holder.mImageViewStudentPic, studentDetail.getStudentImgUrl(), 200);
        if (studentDetails.get(holder.getAdapterPosition()).getAttendanceStatus() == false) {
            holder.mRelativeLayoutImageView.setBackground(context.getResources().getDrawable(R.drawable.bg_red_circle));
        }else holder.mRelativeLayoutImageView.setBackground(context.getResources().getDrawable(R.drawable.bg_green_circle));
        holder.mLinearLayoutStudentCard.setOnClickListener(view -> {
            if (studentDetails.get(holder.getAdapterPosition()).getAttendanceStatus() == false) {
                studentDetails.get(holder.getAdapterPosition()).setAttendanceStatus(true);
                holder.mRelativeLayoutImageView.setBackground(context.getResources().getDrawable(R.drawable.bg_green_circle));
                studentDetailHashMap.get(attendanceMarkList.get(holder.getAdapterPosition())).setAttendanceStatus("1");
            }
            else if (studentDetails.get(holder.getAdapterPosition()).getAttendanceStatus() == true) {
                studentDetails.get(holder.getAdapterPosition()).setAttendanceStatus(false);
                holder.mRelativeLayoutImageView.setBackground(context.getResources().getDrawable(R.drawable.bg_red_circle));
                studentDetailHashMap.get(attendanceMarkList.get(holder.getAdapterPosition())).setAttendanceStatus("0");
            }
        });
    }

    public String convertAttendanceListToJSON(List<AttendanceList> attendanceList) {
        Gson gson = new Gson();
        String json = gson.toJson(attendanceList);
        return json;
    }

    public static String getMarkedAttendanceList(){
        ArrayList<AttendanceList> attendanceArrayList = new ArrayList<>();
        String attendanceRecord = "";
        for (int i = 0; i<studentDetails.size(); i++){
            String rollNo = studentDetails.get(i).getRollNo();
            /*AttendanceList attendanceList = new AttendanceList(rollNo, studentDetailHashMap.get(rollNo).getAttendanceStatus());
            attendanceArrayList.add(attendanceList);*/
            Log.d(TAG, "getMarkedAttendanceList: "+studentDetailHashMap.get(rollNo).getAttendanceStatus());
            attendanceRecord = attendanceRecord + studentDetailHashMap.get(rollNo).getAttendanceStatus();
            Log.d(TAG, "getMarkedAttendanceList: " + studentDetails.get(i).getRollNo());
        }

        Log.d(TAG, "getMarkedAttendanceList: "+ attendanceRecord);

        return attendanceRecord;
    }

    @Override
    public int getItemCount() {
        return studentDetails.size();
    }

    public class MarkAttendanceStudentListViewHolder extends RecyclerView.ViewHolder {
         TextView mTextViewRollNo, mTextViewName, mTextViewGroup;
         ImageView mImageViewStudentPic;
         CardView mCardViewStudent;
         LinearLayout mLinearLayoutStudentCard;
        RelativeLayout mRelativeLayoutImageView;
        public MarkAttendanceStudentListViewHolder(ItemMarkAttendanceStudentListBinding itemMarkAttendanceStudentListBinding) {
            super(itemMarkAttendanceStudentListBinding.getRoot());
            mTextViewRollNo = itemMarkAttendanceStudentListBinding.textViewRollNo;
            mTextViewGroup = itemMarkAttendanceStudentListBinding.textViewGroup;
            mTextViewName = itemMarkAttendanceStudentListBinding.textViewStudentName;
            mImageViewStudentPic = itemMarkAttendanceStudentListBinding.imageViewStudentPic;
            mCardViewStudent = itemMarkAttendanceStudentListBinding.cardViewStudent;
            mLinearLayoutStudentCard =itemMarkAttendanceStudentListBinding.llStudentCard;
            mRelativeLayoutImageView = itemMarkAttendanceStudentListBinding.rlImageView;
        }
    }
}
