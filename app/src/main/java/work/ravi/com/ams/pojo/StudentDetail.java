package work.ravi.com.ams.pojo;

/**
 * Created by acer on 03-03-2018.
 */

public class StudentDetail {

    private String studentRollno;
    private String studentName;
    private String studentMob;
    private String studentGroup;
    private String studentFatherName;
    private String studentEmail;
    private String studentFatherMob;
    private String studentImgUrl;
    private String attendanceStatus="0";

    public StudentDetail(String studentRollno, String studentName, String studentMob, String studentGroup, String studentFatherName, String studentEmail, String studentFatherMob, String studentImgUrl) {
        this.studentRollno = studentRollno;
        this.studentName = studentName;
        this.studentMob = studentMob;
        this.studentGroup = studentGroup;
        this.studentFatherName = studentFatherName;
        this.studentEmail = studentEmail;
        this.studentFatherMob = studentFatherMob;
        this.studentImgUrl = studentImgUrl;
    }

    public String getStudentRollno() {
        return studentRollno;
    }

    public void setStudentRollno(String studentRollno) {
        this.studentRollno = studentRollno;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentMob() {
        return studentMob;
    }

    public void setStudentMob(String studentMob) {
        this.studentMob = studentMob;
    }

    public String getStudentGroup() {
        return studentGroup;
    }

    public void setStudentGroup(String studentGroup) {
        this.studentGroup = studentGroup;
    }

    public String getStudentFatherName() {
        return studentFatherName;
    }

    public void setStudentFatherName(String studentFatherName) {
        this.studentFatherName = studentFatherName;
    }

    public String getStudentEmail() {
        return studentEmail;
    }

    public void setStudentEmail(String studentEmail) {
        this.studentEmail = studentEmail;
    }

    public String getStudentFatherMob() {
        return studentFatherMob;
    }

    public void setStudentFatherMob(String studentFatherMob) {
        this.studentFatherMob = studentFatherMob;
    }

    public String getStudentImgUrl() {
        return studentImgUrl;
    }

    public void setStudentImgUrl(String studentImgUrl) {
        this.studentImgUrl = studentImgUrl;
    }

    public String getAttendanceStatus() {
        return attendanceStatus;
    }

    public void setAttendanceStatus(String attendanceStatus) {
        this.attendanceStatus = attendanceStatus;
    }
}
