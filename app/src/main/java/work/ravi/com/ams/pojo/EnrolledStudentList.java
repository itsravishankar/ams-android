package work.ravi.com.ams.pojo;

/**
 * Created by Ravi on 06-03-2018.
 */

public class EnrolledStudentList {

    private String id;
    private String rollNo;
    private boolean attendanceStatus;

    public EnrolledStudentList(String id, String rollNo) {
        this.id = id;
        this.rollNo = rollNo;
    }

    public String getRollNo() {
        return rollNo;
    }

    public void setRollNo(String rollNo) {
        this.rollNo = rollNo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean getAttendanceStatus() {
        return attendanceStatus;
    }

    public void setAttendanceStatus(boolean attendanceStatus) {
        this.attendanceStatus = attendanceStatus;
    }
}
