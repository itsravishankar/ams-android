package work.ravi.com.ams.adapter;

import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Loader;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.MultipartBody;
import work.ravi.com.ams.R;
import work.ravi.com.ams.databinding.ItemUploadAttendanceListBinding;
import work.ravi.com.ams.pojo.MarkedAttendanceHolder;
import work.ravi.com.ams.sharedprefrence.MarkedAttendanceRecords;
import work.ravi.com.ams.util.AppUtil;
import work.ravi.com.ams.util.JSONExtractor;
import work.ravi.com.ams.util.NetworkUtility;

import static com.nostra13.universalimageloader.core.ImageLoader.TAG;

/**
 * Created by Ravi on 3/16/2018.
 */

public class UploadAttendanceAdapter
        extends RecyclerView.Adapter<UploadAttendanceAdapter.UploadAttendanceViewHolder>
        implements LoaderManager.LoaderCallbacks<String>{

    private HashMap<String, MarkedAttendanceHolder> markedAttendanceHolderHashMap;
    private ArrayList<String> hashmapKeyName;
    private Context context;
    private String operation;
    private MultipartBody.Builder postData;
    private int LOADER_ID = 0;
    private LoaderManager loaderManager;
    private int currentPosition;
    private NotifyAdapterInterface notifyAdapterInterface;

    public UploadAttendanceAdapter(Context context, LoaderManager loaderManager,
                                   HashMap<String, MarkedAttendanceHolder> markedAttendanceHolderHashMap,
                                   ArrayList<String> hashmapKeyName, NotifyAdapterInterface notifyAdapterInterface){
        this.context = context;
        this.loaderManager = loaderManager;
        this.markedAttendanceHolderHashMap = markedAttendanceHolderHashMap;
        this.hashmapKeyName = hashmapKeyName;
        this.notifyAdapterInterface = notifyAdapterInterface;
        postData = new MultipartBody.Builder();
    }

    @Override
    public UploadAttendanceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ItemUploadAttendanceListBinding itemUploadAttendanceListBinding = DataBindingUtil
                .inflate(layoutInflater, R.layout.item_upload_attendance_list, parent, false);
        return new UploadAttendanceViewHolder(itemUploadAttendanceListBinding);
    }

    @Override
    public void onBindViewHolder(UploadAttendanceViewHolder holder, int position) {
        MarkedAttendanceHolder markedAttendanceHolder = markedAttendanceHolderHashMap.get(hashmapKeyName.get(holder.getAdapterPosition()));
        if (markedAttendanceHolder != null) {
            holder.mTextViewDate.setText(markedAttendanceHolder.getDate());
            holder.mTextViewTime.setText(markedAttendanceHolder.getTime());
            holder.mTextViewSubjectName.setText(markedAttendanceHolder.getSubjectName());
            holder.mRelativeLayoutUploadAttendance.setOnClickListener(view -> {
                if (AppUtil.isInternetIsAvailable(context)) {
                    currentPosition = holder.getAdapterPosition();
                    intiMarkAttendanceLoader(markedAttendanceHolder);
                } else {
                    AppUtil.showNoInternetDialog(context);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return markedAttendanceHolderHashMap.size();
    }

    private void intiMarkAttendanceLoader(MarkedAttendanceHolder markedAttendanceHolder) {
        AppUtil.showProgressDialog(context);
        operation = NetworkUtility.MARK_ATTENDANCE;
        postData
                .addFormDataPart("table_name", markedAttendanceHolder.getTableName())
                .addFormDataPart("section", markedAttendanceHolder.getSection())
                .addFormDataPart("date", markedAttendanceHolder.getDate())
                .addFormDataPart("values", markedAttendanceHolder.getAttendanceValues());
       loaderManager.restartLoader(LOADER_ID, null, this).forceLoad();
    }

    private void finishMarkAttendanceLoader(String data) {
        AppUtil.hideProgressDialog();
        if (data.isEmpty()) {
            AppUtil.showCustomDialog(context, "Something Went Wrong", "Failed to connect to server.", true, R.drawable.ic_error_outline);
        } else {
            byte[] decode = Base64.decode(data.getBytes(), Base64.DEFAULT);
            String jsonString = new String(decode);
            Log.d(TAG, "finishMarkAttendanceLoader: jsonStringMsg :"+jsonString);
            String[] response = JSONExtractor.getStatusMessage(jsonString);
            String status = response[0];
            String message = response[1];
            if (status.equals("0")) {
                AppUtil.showCustomDialog(context, "Failed", message, true, R.drawable.ic_error_outline);
            } else {
                AppUtil.showCustomDialog(context, "Success", message, true, R.drawable.ic_thumb_up);
                //TODO notify data set change on adapter
                notifyAdapterInterface.notifyAdapter();
                removeUploadedData();
            }
        }
    }

    private void removeUploadedData() {
        MarkedAttendanceRecords markedAttendanceRecords = MarkedAttendanceRecords.getInstance(context);
        HashMap<String, MarkedAttendanceHolder> attendanceHashMap = markedAttendanceRecords.getAttendanceRecord();
        ArrayList<String> hashMapKey = markedAttendanceRecords.getHashMapKeyName();
        attendanceHashMap.remove(hashMapKey.get(currentPosition));
        hashMapKey.remove(currentPosition);
        markedAttendanceRecords.setAttendanceRecord(attendanceHashMap);
        markedAttendanceRecords.setHashMapKeyName(hashMapKey);
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        return new MarkAttendanceAsyncLoader(context, postData, operation);
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        finishMarkAttendanceLoader(data);
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {

    }

    static class MarkAttendanceAsyncLoader extends AsyncTaskLoader<String> {
        String operation;
        MultipartBody.Builder postData;
        private String TAG = "MarkAttendanceAsyncLoader";

        public MarkAttendanceAsyncLoader(Context context, MultipartBody.Builder postData, String operation) {
            super(context);
            this.postData = postData;
            this.operation = operation;
        }

        @Override
        public String loadInBackground() {
            String response = NetworkUtility.getServerResponse(getContext(), operation, postData);
            Log.d(TAG, "loadInBackground: response :" + response);
            return response.trim();
        }
    }

    public class UploadAttendanceViewHolder extends RecyclerView.ViewHolder {

        TextView mTextViewDate, mTextViewTime, mTextViewSubjectName;
        RelativeLayout mRelativeLayoutUploadAttendance;
        public UploadAttendanceViewHolder(ItemUploadAttendanceListBinding itemUploadAttendanceListBinding) {
            super(itemUploadAttendanceListBinding.getRoot());
            mTextViewDate = itemUploadAttendanceListBinding.textViewDate;
            mTextViewTime = itemUploadAttendanceListBinding.textViewTime;
            mTextViewSubjectName = itemUploadAttendanceListBinding.textViewSubjectNameUploadAttendance;
            mRelativeLayoutUploadAttendance = itemUploadAttendanceListBinding.rlItemUploadAttendance;
        }
    }

    public interface NotifyAdapterInterface{
        public void notifyAdapter();
    }
}
