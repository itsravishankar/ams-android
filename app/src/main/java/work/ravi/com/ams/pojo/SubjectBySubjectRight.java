package work.ravi.com.ams.pojo;

/**
 * Created by Ravi on 06-03-2018.
 */

public class SubjectBySubjectRight {

    private String subjectName;
    private String subjectId;
    private String subjectShortcut;

    public SubjectBySubjectRight(String subjectName, String subjectId, String subjectShortcut) {
        this.subjectName = subjectName;
        this.subjectId = subjectId;
        this.subjectShortcut = subjectShortcut;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }


    public String getSubjectShortcut() {
        return subjectShortcut;
    }

    public void setSubjectShortcut(String subjectShortcut) {
        this.subjectShortcut = subjectShortcut;
    }
}
