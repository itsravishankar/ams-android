package work.ravi.com.ams.pojo;

/**
 * Created by acer on 04-03-2018.
 */

public class DepartmentDetail {

    private String departmentId;
    private String departmentName;
    private String departmentSrct;
    private String departmentKey;
    private String firstYear;
    private String secondYear;
    private String thirdYear;
    private String fourthYear;

    public DepartmentDetail(String departmentId, String departmentName, String departmentSrct, String departmentKey, String firstYear, String secondYear, String thirdYear, String fourthYear) {
        this.departmentId = departmentId;
        this.departmentName = departmentName;
        this.departmentSrct = departmentSrct;
        this.departmentKey = departmentKey;
        this.firstYear = firstYear;
        this.secondYear = secondYear;
        this.thirdYear = thirdYear;
        this.fourthYear = fourthYear;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDepartmentSrct() {
        return departmentSrct;
    }

    public void setDepartmentSrct(String departmentSrct) {
        this.departmentSrct = departmentSrct;
    }

    public String getDepartmentKey() {
        return departmentKey;
    }

    public void setDepartmentKey(String departmentKey) {
        this.departmentKey = departmentKey;
    }

    public String getFirstYear() {
        return firstYear;
    }

    public void setFirstYear(String firstYear) {
        this.firstYear = firstYear;
    }

    public String getSecondYear() {
        return secondYear;
    }

    public void setSecondYear(String secondYear) {
        this.secondYear = secondYear;
    }

    public String getThirdYear() {
        return thirdYear;
    }

    public void setThirdYear(String thirdYear) {
        this.thirdYear = thirdYear;
    }

    public String getFourthYear() {
        return fourthYear;
    }

    public void setFourthYear(String fourthYear) {
        this.fourthYear = fourthYear;
    }
}
