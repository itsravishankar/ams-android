package work.ravi.com.ams.ui.activity;

import android.app.LoaderManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.HashMap;

import work.ravi.com.ams.R;
import work.ravi.com.ams.adapter.UploadAttendanceAdapter;
import work.ravi.com.ams.databinding.ActivityUploadAttendanceBinding;
import work.ravi.com.ams.pojo.MarkedAttendanceHolder;
import work.ravi.com.ams.sharedprefrence.MarkedAttendanceRecords;

/**
 * Created by Ravi on 3/16/2018.
 */

public class UploadAttendanceActivity extends AppCompatActivity implements UploadAttendanceAdapter.NotifyAdapterInterface {
    private RecyclerView recyclerViewMarkedAttendanceList;
    private HashMap<String, MarkedAttendanceHolder> markedAttendanceHolderHashMap;
    private ArrayList<String> hashmapKeyName;
    private RelativeLayout mRelativeLayout;
    private UploadAttendanceAdapter uploadAttendanceAdapter;
    boolean flag = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityUploadAttendanceBinding activityUploadAttendanceBinding = DataBindingUtil
                .setContentView(this, R.layout.activity_upload_attendance);
        recyclerViewMarkedAttendanceList = activityUploadAttendanceBinding.recyclerViewUploadAttendance;
        mRelativeLayout = activityUploadAttendanceBinding.rlUploadEmptyMessage;
        initMarkedAttendanceList();
    }

    private void initMarkedAttendanceList() {
        MarkedAttendanceRecords markedAttendanceRecords = MarkedAttendanceRecords.getInstance(this);
        markedAttendanceHolderHashMap = markedAttendanceRecords.getAttendanceRecord();
        hashmapKeyName = markedAttendanceRecords.getHashMapKeyName();
        setAdapter();
    }

    public void setAdapter() {
        if (hashmapKeyName.size() == 0) {
            mRelativeLayout.setVisibility(View.VISIBLE);
            recyclerViewMarkedAttendanceList.setVisibility(View.GONE);
        } /*else if (flag) {
            uploadAttendanceAdapter.notifyDataSetChanged();
        }*/ else {
            mRelativeLayout.setVisibility(View.GONE);
            recyclerViewMarkedAttendanceList.setVisibility(View.VISIBLE);
            LoaderManager loaderManager = getLoaderManager();
            RecyclerView.LayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
            uploadAttendanceAdapter = new UploadAttendanceAdapter(this,
                    loaderManager, markedAttendanceHolderHashMap, hashmapKeyName,  this);
            recyclerViewMarkedAttendanceList.setLayoutManager(mLinearLayoutManager);
            recyclerViewMarkedAttendanceList.setItemAnimator(new DefaultItemAnimator());
            recyclerViewMarkedAttendanceList.setAdapter(uploadAttendanceAdapter);
        }
    }

    @Override
    public void notifyAdapter() {
       /* flag = true;
        setAdapter();*/
       finish();
    }
}
