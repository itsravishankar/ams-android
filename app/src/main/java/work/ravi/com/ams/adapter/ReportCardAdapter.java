package work.ravi.com.ams.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.util.ArrayList;

import work.ravi.com.ams.R;
import work.ravi.com.ams.databinding.ReportCardItemBinding;
import work.ravi.com.ams.pojo.TotalAttendanceDetail;


public class ReportCardAdapter extends RecyclerView.Adapter<ReportCardAdapter.MyViewHolder> {

    private Context mContext;

    private int lastPosition = -1;
    private ArrayList<TotalAttendanceDetail> totalAttendanceDetail;

    String[] attendanceDataLable;

    public ReportCardAdapter(Context mContext, ArrayList<TotalAttendanceDetail> totalAttendanceDetail, String[] attendanceDataLable) {
        this.mContext = mContext;
        this.totalAttendanceDetail = totalAttendanceDetail;
        this.attendanceDataLable = attendanceDataLable;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ReportCardItemBinding reportCardItemBinding = DataBindingUtil.inflate(layoutInflater, R.layout.report_card_item, parent, false);
        return new MyViewHolder(reportCardItemBinding);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        int percent = 0;
        if (totalAttendanceDetail.get(position).getTotal() == 0) {
            percent = 0;
        } else {
            percent = totalAttendanceDetail.get(position).getPresent() * 100 / totalAttendanceDetail.get(position).getTotal();
        }

        holder.subject.setText(attendanceDataLable[position]);
        holder.get.setText(String.valueOf(totalAttendanceDetail.get(position).getPresent()));
        holder.inPercent.setText(String.valueOf(percent)+"%");
        holder.total.setText(String.valueOf(totalAttendanceDetail.get(position).getTotal()));

        setAnimation(holder.itemView, position);

    }

    @Override
    public int getItemCount() {

        return totalAttendanceDetail.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView subject, get, inPercent, total;

        public MyViewHolder(ReportCardItemBinding reportCardItemBinding) {
            super(reportCardItemBinding.getRoot());

            subject = reportCardItemBinding.repoSubject;
            get = reportCardItemBinding.repoGet;
            inPercent = reportCardItemBinding.repoIn;
            total = reportCardItemBinding.repoTotal;
        }
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.zoom_in_anim);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}