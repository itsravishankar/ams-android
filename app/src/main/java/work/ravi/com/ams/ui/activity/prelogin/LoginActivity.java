package work.ravi.com.ams.ui.activity.prelogin;

import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.InputType;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.florent37.materialtextfield.MaterialTextField;

import okhttp3.MultipartBody;
import work.ravi.com.ams.R;
import work.ravi.com.ams.databinding.ActivityLoginBinding;
import work.ravi.com.ams.interfaces.NotifyLogout;
import work.ravi.com.ams.pojo.FacultyLoginDetail;
import work.ravi.com.ams.pojo.HodLoginDetail;
import work.ravi.com.ams.pojo.StudentDetail;
import work.ravi.com.ams.sharedprefrence.LoginSession;
import work.ravi.com.ams.ui.activity.MainActivity;
import work.ravi.com.ams.ui.activity.StudentActivity;
import work.ravi.com.ams.ui.fragment.selector.DepartmentSelectorFragment;
import work.ravi.com.ams.util.AppUtil;
import work.ravi.com.ams.util.JSONExtractor;
import work.ravi.com.ams.util.NetworkUtility;

/**
 * Created by Ravi on 30-12-2017.
 */

public class LoginActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<String> {

    private static final String TAG = "LoginActivity";
    public static final String USER_TYPE = "USER_TYPE";
    EditText mEditTextEmail, mEditTextPassword;
    Button mButtonLogin;
    TextView mTextViewForgerPassword;
    ImageView mImageViewUserImage;
    private static String sStringIntentExtra;
    private MultipartBody.Builder postData;
    private String operation = "";
    private int LOADER_ID = 0;
    private static String sStringDepartmentIntentExtra, sStringYearIntentExtra, sStringSectionIntentExtra;


    private String tempEmail, tempPassword, tableName; /*= "cse_first_year_student_record_a"*/;
    private MaterialTextField materialTextFieldRollNo;
    private NotifyLogout notifyLogoutListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityLoginBinding activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setAllowEnterTransitionOverlap(false);
        }
        Intent intent = getIntent();
        sStringIntentExtra = intent.getStringExtra(USER_TYPE);
        sStringDepartmentIntentExtra = intent.getStringExtra(DepartmentSelectorFragment.DEPARTMENT);
        sStringYearIntentExtra = intent.getStringExtra(DepartmentSelectorFragment.YEAR);
        sStringSectionIntentExtra = intent.getStringExtra(DepartmentSelectorFragment.SECTION);
        tableName = sStringDepartmentIntentExtra+ "_"+ sStringYearIntentExtra + "_student_record_"+ sStringSectionIntentExtra;
        postData = new MultipartBody.Builder();
        bindViews(activityLoginBinding);
        setUserPreferedData();

        mButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (Integer.parseInt(sStringIntentExtra)) {
                    // When User is HOD
                    case 0: {
                        tempEmail = mEditTextEmail.getText().toString();
                        tempPassword = mEditTextPassword.getText().toString();
                        if (TextUtils.isEmpty(tempEmail)) {
                            mEditTextEmail.setError("Enter your Email address");
                        } else if (!AppUtil.isValidEmail(LoginActivity.this, tempEmail)) {
                            AppUtil.showCustomDialog(LoginActivity.this, "Error", "Invalid Email address", true, R.drawable.ic_error_outline);
                        } else if (TextUtils.isEmpty(tempPassword)) {
                            mEditTextPassword.setError("Enter your Password");
                        } else {
                            if (AppUtil.isInternetIsAvailable(LoginActivity.this)) {
                                initLoginLoader();
                            } else {
                                AppUtil.showNoInternetDialog(LoginActivity.this);
                            }
                        }

                    }
                    break;
                    // When User is Faculty
                    case 1: {
                        tempEmail = mEditTextEmail.getText().toString();
                        tempPassword = mEditTextPassword.getText().toString();
                        if (TextUtils.isEmpty(tempEmail)) {
                            mEditTextEmail.setError("Enter your Email address");
                        } else if (!AppUtil.isValidEmail(LoginActivity.this, tempEmail)) {
                            AppUtil.showCustomDialog(LoginActivity.this, "Error", "Invalid Email address", true, R.drawable.ic_error_outline);
                        } else if (TextUtils.isEmpty(tempPassword)) {
                            mEditTextPassword.setError("Enter your Password");
                        } else {
                            if (AppUtil.isInternetIsAvailable(LoginActivity.this)) {
                                initLoginLoader();
                            } else {
                                AppUtil.showNoInternetDialog(LoginActivity.this);
                            }
                        }
                    }
                    break;
                    // When User will be Student
                    case 2: {
                        tempEmail = mEditTextEmail.getText().toString();
                        tempPassword = mEditTextPassword.getText().toString();
                        if (TextUtils.isEmpty(tempEmail)) {
                            mEditTextEmail.setError("Enter your Email address");
                        } else if (!AppUtil.isValidEmail(LoginActivity.this, tempEmail)) {
                            AppUtil.showCustomDialog(LoginActivity.this, "Error", "Invalid Email address", true, R.drawable.ic_error_outline);
                        } else if (TextUtils.isEmpty(tempPassword)) {
                            mEditTextPassword.setError("Enter your University roll number");
                        } else if (!(tempPassword.length() == 10)) {
                            AppUtil.showCustomDialog(LoginActivity.this, "Error", "Invalid Roll No! Please check", true, R.drawable.ic_error_outline);
                        } else {
                            if (AppUtil.isInternetIsAvailable(LoginActivity.this)) {
                                initLoginLoader();
                            } else {
                                AppUtil.showNoInternetDialog(LoginActivity.this);
                            }
                        }
                    }
                    break;
                }
            }
        });
    }

    /**
     * Sets the user prefered image and make elements visible or invisible
     * according to the type of the user
     */
    private void setUserPreferedData() {

        switch (Integer.parseInt(sStringIntentExtra)) {

            case 0:
                mImageViewUserImage.setImageDrawable(getResources()
                        .getDrawable(R.drawable.ic_hod_96));
                setSpannableString(0);
                break;

            case 1:
                mImageViewUserImage.setImageDrawable(getResources()
                        .getDrawable(R.drawable.ic_faculty_96));
                setSpannableString(1);
                break;

            case 2:
                mImageViewUserImage.setImageDrawable(getResources()
                        .getDrawable(R.drawable.ic_student_96));

                mEditTextPassword.setInputType(InputType.TYPE_CLASS_PHONE);
                mEditTextPassword.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
                materialTextFieldRollNo.getLabel().setText(R.string.hint_roll_no);
                setSpannableString(2);
                break;

        }
    }

    /**
     * @prams userType the int to set user prefered
     * spannable strings for the text view forget password
     */

    private void setSpannableString(int userType) {
        switch (userType) {

            case 0: {
                SpannableString spannableString = new SpannableString("Forget your Password ?");
                spannableString.setSpan(new ForegroundColorSpan(Color.BLACK),
                        0, 22, 0);


                ClickableSpan clickableSpanForgetPassword = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        // Open Activity for password reset
                        Toast.makeText(getApplicationContext(), "Open Forget Password" +
                                " Activity", Toast.LENGTH_SHORT).show();
                    }
                };
                spannableString.setSpan(clickableSpanForgetPassword,
                        13, 20, 0);
                spannableString.setSpan(new ForegroundColorSpan(getResources()
                                .getColor(R.color.cyan)),
                        13, 20, 0);

                mTextViewForgerPassword.setMovementMethod(LinkMovementMethod.getInstance());
                mTextViewForgerPassword.setGravity(Gravity.CENTER);
                mTextViewForgerPassword.setWidth(100);
                mTextViewForgerPassword.setBackgroundColor(Color.TRANSPARENT);
                mTextViewForgerPassword.setText(spannableString);
            }
            break;

            case 1: {
                SpannableString spannableString = new SpannableString("Forget your Password ?");
                spannableString.setSpan(new ForegroundColorSpan(Color.BLACK),
                        0, 22, 0);

                ClickableSpan clickableSpanForgetPassword = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        // Open Activity for password reset
                        Toast.makeText(getApplicationContext(), "Open Forget Password" +
                                " Activity", Toast.LENGTH_SHORT).show();
                    }
                };
                spannableString.setSpan(clickableSpanForgetPassword, 13, 20, 0);
                spannableString.setSpan(new ForegroundColorSpan(getResources()
                                .getColor(R.color.cyan)),
                        13, 20, 0);

                mTextViewForgerPassword.setMovementMethod(LinkMovementMethod.getInstance());
                mTextViewForgerPassword.setGravity(Gravity.CENTER);
                mTextViewForgerPassword.setWidth(100);
                mTextViewForgerPassword.setBackgroundColor(Color.TRANSPARENT);
                mTextViewForgerPassword.setText(spannableString);
            }
            break;

            case 2: {
                SpannableString spannableString = new SpannableString("Forget your " +
                        "Password ?");
                spannableString.setSpan(new ForegroundColorSpan(Color.BLACK),
                        0, 22, 0);

                ClickableSpan clickableSpanForgetPassword = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        // Open Activity for password reset
                        Toast.makeText(getApplicationContext(), "Open Forget Password" +
                                " Activity", Toast.LENGTH_SHORT).show();
                    }
                };
                spannableString.setSpan(clickableSpanForgetPassword,
                        12, 20, 0);
                spannableString.setSpan(new ForegroundColorSpan(getResources()
                                .getColor(R.color.cyan)),
                        12, 20, 0);

                mTextViewForgerPassword.setMovementMethod(LinkMovementMethod.getInstance());
                mTextViewForgerPassword.setGravity(Gravity.CENTER);
                mTextViewForgerPassword.setWidth(100);
                mTextViewForgerPassword.setBackgroundColor(Color.TRANSPARENT);
                mTextViewForgerPassword.setText(spannableString);
            }
            break;
        }
    }

    /**
     * Bind all view in this function
     */
    private void bindViews(ActivityLoginBinding activityLoginBinding) {
        mEditTextEmail = activityLoginBinding.editTextEmail;
        mEditTextPassword = activityLoginBinding.editTextPassword;
        mButtonLogin = activityLoginBinding.buttonLogin;
        mTextViewForgerPassword = activityLoginBinding.textForgetPass;
        mImageViewUserImage = activityLoginBinding.imageImageCircular;
        materialTextFieldRollNo = activityLoginBinding.mtfPassword;
    }

    /*Initialize post data here*/
    public void initLoginLoader() {
        AppUtil.showProgressDialog(this);


        operation = NetworkUtility.LOGIN;

        Log.d(TAG, "initLoginLoader: postData email" + tempEmail);
        if (tempEmail.isEmpty()) {
            Toast.makeText(this, "plz enter your email address", Toast.LENGTH_SHORT).show();
        } else {

            postData
                    .addFormDataPart("email", tempEmail)
                    .addFormDataPart("password", tempPassword)
                    .addFormDataPart("usertype", sStringIntentExtra)
                    .addFormDataPart("table_name", tableName);

            getLoaderManager().restartLoader(LOADER_ID, null, this).forceLoad();
        }
    }

    public void finishLoginLoader(String data, int userType) {
        Log.d(TAG, "finishLoginLoader: userType " + userType);
        Intent intent;
        AppUtil.hideProgressDialog();
        if (data.isEmpty()) {
            AppUtil.showCustomDialog(this, "Something Went Wrong", "Failed to connect to server.", true, R.drawable.ic_error_outline);
        } else {
            byte[] decode = Base64.decode(data.getBytes(), Base64.DEFAULT);
            String jsonString = new String(decode);
            Log.d(TAG, "onLoadFinished: JsonResponse " + jsonString);
            if (userType == 0) {
                HodLoginDetail hodLoginDetail = JSONExtractor.extractHodLoginResponce(jsonString);
                if (hodLoginDetail != null) {
                    Log.d(TAG, "finishLoginLoader: startActivity Main");
                    setHodSharedPreference(hodLoginDetail);
                    LoginSession.getInstance(this).setLogIn(true);
                    LoginSession.getInstance(this).setUserType(userType);
                    //TODO make sure what is the next activity for the hod login

                    intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.putExtra(USER_TYPE, "0");
                    startActivity(intent);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("finish_activity"));
                    overridePendingTransition(R.anim.intent_enter_anim, R.anim.intent_exit_anim);
                    finish();
                } else {
                    AppUtil.showCustomDialog(this, "Something Went Wrong", "Failed to connect to server.", true, R.drawable.ic_error_outline);
                }
            } else if (userType == 1) {
                FacultyLoginDetail facultyLoginDetail = JSONExtractor.extractFacultyLoginResponse(jsonString);
                if (facultyLoginDetail != null) {
                    Log.d(TAG, "finishLoginLoader: startActivity Main");
                    setFacultySharedPreference(facultyLoginDetail);
                    LoginSession.getInstance(this).setLogIn(true);
                    LoginSession.getInstance(this).setUserType(userType);
                    intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.putExtra(USER_TYPE, "1");
                    startActivity(intent);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("finish_activity"));
                    overridePendingTransition(R.anim.intent_enter_anim, R.anim.intent_exit_anim);
                    finish();
                } else {
                    AppUtil.showCustomDialog(this, "Something Went Wrong", "Failed to connect to server.", true, R.drawable.ic_error_outline);
                }

            } else if (userType == 2) {
                StudentDetail studentDetail = JSONExtractor.extractStudentResponse(jsonString);
                if (studentDetail != null) {
                    //todo what about the student.....?
                    setStudentSharedPreference(studentDetail);
                    LoginSession.getInstance(this).setLogIn(true);
                    LoginSession.getInstance(this).setUserType(userType);
                    intent = new Intent(LoginActivity.this, StudentActivity.class);
                    intent.putExtra(USER_TYPE, "2");
                    intent.putExtra(DepartmentSelectorFragment.DEPARTMENT, sStringDepartmentIntentExtra);
                    intent.putExtra(DepartmentSelectorFragment.YEAR, sStringYearIntentExtra);
                    intent.putExtra(DepartmentSelectorFragment.SECTION, sStringSectionIntentExtra);
                    LoginSession loginSession = LoginSession.getInstance(this);
                    loginSession.setDepartment(sStringDepartmentIntentExtra);
                    loginSession.setYear(sStringYearIntentExtra);
                    loginSession.setDSection(sStringSectionIntentExtra);
                    startActivity(intent);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("finish_activity"));
                    overridePendingTransition(R.anim.intent_enter_anim, R.anim.intent_exit_anim);
                    finish();
                } else {
                    AppUtil.showCustomDialog(this, "Something Went Wrong", "Failed to connect to server.", true, R.drawable.ic_error_outline);
                }
            }
        }
    }

    private void setFacultySharedPreference(FacultyLoginDetail facultyLoginDetail) {
        SharedPreferences sp = getSharedPreferences(getString(R.string.facutly_login_sp), MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(getString(R.string.faculty_dep), facultyLoginDetail.getFacultyDep());
        editor.putString(getString(R.string.faculty_name), facultyLoginDetail.getFacultyName());
        editor.putString(getString(R.string.faculty_pass), facultyLoginDetail.getFacultyPassword());
        editor.putString(getString(R.string.faculty_email), facultyLoginDetail.getFacultyEmail());
        editor.putString(getString(R.string.faculty_mob), facultyLoginDetail.getFacultyMob());
        editor.putString(getString(R.string.faculty_spe), facultyLoginDetail.getFacultySpec());
        editor.apply();
    }

    private void setHodSharedPreference(HodLoginDetail hodLoginDetail) {
        SharedPreferences sp = getSharedPreferences(getString(R.string.hod_login_sp), MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(getString(R.string.hod_department), hodLoginDetail.getHodDepartment());
        editor.putString(getString(R.string.hod_name), hodLoginDetail.getHodName());
        editor.putString(getString(R.string.hod_email), hodLoginDetail.getHodEmail());
        editor.putString(getString(R.string.dep_key), hodLoginDetail.getDepartmentKey());
        editor.putString(getString(R.string.public_key), hodLoginDetail.getPublicKey());
        editor.apply();
    }

    private void setStudentSharedPreference(StudentDetail studentDetail) {
        SharedPreferences sp = getSharedPreferences(getString(R.string.student_login_sp), MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        editor.putString(getString(R.string.student_name), studentDetail.getStudentName());
        editor.putString(getString(R.string.student_roll_no), studentDetail.getStudentRollno());
        editor.putString(getString(R.string.student_mob), studentDetail.getStudentMob());
        editor.putString(getString(R.string.student_email), studentDetail.getStudentEmail());
        editor.putString(getString(R.string.student_father_name), studentDetail.getStudentFatherName());
        editor.putString(getString(R.string.student_father_mob), studentDetail.getStudentFatherMob());
        editor.putString(getString(R.string.student_group), studentDetail.getStudentGroup());
        editor.putString(getString(R.string.student_img_url), studentDetail.getStudentImgUrl());
        editor.apply();
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        return new LoginAsyncLoader(this, postData, operation);
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        finishLoginLoader(data, Integer.parseInt(sStringIntentExtra));
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {

    }


    static class LoginAsyncLoader extends AsyncTaskLoader<String> {
        private static final String TAG = "LoginAsyncLoader";
        MultipartBody.Builder postData;
        String operation;

        public LoginAsyncLoader(Context context, MultipartBody.Builder postData, String operation) {
            super(context);
            this.postData = postData;
            this.operation = operation;
        }

        @Override
        public String loadInBackground() {
            String response = NetworkUtility.getServerResponse(getContext(), operation, postData);
            Log.d(TAG, "loadInBackground: UserLogin Response" + response);
            return response;
        }
    }

}

