package work.ravi.com.ams.ui.activity;

import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Loader;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import work.ravi.com.ams.R;
import work.ravi.com.ams.databinding.ActivityViewAttendanceBinding;
import work.ravi.com.ams.pojo.SubjectDetail;
import work.ravi.com.ams.pojo.TotalAttendanceDetail;
import work.ravi.com.ams.util.AppUtil;
import work.ravi.com.ams.util.JSONExtractor;
import work.ravi.com.ams.util.NetworkUtility;

import static android.content.ContentValues.TAG;


/**
 * Created by acer on 14-03-2018.
 */

public class ViewAttendanceActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<String>, CompoundButton.OnCheckedChangeListener {

    //private View dummyChartBinding;
    private ActivityViewAttendanceBinding activityViewAttendanceBinding;
    private String operation;
    private MultipartBody.Builder postData;
    private int LOADER_ID = 1;
    private BarChart compiledBarChart, totalBarChart, absentBarChart;
    private String attendanceDataLable[];
    private boolean isFirtLoader = false;
    private Switch aSwitch;
    private float attendanceData[];
    private float totalAttendanceData[];
    private float absentData[];
    private int totalSubjectSize = 0;
    private String tableName;
    private String department;
    private String year;
    private String section;
    private String rollno;
    private TextView studentNameForGraph;
    private ArrayList<TotalAttendanceDetail> totalAttendanceDetail;
    private String sname;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityViewAttendanceBinding = DataBindingUtil.setContentView(this, R.layout.activity_view_attendance);
        compiledBarChart = activityViewAttendanceBinding.compiledChart;
        totalBarChart = activityViewAttendanceBinding.totalChart;
        absentBarChart = activityViewAttendanceBinding.absentChart;
        aSwitch = activityViewAttendanceBinding.switcher;
        studentNameForGraph = activityViewAttendanceBinding.studentNameForGraph;
        getBundleExtras();

        studentNameForGraph.setText(sname);

        postData = new MultipartBody.Builder();

        initGetSubjectDetail();

        aSwitch.setOnCheckedChangeListener(this);
    }

    private void getBundleExtras() {
        department = getIntent().getExtras().getString("g_department");
        year = getIntent().getExtras().getString("g_year");
        section = getIntent().getExtras().getString("g_section");
        rollno = getIntent().getExtras().getString("g_rollno");
        sname = getIntent().getExtras().getString("g_student_name");

        tableName = department + "_" + year;
        Log.d(TAG, "getBundleExtras: " + tableName);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        if (isChecked) {
            for (int i = 0; i < totalSubjectSize; i++) {
                if ((totalAttendanceDetail.get(i).getTotal()) == 0) {
                    attendanceData[i] = 0;
                    totalAttendanceData[i] = 0;
                    absentData[i] = 0;
                } else {
                    attendanceData[i] = (totalAttendanceDetail.get(i).getPresent()) * 100 / (totalAttendanceDetail.get(i).getTotal());
                    totalAttendanceData[i] = (totalAttendanceDetail.get(i).getTotal()) * 100 / (totalAttendanceDetail.get(i).getTotal());
                    absentData[i] = (totalAttendanceDetail.get(i).getAbsent()) * 100 / (totalAttendanceDetail.get(i).getTotal());
                }
            }
            initGraph();
        } else {
            for (int i = 0; i < totalSubjectSize; i++) {
                attendanceData[i] = totalAttendanceDetail.get(i).getPresent();
                totalAttendanceData[i] = totalAttendanceDetail.get(i).getTotal();
                absentData[i] = totalAttendanceDetail.get(i).getAbsent();
            }
            initGraph();
        }
    }

    private void initGraph() {
        chartInit(compiledBarChart, totalAttendanceDetail.size(), attendanceData, attendanceDataLable);
        chartInit(totalBarChart, totalAttendanceDetail.size(), totalAttendanceData, attendanceDataLable);
        chartInit(absentBarChart, totalAttendanceDetail.size(), absentData, attendanceDataLable);
    }

    private void initGetSubjectDetail() {
        AppUtil.showProgressDialog(this);
        isFirtLoader = true;
        operation = NetworkUtility.ALL_SUBJECT;
        postData
                .addFormDataPart("table_name", tableName);
        getLoaderManager().restartLoader(LOADER_ID, null, this).forceLoad();
    }

    private void initDummyChartActivity() {

        AppUtil.showProgressDialog(this);
        operation = NetworkUtility.TOTAL_ATTENDANCE;
        postData
                .addFormDataPart("table_name", tableName)
                .addFormDataPart("roll_no", rollno)
                .addFormDataPart("section", section);
        getLoaderManager().restartLoader(LOADER_ID, null, this).forceLoad();

    }

    private void finishDummyChartActivity(String data) {
        AppUtil.hideProgressDialog();
        if (data.isEmpty()) {
            AppUtil.showCustomDialog(this, "Something Went Wrong", "Failed to connect to server.", true, R.drawable.ic_error_outline);
        } else {
            byte[] decode = Base64.decode(data.getBytes(), Base64.DEFAULT);
            String jsonString = new String(decode);

            String[] response = JSONExtractor.getStatusMessage(jsonString);
            String status = response[0];
            String message = response[1];
            if (status.equals("0")) {
                AppUtil.showCustomDialog(this, "Failed", message, true, R.drawable.ic_error_outline);
            } else {
                totalAttendanceDetail = JSONExtractor.extractTotalAttendanceResponse(jsonString);
                totalSubjectSize = totalAttendanceDetail.size();

                attendanceData = new float[totalSubjectSize];
                totalAttendanceData = new float[totalSubjectSize];
                absentData = new float[totalSubjectSize];

                for (int i = 0; i < totalSubjectSize; i++) {
                    attendanceData[i] = totalAttendanceDetail.get(i).getPresent();
                    totalAttendanceData[i] = totalAttendanceDetail.get(i).getTotal();
                    absentData[i] = totalAttendanceDetail.get(i).getAbsent();
                }
                initGraph();
            }
        }
    }

    private void finishGetSubjectDetail(String data) {
        AppUtil.hideProgressDialog();
        if (data.isEmpty()) {
            AppUtil.showCustomDialog(this, "Something Went Wrong", "Failed to connect to server.", true, R.drawable.ic_error_outline);
        } else {
            byte[] decode = Base64.decode(data.getBytes(), Base64.DEFAULT);
            String jsonString = new String(decode);

            String[] response = JSONExtractor.getStatusMessage(jsonString);
            String status = response[0];
            String message = response[1];
            if (status.equals("0")) {
                AppUtil.showCustomDialog(this, "Failed", message, true, R.drawable.ic_error_outline);
            } else {
                ArrayList<SubjectDetail> subjectDetailArrayList = JSONExtractor.extractSubjectDetailResponse(jsonString);
                attendanceDataLable = new String[subjectDetailArrayList.size()];
                for (int i = 0; i < subjectDetailArrayList.size(); i++) {
                    attendanceDataLable[i] = subjectDetailArrayList.get(i).getSubjectSrct().toUpperCase();
                }
                initDummyChartActivity();
            }
        }
    }

    public void chartInit(BarChart barChart1, int size, float data[], String label[]) {

        BarDataSet bardataset = new BarDataSet(barEntityList(size, data), "Cells");
        BarData barData = new BarData(barDataList(size, label), bardataset);
        bardataset.setColors(ColorTemplate.COLORFUL_COLORS);
        barChart1.animateY(3000);
        barChart1.setData(barData);

    }

    public ArrayList<String> barDataList(int size, String data[]) {
        ArrayList<String> labels = new ArrayList<String>();
        for (int i = 0; i < size; i++) {
            labels.add(data[i]);
        }
        return labels;
    }

    public ArrayList<BarEntry> barEntityList(int size, float data[]) {

        ArrayList<BarEntry> bargroup1 = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            bargroup1.add(new BarEntry(data[i], i));
        }
        return bargroup1;
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        return new DummyChartAsyncLoader(this, postData, operation);
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {

        if (isFirtLoader) {
            isFirtLoader = false;
            finishGetSubjectDetail(data);
        } else {
            finishDummyChartActivity(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {

    }

    static class DummyChartAsyncLoader extends AsyncTaskLoader<String> {
        private static final String TAG = "ActivityViewAttendance";
        MultipartBody.Builder postData;
        String operation;

        public DummyChartAsyncLoader(Context context, MultipartBody.Builder postData, String operation) {
            super(context);
            this.postData = postData;
            this.operation = operation;
        }

        @Override
        public String loadInBackground() {
            String response = NetworkUtility.getServerResponse(getContext(), operation, postData);
            Log.d(TAG, "loadInBackground: response " + response);
            return response;
        }
    }
}
