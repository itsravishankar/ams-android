package work.ravi.com.ams.ui.activity;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import de.hdodenhof.circleimageview.CircleImageView;
import work.ravi.com.ams.R;
import work.ravi.com.ams.databinding.ActivityStudentBinding;
import work.ravi.com.ams.ui.activity.prelogin.PreLoginActivity;
import work.ravi.com.ams.ui.fragment.selector.DepartmentSelectorFragment;
import work.ravi.com.ams.ui.fragment.student.StudentAttendanceFragment;
import work.ravi.com.ams.ui.fragment.student.StudentHomeFragment;
import work.ravi.com.ams.ui.fragment.student.StudentProfileFragment;
import work.ravi.com.ams.sharedprefrence.LoginSession;

public class StudentActivity extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout mRelativeLayoutContainer;
    private Toolbar mToolbar;
    private ImageView mImageViewLogOut;
    private CircleImageView circleImageView;
    public static String sStringDepartmentIntentExtra, sStringYearIntentExtra, sStringSectionIntentExtra;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_home:
                onSwapHomeFragment();
                return true;
            /*case R.id.navigation_attendance:
                onSwapAttendanceFragment();
                return true;*/
            case R.id.navigation_profile:
                onSwapProfileFragment();
                return true;
        }
        return false;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityStudentBinding activityStudentBinding = DataBindingUtil
                .setContentView(this, R.layout.activity_student);
        mRelativeLayoutContainer = activityStudentBinding.rlFragmentContainer;
        Intent intent = getIntent();
        sStringDepartmentIntentExtra = intent.getStringExtra(DepartmentSelectorFragment.DEPARTMENT);
        sStringYearIntentExtra = intent.getStringExtra(DepartmentSelectorFragment.YEAR);
        sStringSectionIntentExtra = intent.getStringExtra(DepartmentSelectorFragment.SECTION);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        mToolbar = activityStudentBinding.toolbarStudentActivity;
        mImageViewLogOut = activityStudentBinding.imageViewLogout;
        circleImageView = activityStudentBinding.imageViewStudentPic;
        circleImageView.setOnClickListener(this);
        mImageViewLogOut.setOnClickListener(this);

        onSwapHomeFragment();
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.student_menu, menu);
        return true;
    }

    public void showToolbar(boolean b) {
        if (b)
            mToolbar.setVisibility(View.VISIBLE);
        else mToolbar.setVisibility(View.GONE);
    }

    public void logOut() {
        clearSharedPreference();
        Intent intent = new Intent(StudentActivity.this, PreLoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.log_out) {
            clearSharedPreference();
            Intent intent = new Intent(StudentActivity.this, PreLoginActivity.class);
            startActivity(intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*clear sharedPreference*/
    private void clearSharedPreference() {
        LoginSession.getInstance(this).setLogIn(false);
        SharedPreferences sp = getSharedPreferences("2", MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(getString(R.string.student_name));
        editor.remove(getString(R.string.student_roll_no));
        editor.remove(getString(R.string.student_mob));
        editor.remove(getString(R.string.student_email));
        editor.remove(getString(R.string.student_father_name));
        editor.remove(getString(R.string.student_father_mob));
        editor.remove(getString(R.string.student_group));
        editor.remove(getString(R.string.student_img_url));
        editor.apply();
    }

    private void onSwapHomeFragment() {
        showToolbar(true);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        StudentHomeFragment studentHomeFragment = new StudentHomeFragment();
        fragmentTransaction.replace(R.id.rl_fragment_container, studentHomeFragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    private void onSwapProfileFragment() {
        showToolbar(false);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        StudentProfileFragment studentProfileFragment = new StudentProfileFragment();
        fragmentTransaction.replace(R.id.rl_fragment_container, studentProfileFragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    private void onSwapAttendanceFragment() {
        showToolbar(true);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        StudentAttendanceFragment studentAttendanceFragment = new StudentAttendanceFragment();
        fragmentTransaction.replace(R.id.rl_fragment_container, studentAttendanceFragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.image_view_student_pic:
                //openAttendanceReport();
                break;
            case R.id.imageView_logout:
                logOut();
                break;
        }

    }

    public void openAttendanceReport() {
        Intent attendancGraphIntent = new Intent(this, ViewAttendanceActivity.class);
        attendancGraphIntent.putExtra("g_department", sStringDepartmentIntentExtra);
        attendancGraphIntent.putExtra("g_year", sStringYearIntentExtra);
        attendancGraphIntent.putExtra("g_section", sStringSectionIntentExtra);
        attendancGraphIntent.putExtra("g_rollno", getValueByPrefrenceForStudent(getString(R.string.student_roll_no)));
        attendancGraphIntent.putExtra("g_student_name", getValueByPrefrenceForStudent(getString(R.string.student_name)));
        startActivity(attendancGraphIntent);
    }

    public String getValueByPrefrenceForStudent(String key) {
        String value = null;
        SharedPreferences userDetails = this.getSharedPreferences(getString(R.string.student_login_sp), MODE_PRIVATE);
        value = userDetails.getString(key, "Dummy Value").toString();
        return value;
    }
}
