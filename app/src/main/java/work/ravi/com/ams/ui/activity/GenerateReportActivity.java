package work.ravi.com.ams.ui.activity;

import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;

import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import work.ravi.com.ams.R;
import work.ravi.com.ams.adapter.GenerateReportAdapter;
import work.ravi.com.ams.databinding.ActivityGenerateReportBinding;
import work.ravi.com.ams.pojo.GenerateReportList;
import work.ravi.com.ams.pojo.StudentDetail;
import work.ravi.com.ams.pojo.TotalAttendanceDetail;
import work.ravi.com.ams.ui.fragment.selector.DepartmentSelectorFragment;
import work.ravi.com.ams.util.AppUtil;
import work.ravi.com.ams.util.JSONExtractor;
import work.ravi.com.ams.util.NetworkUtility;

import static android.content.ContentValues.TAG;


public class GenerateReportActivity extends AppCompatActivity implements View.OnClickListener, LoaderManager.LoaderCallbacks<String> {


    private ActivityGenerateReportBinding generateReportBinding;
    private MultipartBody.Builder postData;
    String operation;
    private int LOADER_ID = 0;
    private ArrayList<TotalAttendanceDetail> totalAttendanceDetail;
    private ArrayList<String> mappedRollno;
    private ArrayList<GenerateReportList> generateReportLists;

    private GenerateReportAdapter generateReportAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView generateRecyclerView;
    private String tableName;
    private String department;
    private String year;
    private String section;
    private boolean isGenerateReportLoader = false;
    private ArrayList<StudentDetail> studentDetailArrayList;
    private String recordTableName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        generateReportBinding = DataBindingUtil.setContentView(this, R.layout.activity_generate_report);
        getBundleExtras();
        bindViews();
        postData = new MultipartBody.Builder();
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("finish_activity"));
        generateReportLists = new ArrayList<>();
        studentDetailArrayList = new ArrayList<>();
        generateReportAdapter = new GenerateReportAdapter(this, generateReportLists, studentDetailArrayList, department, year, section);


        mLayoutManager = new GridLayoutManager(this, 1);
        generateRecyclerView.setLayoutManager(mLayoutManager);
        generateRecyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        generateRecyclerView.setItemAnimator(new DefaultItemAnimator());

        initGenerateReportLoader();
    }

    private void getBundleExtras() {
        department = getIntent().getExtras().getString(DepartmentSelectorFragment.DEPARTMENT);
        year = getIntent().getExtras().getString(DepartmentSelectorFragment.YEAR);
        section = getIntent().getExtras().getString(DepartmentSelectorFragment.SECTION);
        tableName = department + "_" + year;
        recordTableName = this.department + "_" + this.year + "_student_record_" + this.section;
        Log.d(TAG, "getBundleExtras: " + tableName);
    }

    /**
     * Bind all view in this function
     */
    private void bindViews() {
        //generateRecyclerView =
        generateRecyclerView = generateReportBinding.recyclerViewStudent;
    }

    private void initGenerateReportLoader() {

        isGenerateReportLoader = true;
        operation = NetworkUtility.GENERATE_REPORT;
        postData
                .addFormDataPart("table_name", tableName)//TODO make Dynamic
                .addFormDataPart("section", section);//TODO make Dynamic
        getLoaderManager().restartLoader(LOADER_ID, null, this).forceLoad();
    }

    public void initGenRepLoader() {

        AppUtil.showProgressDialog(this);
        operation = NetworkUtility.STUDENT_ALL_DETAIL;
        postData
                .addFormDataPart("table_name", recordTableName);
        getLoaderManager().restartLoader(LOADER_ID, null, this).forceLoad();
    }

    private void finishGetSubjectDetail(String data) {
        AppUtil.hideProgressDialog();
        if (data.isEmpty()) {
            AppUtil.showCustomDialog(this, "Something Went Wrong", "Failed to connect to server.", true, R.drawable.ic_error_outline);
        } else {
            byte[] decode = Base64.decode(data.getBytes(), Base64.DEFAULT);
            String jsonString = new String(decode);

            String[] response = JSONExtractor.getStatusMessage(jsonString);
            String status = response[0];
            String message = response[1];
            if (status.equals("0")) {
                AppUtil.showCustomDialog(this, "Failed", message, true, R.drawable.ic_error_outline);
            } else {
                mappedRollno = JSONExtractor.extractMappedStudentResponse(jsonString);
                for (int i = 0; i < mappedRollno.size(); i++) {
                    totalAttendanceDetail = JSONExtractor.extractGenerateReportResponse(jsonString, mappedRollno.get(i));
                    GenerateReportList generateReports = new GenerateReportList(totalAttendanceDetail, mappedRollno.get(i));
                    generateReportLists.add(generateReports);
                }
                initGenRepLoader();
                // Log.d("GenerateReportActivity", "finishGetSubjectDetail: arrayList Size :" + totalAttendanceDetail.size());
                // Log.d("GenerateReportActivity", "finishGetSubjectDetail: arrayList Size :" + generateReportLists.size());

            }
        }
    }

    private void finishGenerateReportLoader(String data) {
        AppUtil.hideProgressDialog();
        if (data.isEmpty()) {
            AppUtil.showCustomDialog(this, "Something Went Wrong", "Failed to connect to server.", true, R.drawable.ic_error_outline);
        } else {
            byte[] decode = Base64.decode(data.getBytes(), Base64.DEFAULT);
            String jsonString = new String(decode);


            String[] response = JSONExtractor.getStatusMessage(jsonString);
            String status = response[0];
            String message = response[1];
            if (status.equals("0")) {
                AppUtil.showCustomDialog(this, "Failed", message, true, R.drawable.ic_error_outline);
            } else {
                studentDetailArrayList = JSONExtractor.extractStudentAllDetailResponse(jsonString);
                Log.d(TAG, "finishGenerateReportLoader: size of studentDetailArrayList " + studentDetailArrayList.size());
                generateReportAdapter = new GenerateReportAdapter(this, generateReportLists, studentDetailArrayList, department, year, section);
                generateRecyclerView.setAdapter(generateReportAdapter);
            }
        }
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        private GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /*converter dp to px*/
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        return new GenerateReportAsyncLoader(this, postData, operation);
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {

        if (isGenerateReportLoader) {
            isGenerateReportLoader = false;
            finishGetSubjectDetail(data);
        } else {
            finishGenerateReportLoader(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {

    }

    static class GenerateReportAsyncLoader extends AsyncTaskLoader<String> {
        private static String TAG = "GenerateReportAsyncLoader";
        MultipartBody.Builder postData;
        String operation;

        public GenerateReportAsyncLoader(Context context, MultipartBody.Builder postData, String operation) {
            super(context);
            this.postData = postData;
            this.operation = operation;
        }

        @Override
        public String loadInBackground() {
            String response = NetworkUtility.getServerResponse(getContext(), operation, postData);
            Log.d(TAG, "loadInBackground: generateResponse " + response);
            return response;
        }
    }
}
