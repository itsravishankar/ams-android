package work.ravi.com.ams.adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import work.ravi.com.ams.R;
import work.ravi.com.ams.databinding.NotifyStudentItemBinding;
import work.ravi.com.ams.pojo.GenerateReportList;
import work.ravi.com.ams.pojo.StudentDetail;

import static android.content.ContentValues.TAG;


public class NotifyStudentAdapter extends RecyclerView.Adapter<NotifyStudentAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<GenerateReportList> generateReportLists;
    private ArrayList<StudentDetail> studentDetailArrayList;
    private int lastPosition = -1;
    private ProgressBar mProgressBar;
    private SmsManager smsManager;

    public NotifyStudentAdapter(Context mContext, ArrayList<GenerateReportList> generateReportLists, ArrayList<StudentDetail> studentDetailArrayList) {
        this.mContext = mContext;
        this.generateReportLists = generateReportLists;
        this.studentDetailArrayList = studentDetailArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        NotifyStudentItemBinding notifyStudentItemBinding = DataBindingUtil.inflate(layoutInflater, R.layout.notify_student_item, parent, false);
        return new MyViewHolder(notifyStudentItemBinding);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final GenerateReportList generateReportList = generateReportLists.get(position);
        int p = 0, a = 0, t = 0, totalP = 0;

        for (int i = 0; i < generateReportList.getTotalAttendanceDetail().size(); i++) {
            p = p + generateReportList.getTotalAttendanceDetail().get(i).getPresent();
            a = a + generateReportList.getTotalAttendanceDetail().get(i).getAbsent();
            t = t + generateReportList.getTotalAttendanceDetail().get(i).getTotal();
        }
        totalP = (p * 100) / t;

        holder.studentRoll.setText(generateReportList.getRollNo());
        holder.studentName.setText(studentDetailArrayList.get(position).getStudentName());
        updateProgressBar(p);
        holder.compiledTotal.setText(String.valueOf(totalP));
        //holder.studentRoll.setText("P :"+String.valueOf(p)+" A :"+String.valueOf(a)+" T :"+String.valueOf(t));//Todo use for print
        Log.d(TAG, "onBindViewHolder: Gl->GetTA :" + generateReportList.getTotalAttendanceDetail().size());


        if (totalP <= 75) {
            holder.callBt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialContactPhone(studentDetailArrayList.get(position).getStudentMob());
                }
            });
            holder.smsBt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String body = "Dear " + studentDetailArrayList.get(position).getStudentName() + ",\n" +//todo change msg ......
                            "\n" +
                            "This is to inform you that your attendance has fallen below the given criteria.\n" +
                            "Thus, extreme action will be taken by the concerned officials. \n" +
                            "Make sure you attend regular classes to avoid any detention from the final exams.\n" +
                            "Regards,\n" +
                            "\n" +
                            "HOD\n" +
                            "IIMT College of Engg. Greater Noida";
                    sendSMS(studentDetailArrayList.get(position).getStudentMob().trim(), body);
                }
            });
            holder.emailBt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    sendMail("Warning Letter", studentDetailArrayList.get(position).getStudentEmail(), studentDetailArrayList.get(position).getStudentName());
                }
            });
        } else {

            holder.callBt.setEnabled(false);
            holder.smsBt.setEnabled(false);
            holder.emailBt.setEnabled(false);
        }
        setAnimation(holder.itemView, position);

    }

    private void updateProgressBar(int value) {
        if (android.os.Build.VERSION.SDK_INT >= 11) {
            // will update the "progress" propriety of seekbar until it reaches progress
            ObjectAnimator animation = ObjectAnimator.ofInt(mProgressBar, "progress", value);
            animation.setDuration(1500); // 1.5 second
            animation.setInterpolator(new DecelerateInterpolator());
            animation.start();
        } else
            mProgressBar.setProgress(50);
    }

    @Override
    public int getItemCount() {
        return generateReportLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView studentRoll, studentName, compiledTotal;
        private Button callBt, smsBt, emailBt;


        public MyViewHolder(NotifyStudentItemBinding notifyStudentItemBinding) {
            super(notifyStudentItemBinding.getRoot());
            smsManager = SmsManager.getDefault();
            callBt = notifyStudentItemBinding.call;
            smsBt = notifyStudentItemBinding.sms;
            emailBt = notifyStudentItemBinding.email;
            studentRoll = notifyStudentItemBinding.tvRoll;
            studentName = notifyStudentItemBinding.tvName;
            compiledTotal = notifyStudentItemBinding.progressBarNotifyStudent.complianceObservations;
            mProgressBar = notifyStudentItemBinding.progressBarNotifyStudent.circleProgressBar;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                mProgressBar.setProgress(0, true);
            } else mProgressBar.setProgress(0);
        }
    }

    private void sendMail(String subject, String emailAddress, String studentName) {
        String body = "Dear " + studentName + ",\n" +
                "\n" +
                "This is to inform you that your attendance has fallen below the given criteria.\n" +
                "Thus, extreme action will be taken by the concerned officials. \n" +
                "Make sure you attend regular classes to avoid any detention from the final exams.\n" +
                "Regards,\n" +
                "\n" +
                "HOD\n" +
                "IIMT College of Engg. Greater Noida";
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", emailAddress, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, body);
        mContext.startActivity(Intent.createChooser(emailIntent, "Reach to Us..."));
    }

    private void dialContactPhone(final String phoneNumber) {
        mContext.startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNumber, null)));
    }

    private void sendSMS(String smsNumber, String smsText) {
        Uri uri = Uri.parse("smsto:" + smsNumber);
        Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
        intent.putExtra("sms_body", smsText);
        mContext.startActivity(intent);
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.zoom_in_anim);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}