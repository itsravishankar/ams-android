package work.ravi.com.ams.interfaces;

/**
 * Created by ravi on 7/4/18.
 */

public interface NotifyLogout {
    public void notifyOnLogout();
}
