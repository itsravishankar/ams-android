package work.ravi.com.ams.adapter.spinner;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import work.ravi.com.ams.R;
import work.ravi.com.ams.pojo.DepartmentDetail;

/**
 * Created by Ravi on 04-03-2018.
 */

public class CustomSectionSpinnerAdapter extends ArrayAdapter<String> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final List<String> sectionList;
    private final int mResourceLayout;

    public CustomSectionSpinnerAdapter(@NonNull Context context, @LayoutRes int resource,
                                       @NonNull List objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResourceLayout = resource;
        sectionList = objects;
    }
    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResourceLayout, parent, false);

        TextView textViewSection = view.findViewById(R.id.text_view_section);
        TextView textViewSectionLogo = view.findViewById(R.id.text_view_section_icon);


        textViewSection.setText("Section " + sectionList.get(position));
        textViewSectionLogo.setText(sectionList.get(position));

        return view;
    }
}
