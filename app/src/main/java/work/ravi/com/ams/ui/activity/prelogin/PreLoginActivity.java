package work.ravi.com.ams.ui.activity.prelogin;

import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import work.ravi.com.ams.R;
import work.ravi.com.ams.databinding.ActivityPreLoginBinding;
import work.ravi.com.ams.ui.activity.MainActivity;
import work.ravi.com.ams.ui.activity.SelectorActivity;

/**
 * Created by Ravi on 29-12-2017.
 */

public class PreLoginActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String HOD = "0";
    public static final String FACULTY = "1";
    public static final String STUDENT = "2";
    private CardView mCardViewHODLogin, mCardViewFacultyLogin, mCardViewStudentLogin;
    private ImageView mImageViewHod, mImageViewFaculty, mImageViewStudent;
    private ActivityPreLoginBinding activityPreLoginBinding;

    private BroadcastReceiver act2InitReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityPreLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_pre_login);
        initToolbar();
        bindViews();
        clickListeners();
        act2InitReceiver= new BroadcastReceiver()
        {

            @Override
            public void onReceive(Context context, Intent intent)
            {
                finish();
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(act2InitReceiver, new IntentFilter("finish_activity"));

    }

    /**
     * All Click Listeners are set in this method
     */
    private void clickListeners() {
        mCardViewHODLogin.setOnClickListener(this);
        mCardViewFacultyLogin.setOnClickListener(this);
        mCardViewStudentLogin.setOnClickListener(this);
    }

    /**
     * Bind all view in this function
     */
    private void bindViews() {
        mCardViewHODLogin = activityPreLoginBinding.layoutPreLogin.cardLayoutHod;
        mCardViewFacultyLogin = activityPreLoginBinding.layoutPreLogin.cardLayoutFaculty;
        mCardViewStudentLogin = activityPreLoginBinding.layoutPreLogin.cardLayoutStudent;
        mImageViewHod = activityPreLoginBinding.layoutPreLogin.imageViewHod;
        mImageViewFaculty = activityPreLoginBinding.layoutPreLogin.imageViewFaculty;
        mImageViewStudent = activityPreLoginBinding.layoutPreLogin.imageViewStudent;
    }

    /**
     * Toolbar initilization is done inside this function
     */
    private void initToolbar() {
        Toolbar toolbar = activityPreLoginBinding.toolbarid;
        setSupportActionBar(toolbar);
        ActionBar mActionBarSupport = getSupportActionBar();
        if (mActionBarSupport != null) {
            mActionBarSupport.setDisplayUseLogoEnabled(true);
            mActionBarSupport.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(act2InitReceiver);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        Intent intentLogin = new Intent(PreLoginActivity.this, LoginActivity.class);
        ActivityOptions activityOptions = null;
        switch (id){

            case R.id.card_layout_hod :
                intentLogin.putExtra("USER_TYPE", HOD);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    activityOptions = ActivityOptions.makeSceneTransitionAnimation(PreLoginActivity.this, mImageViewHod, "login");
                    startActivity(intentLogin, activityOptions.toBundle());
                } else startActivity(intentLogin);
                break;

            case R.id.card_layout_faculty :
                intentLogin.putExtra("USER_TYPE", FACULTY);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    activityOptions = ActivityOptions.makeSceneTransitionAnimation(PreLoginActivity.this, mImageViewFaculty, "login");
                    startActivity(intentLogin, activityOptions.toBundle());
                } else startActivity(intentLogin);
                break;

            case R.id.card_layout_student :

                Intent intentSelector1 = new Intent(PreLoginActivity.this, SelectorActivity.class);
                intentSelector1.putExtra("activity_indicator", "student_login");
                startActivity(intentSelector1);
                /*intentLogin.putExtra("USER_TYPE", STUDENT);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    activityOptions = ActivityOptions.makeSceneTransitionAnimation(PreLoginActivity.this, mImageViewStudent, "login");
                    startActivity(intentLogin, activityOptions.toBundle());
                } else startActivity(intentLogin);*/
                break;

        }

    }
}
