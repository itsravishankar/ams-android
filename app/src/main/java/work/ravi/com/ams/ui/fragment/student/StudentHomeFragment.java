package work.ravi.com.ams.ui.fragment.student;

import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.transition.TransitionManager;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import work.ravi.com.ams.R;
import work.ravi.com.ams.adapter.GenerateReportAdapter;
import work.ravi.com.ams.databinding.FragmentStudentHomeBinding;
import work.ravi.com.ams.pojo.GenerateReportList;
import work.ravi.com.ams.pojo.StudentDetail;
import work.ravi.com.ams.pojo.TotalAttendanceDetail;
import work.ravi.com.ams.sharedprefrence.LoginSession;
import work.ravi.com.ams.ui.activity.StudentActivity;
import work.ravi.com.ams.ui.activity.ViewAttendanceActivity;
import work.ravi.com.ams.util.AppUtil;
import work.ravi.com.ams.util.JSONExtractor;
import work.ravi.com.ams.util.NetworkUtility;

import static android.content.ContentValues.TAG;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Ravi on 3/17/2018.
 */

public class StudentHomeFragment extends Fragment implements LoaderManager.LoaderCallbacks<String> {

    private FragmentStudentHomeBinding fragmentStudentHomeBinding;
    private ProgressBar mProgressBar;
    private ImageView mImageView;
    private ConstraintLayout mConstraintLayout;
    private boolean isLayoutLarge;

    private MultipartBody.Builder postData;
    String operation;
    private int LOADER_ID = 0;
    private ArrayList<TotalAttendanceDetail> totalAttendanceDetail;
    private ArrayList<GenerateReportList> generateReportLists;
    private ArrayList<String> mappedRollno;
    private String tableName;
    private String department;
    private String year;
    private String section;
    private boolean isGenerateReportLoader = false;
    private ArrayList<StudentDetail> studentDetailArrayList;
    private String recordTableName;
    TotalAttendanceDetail attendanceDetail;
    private TextView mTextViewName, mTextViewBranch, mTextViewTotal,
            mTextViewPresent, mTextViewAbsent, mTextViewAttendancePercentage;
    private Button mButtonAttendanceReport;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        fragmentStudentHomeBinding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_student_home, container, false);
        mProgressBar = fragmentStudentHomeBinding.circleProgressBar;
        mImageView = fragmentStudentHomeBinding.imageViewStudent;
        mConstraintLayout = fragmentStudentHomeBinding.constrainLayout;
        mTextViewName = fragmentStudentHomeBinding.tvStudentName;
        mTextViewAttendancePercentage = fragmentStudentHomeBinding.complianceObservations;
        mTextViewBranch = fragmentStudentHomeBinding.tvBranch;
        mTextViewTotal = fragmentStudentHomeBinding.tvTotoal;
        mTextViewAbsent = fragmentStudentHomeBinding.tvAbsent;
        mTextViewPresent = fragmentStudentHomeBinding.tvPresent;
        mButtonAttendanceReport = fragmentStudentHomeBinding.btnAttendanceReport;

        postData = new MultipartBody.Builder();

        generateReportLists = new ArrayList<>();
        studentDetailArrayList = new ArrayList<>();
        LoginSession loginSession = LoginSession.getInstance(getActivity());
        department = loginSession.getDepartment();
        year = loginSession.getYear();
        section = loginSession.getSection();

        tableName = department + "_" + year;
        recordTableName = this.department + "_" + this.year + "_student_record_" + this.section;

        initGenerateReportLoader();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mProgressBar.setProgress(0, true);
        } else mProgressBar.setProgress(0);
        return fragmentStudentHomeBinding.getRoot();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateProgressBar(0);
        mImageView.setOnClickListener(view1 -> {
            onUiChange();
        });
        mButtonAttendanceReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent attendancGraphIntent = new Intent(getActivity(), ViewAttendanceActivity.class);
                attendancGraphIntent.putExtra("g_department", department);
                attendancGraphIntent.putExtra("g_year", year);
                attendancGraphIntent.putExtra("g_section", section);
                attendancGraphIntent.putExtra("g_rollno", getValueByPrefrenceForStudent(getString(R.string.student_roll_no)));
                attendancGraphIntent.putExtra("g_student_name", getValueByPrefrenceForStudent(getString(R.string.student_name)));
                startActivity(attendancGraphIntent);
            }
        });
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mProgressBar.setProgress(80, true);
        } else mProgressBar.setProgress(80);*/
    }

    private void onUiChange() {
        if (isLayoutLarge) {
            isLayoutLarge = false;
            ConstraintSet constraintSet1 = new ConstraintSet();
            ConstraintSet constraintSet2 = new ConstraintSet();
            constraintSet2.clone(getActivity(), R.layout.fragment_student_home);
            constraintSet1.clone(mConstraintLayout);
            TransitionManager.beginDelayedTransition(mConstraintLayout);
            constraintSet2.applyTo(mConstraintLayout);
        } else {
            isLayoutLarge = true;
            ConstraintSet constraintSet1 = new ConstraintSet();
            ConstraintSet constraintSet2 = new ConstraintSet();
            constraintSet2.clone(getActivity(), R.layout.fragment_student_home_large);
            constraintSet1.clone(mConstraintLayout);
            TransitionManager.beginDelayedTransition(mConstraintLayout);
            constraintSet2.applyTo(mConstraintLayout);
        }
    }

    private void updateProgressBar(int progress) {
        if (android.os.Build.VERSION.SDK_INT >= 11) {
            // will update the "progress" propriety of seekbar until it reaches progress
            ObjectAnimator animation = ObjectAnimator.ofInt(mProgressBar, "progress", progress);
            animation.setDuration(1500); // 1.5 second
            animation.setInterpolator(new DecelerateInterpolator());
            animation.start();
        } else
            mProgressBar.setProgress(progress);
    }


    private void initGenerateReportLoader() {

        isGenerateReportLoader = true;
        operation = NetworkUtility.GENERATE_REPORT;
        postData
                .addFormDataPart("table_name", tableName)//TODO make Dynamic
                .addFormDataPart("section", section);//TODO make Dynamic
        getLoaderManager().restartLoader(LOADER_ID, null, this).forceLoad();
    }

    public void initGenRepLoader() {

        AppUtil.showProgressDialog(getActivity());
        operation = NetworkUtility.STUDENT_ALL_DETAIL;
        postData
                .addFormDataPart("table_name", recordTableName);
        getLoaderManager().restartLoader(LOADER_ID, null, this).forceLoad();
    }

    private void finishGetSubjectDetail(String data) {
        AppUtil.hideProgressDialog();
        if (data.isEmpty()) {
            AppUtil.showCustomDialog(getActivity(), "Something Went Wrong", "Failed to connect to server.", true, R.drawable.ic_error_outline);
        } else {
            byte[] decode = Base64.decode(data.getBytes(), Base64.DEFAULT);
            String jsonString = new String(decode);

            String[] response = JSONExtractor.getStatusMessage(jsonString);
            String status = response[0];
            String message = response[1];
            if (status.equals("0")) {
                AppUtil.showCustomDialog(getActivity(), "Failed", message, true, R.drawable.ic_error_outline);
            } else {
                mappedRollno = JSONExtractor.extractMappedStudentResponse(jsonString);
                for (int i = 0; i < mappedRollno.size(); i++) {
                    totalAttendanceDetail = JSONExtractor.extractGenerateReportResponse(jsonString, mappedRollno.get(i));
                    GenerateReportList generateReports = new GenerateReportList(totalAttendanceDetail, mappedRollno.get(i));
                    generateReportLists.add(generateReports);
                }
                initGenRepLoader();
                // Log.d("GenerateReportActivity", "finishGetSubjectDetail: arrayList Size :" + totalAttendanceDetail.size());
                // Log.d("GenerateReportActivity", "finishGetSubjectDetail: arrayList Size :" + generateReportLists.size());

            }
        }
    }

    private void finishGenerateReportLoader(String data) {
        AppUtil.hideProgressDialog();
        if (data.isEmpty()) {
            AppUtil.showCustomDialog(getActivity(), "Something Went Wrong", "Failed to connect to server.", true, R.drawable.ic_error_outline);
        } else {
            byte[] decode = Base64.decode(data.getBytes(), Base64.DEFAULT);
            String jsonString = new String(decode);


            String[] response = JSONExtractor.getStatusMessage(jsonString);
            String status = response[0];
            String message = response[1];
            if (status.equals("0")) {
                AppUtil.showCustomDialog(getActivity(), "Failed", message, true, R.drawable.ic_error_outline);
            } else {
                studentDetailArrayList = JSONExtractor.extractStudentAllDetailResponse(jsonString);
                Log.d(TAG, "finishGenerateReportLoader: size of studentDetailArrayList " + studentDetailArrayList.size());
                //generateReportAdapter = new GenerateReportAdapter(this, generateReportLists, studentDetailArrayList, department, year, section);
                int total = 0, present = 0, absent = 0;
                for (int i = 0; i < generateReportLists.size(); i++) {
                    for (int j = 0; j < generateReportLists.get(i).getTotalAttendanceDetail().size(); j++) {
                        if ((getValueByPrefrenceForStudent(getString(R.string.student_roll_no))).equals(generateReportLists.get(i).getRollNo())) {

                            attendanceDetail = generateReportLists.get(i).getTotalAttendanceDetail().get(j);
                            Log.d(TAG, "finishGenerateReportLoader: size :" + attendanceDetail.getPresent());
                            total = total + attendanceDetail.getTotal();
                            present = present + attendanceDetail.getPresent();
                            absent = absent + attendanceDetail.getAbsent();
                        }
                    }
                }
                setUi(total, absent, present);
                onUiChange();
            }
        }
    }

    private void setUi(int total, int absent, int present) {
        mTextViewName.setText(getValueByPrefrenceForStudent(getString(R.string.student_name)));
        mTextViewBranch.setText("Branch: " + LoginSession.getInstance(getActivity()).getDepartment().toUpperCase());
        mTextViewTotal.setText(String.valueOf(total));
        mTextViewPresent.setText(String.valueOf(present));
        mTextViewAbsent.setText(String.valueOf(absent));
        int percentage;
        if (total != 0) {
            percentage = present * 100 / total;
        }else percentage = 0;
        mTextViewAttendancePercentage.setText(String.valueOf(percentage));
        updateProgressBar(percentage);
    }


    public String getValueByPrefrenceForStudent(String key) {
        String value = null;
        SharedPreferences userDetails = getActivity().getSharedPreferences(getString(R.string.student_login_sp), MODE_PRIVATE);
        value = userDetails.getString(key, "Dummy Value").toString();
        return value;
    }

    @Override
    public Loader<String> onCreateLoader(int i, Bundle bundle) {
        return new StudentHomeAsyncLoader(getActivity(), postData, operation);
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {

        if (isGenerateReportLoader) {
            isGenerateReportLoader = false;
            finishGetSubjectDetail(data);
        } else {
            finishGenerateReportLoader(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {

    }

    static class StudentHomeAsyncLoader extends AsyncTaskLoader<String> {
        MultipartBody.Builder postData;
        String operation;

        public StudentHomeAsyncLoader(Context context, MultipartBody.Builder postData, String operation) {
            super(context);
            this.postData = postData;
            this.operation = operation;
        }

        @Override
        public String loadInBackground() {
            String response = NetworkUtility.getServerResponse(getContext(), operation, postData);

            return response;
        }
    }
}
